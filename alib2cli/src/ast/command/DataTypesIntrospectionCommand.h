#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class DataTypesIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

	static void printTypes ( const ext::set < std::string > & types );

public:
	DataTypesIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */

