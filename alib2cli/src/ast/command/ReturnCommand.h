#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <ast/Expression.h>

namespace cli {

class ReturnCommand : public Command {
	std::unique_ptr < Expression > m_command;

public:
	ReturnCommand ( std::unique_ptr < Expression > command ) : m_command ( std::move ( command ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		if ( m_command )
			environment.setResult ( m_command->translateAndEval ( environment ) );

		return CommandResult::RETURN;
	}
};

} /* namespace cli */

