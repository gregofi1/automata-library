#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class AlgorithmsIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

	static void printAlgos ( const ext::set < ext::pair < std::string, ext::vector < std::string > > > & algos );

public:
	AlgorithmsIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */

