#include "AlgorithmsIntrospectionCommand.h"

#include <registry/Registry.h>
#include <global/GlobalData.h>

namespace cli {

void AlgorithmsIntrospectionCommand::printAlgos ( const ext::set < ext::pair < std::string, ext::vector < std::string > > > & algos ) {
	for ( const ext::pair < std::string, ext::vector < std::string > > & algo : algos ) {
		common::Streams::out << algo.first;
		if ( !algo.second.empty ( ) ) {
			common::Streams::out << " @";
			for ( const std::string & templateParam : algo.second )
				common::Streams::out << templateParam;
		}
		common::Streams::out << std::endl;
	}
}

CommandResult AlgorithmsIntrospectionCommand::run ( Environment & environment ) const {
	std::string param;
	if ( m_param != nullptr )
		param = m_param->eval ( environment );

	if ( param.empty ( ) ) {
		printAlgos ( abstraction::Registry::listAlgorithms ( ) );
	} else if ( param.find ( "::", param.size ( ) - 2 ) != std::string::npos ) {
		printAlgos ( abstraction::Registry::listAlgorithmGroup ( param ) );
	} else {
		throw exception::CommonException ( "Invalid Algorithm introspection param" );
	}
	return CommandResult::OK;
}

} /* namespace cli */
