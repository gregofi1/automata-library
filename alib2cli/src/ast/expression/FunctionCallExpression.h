#pragma once

#include <ext/foreach>

#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class FunctionCallExpression : public Expression {
	std::string m_name;
	std::vector < std::unique_ptr < Expression > > m_params;

public:
	FunctionCallExpression ( std::string name, std::vector < std::unique_ptr < Expression > > params ) : m_name ( std::move ( name ) ), m_params ( std::move ( params ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		std::shared_ptr < abstraction::Value > result;

		ext::vector < std::shared_ptr < abstraction::Value > > params;
		for ( const std::unique_ptr < Expression > & param : m_params ) {
			std::shared_ptr < abstraction::Value > value = param->translateAndEval ( environment );
			environment.holdTemporary ( value );
			params.push_back ( value );
		}

		std::string name = m_name;

		ext::vector < std::string > templateParams;
/*			for ( const std::unique_ptr < cli::Arg > & templateParam : m_templateParams )
			templateParams.push_back ( templateParam->eval ( environment ) );*/

		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
/*			if ( m_category )
			category = m_category->getCategory ( );*/

		return abstraction::EvalHelper::evalAlgorithm ( environment, name, templateParams, params, category );
	}
};

} /* namespace cli */

