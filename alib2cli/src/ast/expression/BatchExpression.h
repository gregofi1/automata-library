#pragma once

#include <ast/Expression.h>
#include <ast/Statement.h>

namespace cli {

class BatchExpression final : public Expression {
	std::shared_ptr < Statement > m_statement;

public:
	BatchExpression ( std::shared_ptr < Statement > statement ) : m_statement ( std::move ( statement ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		return m_statement->translateAndEval ( nullptr, environment );
	}

};

} /* namespace cli */

