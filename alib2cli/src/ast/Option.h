#pragma once

namespace cli {

class Option {
public:
	virtual ~Option ( ) noexcept = default;
};

} /* namespace cli */

