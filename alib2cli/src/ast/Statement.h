#pragma once

#include <ast/Ast.h>
#include <abstraction/OperationAbstraction.hpp>
#include <environment/Environment.h>

namespace cli {

class Statement : public std::enable_shared_from_this < Statement > {
public:
	virtual ~Statement ( ) noexcept = default;

	virtual std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const = 0;
};

} /* namespace cli */

