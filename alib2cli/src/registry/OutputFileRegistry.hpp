#pragma once

#include <memory>
#include <alib/string>
#include <alib/map>
#include <functional>

#include <exception/CommonException.h>
#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class OutputFileRegistry {
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & typehint ) const = 0;

		virtual ~Entry ( ) = default;
	};

	class EntryImpl : public Entry {
		std::function < std::unique_ptr < abstraction::OperationAbstraction > ( const std::string & typehint ) > m_callback;
	public:
		explicit EntryImpl ( std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const std::string & typehint ) ) : m_callback ( callback ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & typehint ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void registerOutputFileHandler ( const std::string & fileType, std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const std::string & typehint ) );

	static void unregisterOutputFileHandler ( const std::string & fileType );

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & fileType, const std::string & typehint );
};

} /* namespace abstraction */

