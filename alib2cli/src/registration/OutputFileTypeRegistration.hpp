#pragma once

#include <registry/OutputFileRegistry.hpp>

namespace registration {

class OutputFileRegister {
	std::string m_FileType;

public:
	OutputFileRegister ( std::string fileType, std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const std::string & typehint ) ) : m_FileType ( std::move ( fileType ) ) {
		abstraction::OutputFileRegistry::registerOutputFileHandler ( m_FileType, callback );
	}

	~OutputFileRegister ( ) {
		abstraction::OutputFileRegistry::unregisterOutputFileHandler ( m_FileType );
	}
};

} /* namespace registration */

