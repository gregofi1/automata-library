#include "Dot.h"
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <numeric>
#include <sys/wait.h>
#include <unistd.h>

#include <ext/algorithm>

#include <exception/CommonException.h>
#include <registration/AlgoRegistration.hpp>

namespace cli::builtin {

std::vector < std::string> Dot::allowedOutputTypes { "dot", "xdot", "ps", "pdf", "svg", "svgz", "fig", "png", "gif", "jpg", "jpeg", "json", "imap", "cmapx" };

void Dot::run ( const std::string & data, const std::string & outputType, const std::optional < std::string > & outputFile, bool runInBackground ) {
	std::array < int, 2 > fd;
	if ( pipe ( fd.data ( ) ) != 0 )
		throw exception::CommonException ( "Dot: Failed to initialize communication pipe." );

	pid_t pid = fork ( );

	if ( pid < 0 ) {
		throw exception::CommonException ( "Dot: Failed to fork." );
	} else if ( pid == 0 ) { // child
		close ( STDIN_FILENO );
		close ( STDOUT_FILENO );
		dup2 ( fd [ 0 ], STDIN_FILENO );
		close ( fd [ 0 ] );
		close ( fd [ 1 ] );

		std::vector < const char * > cmd;
		cmd.push_back ( "dot" );
		cmd.push_back ( "-T" );
		cmd.push_back ( outputType.c_str ( ) );
		if ( outputFile.has_value ( ) ) {
			cmd.push_back ( "-o" );
			cmd.push_back ( outputFile->c_str ( ) );
		}
		cmd.push_back ( nullptr );

		if ( execvp ( "dot", const_cast < char* const* > ( cmd.data ( ) ) ) == -1 ) // I do not like the const_cast but this is a real issue when mixing C with C++
			throw exception::CommonException ( "Dot: Failed to spawn child process." );
	} else {
		close ( fd [ 0 ] );
		if ( write ( fd [ 1 ], data.c_str ( ), data.size ( ) + 1 ) != static_cast < ssize_t > ( data.size ( ) ) + 1 )
			throw exception::CommonException ( "Dot: Failed to write data to dot child process." );

		close ( fd [ 1 ] );

		if ( ! runInBackground ) {
			int status;
			waitpid ( pid, &status, 0 );
		}
	}
}

void Dot::dot ( const std::string & data, bool runsInBackground ) {
	run ( data, "x11", std::nullopt, runsInBackground );
}

void Dot::dot2 ( const std::string & data ) {
	dot ( data, true );
}

void Dot::dot ( const std::string & data, std::string outputType, const std::string & outputFile ) {
	std::transform ( outputType.begin ( ), outputType.end ( ), outputType.begin ( ), ::tolower );

	if ( std::find ( allowedOutputTypes.begin( ), allowedOutputTypes.end ( ), outputType ) == allowedOutputTypes.end ( ) ) {
		throw exception::CommonException ( "Dot: Invalid output type (" + outputType + ")." );
	}

	run ( data, outputType, outputFile, false );
}



auto DotTx11NonBlocking = registration::AbstractRegister < Dot, void, const std::string & > ( Dot::dot2, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "dot" ).setDocumentation (
"Cli builtin command for DOT format visualization. Does not block CLI.\n\
\n\
@param dot a string containing dot data" );

auto DotTx11 = registration::AbstractRegister < Dot, void, const std::string &, bool > ( Dot::dot, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "dot", "background" ).setDocumentation (
"Cli builtin command for DOT format visualization.\n\
\n\
@param dot a string containing dot data\n\
@param background a flag specifying whether to run in background (does not block CLI input)" );

auto DotFile = registration::AbstractRegister < Dot, void, const std::string &, std::string, const std::string & > ( Dot::dot, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "data", "outputType", "outputFile" ).setDocumentation (
"Cli builtin command for DOT format visualization. Runs dot -T<outputType> -o <outputFile> and blocks the input until done.\n\
\n\
@param data a string containing dot data\n\
@param outputType the type of dot created image (dot, xdot, ps, pdf, svg, svgz, fig, png, gif, jpg, jpeg, json, imap, cmapx )\n\
@param outputFile the destination file name" );

} /* namespace cli::builtin */
