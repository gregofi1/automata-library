#pragma once

#include <optional>
#include <string>
#include <vector>

namespace cli::builtin {

/**
 * Dot visualisation executioner.
 *
 */
class Dot {
public:
	/**
	 * Cli builtin command for DOT format visualization. Runs dot -Tx11.
	 *
	 * \param dot a string containing dot data
	 * \param runsInBackground specify whether run in background and not block CLI input
	 */
	static void dot ( const std::string & data, bool runsInBackground );

	/**
	 * Cli builtin command for DOT format visualization. Runs dot -Tx11 and does not block.
	 *
	 * \param dot a string containing dot data
	 */
	static void dot2 ( const std::string & data );

	/**
	 * Cli builtin command for DOT format visualization. Runs dot -T<outputType> -o <outputFile> and blocks until done.
	 *
	 * \param data a string containing dot data
	 * \param outputType the type of dot created image
	 * \param outputFile the destination file name
	 */
	static void dot ( const std::string & data, std::string outputType, const std::string & outputFile );

protected:
	/** Allowed output types */
	static std::vector < std::string > allowedOutputTypes;

	static void run ( const std::string & data, const std::string & outputType, const std::optional < std::string > & outputFile, bool runInBackground );
};

} /* namespace cli::builtin */

