#!/bin/sh

set -e
set -x
VERSION=$(git describe --long --tags --match=v* | sed 's/^v//g' | sed 's/\([^-]*-g\)/r\1/;s/-/./g')

if [ -z "$VERSION" ]; then
    echo "failed to retrieve current version :("
    exit 1
fi

OUTPATH=pkg/archives/dev
NAMEVER=algorithms-library-v$VERSION
ARCHIVE=$NAMEVER.tar.gz
ARPATH=$OUTPATH/$ARCHIVE

mkdir -p "$OUTPATH"
git archive --format tgz --output $ARPATH --prefix $NAMEVER/ HEAD

echo $ARPATH

