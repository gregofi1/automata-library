/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <chrono>
#include <ostream>
#include <deque>
#include "MeasurementTypes.hpp"
#include "frames/TimeDataFrame.hpp"
#include "frames/MemoryDataFrame.hpp"
#include "frames/CounterDataFrame.hpp"

namespace measurements {

struct MeasurementFrame {
	measurements::stealth_string name;
	measurements::Type type;
	unsigned parentIdx;

	measurements::stealth_vector < unsigned > subIdxs;

	TimeDataFrame	 time;
	MemoryDataFrame	 memory;
	CounterDataFrame counter;

	MeasurementFrame ( measurements::stealth_string, measurements::Type, unsigned );

	static MeasurementFrame aggregate ( const std::vector < MeasurementFrame > & );
};

std::ostream & operator <<( std::ostream &, const MeasurementFrame & );
}

