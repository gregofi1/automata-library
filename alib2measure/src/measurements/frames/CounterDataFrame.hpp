/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <deque>
#include "../MeasurementTypes.hpp"

namespace measurements {

struct MeasurementFrame;
struct CounterDataFrame;

struct CounterHint {
	using frame_type = CounterDataFrame;
	using value_type = long long int;
	enum class Type {
		ADD, SUB
	};

	measurements::stealth_string name;
	Type						 type;
	value_type					 value;
};

struct CounterDataFrame {
	measurements::stealth_map < measurements::stealth_string, CounterHint::value_type > counters;
	measurements::stealth_map < measurements::stealth_string, CounterHint::value_type > inFrameCounters;

	static void init ( unsigned, measurements::stealth_vector < MeasurementFrame > & );
	static void update ( unsigned, measurements::stealth_vector < MeasurementFrame > & );
	static void hint ( unsigned, measurements::stealth_vector < MeasurementFrame > &, const CounterHint & );

	static CounterDataFrame aggregate ( const std::vector < MeasurementFrame > & );
};

std::ostream & operator <<( std::ostream &, const CounterDataFrame & );
}

