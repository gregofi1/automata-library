#include "LinearString.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/SetRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class string::LinearString < >;

namespace {

auto components = registration::ComponentRegister < string::LinearString < > > ( );

auto LinearStringFromString = registration::CastRegister < string::LinearString < char >, std::string > ( );

auto LinearStringSet = registration::SetRegister < string::LinearString < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < string::LinearString < > > ( );

} /* namespace */
