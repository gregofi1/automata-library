#include "String.h"

namespace string {

string::LinearString < char > stringFrom ( const std::string & string ) {
	return string::LinearString < char > { string };
}

string::LinearString < char > stringFrom ( const char * string ) {
	return stringFrom ( std::string ( string ) );
}

} /* namespace string */
