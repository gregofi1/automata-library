#include "WildcardLinearString.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class string::WildcardLinearString < >;

namespace {

auto components = registration::ComponentRegister < string::WildcardLinearString < > > ( );

auto WildcardLinearStringFromString = registration::CastRegister < string::WildcardLinearString < char >, std::string > ( );
auto WildcardLinearStringFromLinearString = registration::CastRegister < string::WildcardLinearString < >, string::LinearString < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < string::WildcardLinearString < > > ( );

} /* namespace */
