#include "RegExp.h"

namespace regexp {

regexp::UnboundedRegExp < char > regexpFrom ( const std::string & string ) {
	return regexpFrom ( string::LinearString < char > ( string ) );
}

regexp::UnboundedRegExp < char > regexpFrom ( const char * string ) {
	return regexpFrom ( std::string ( string ) );
}

template < class SymbolType >
regexp::UnboundedRegExp < SymbolType > regexpFrom ( ) {
	return regexp::UnboundedRegExp < SymbolType > ( regexp::UnboundedRegExpStructure < SymbolType > ( regexp::UnboundedRegExpEmpty < SymbolType > { } ) );
}

} /* namespace regexp */
