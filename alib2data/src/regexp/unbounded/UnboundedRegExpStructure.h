/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace regexp {

template < class SymbolType >
class UnboundedRegExpStructure;

} /* namespace regexp */

#include <exception/CommonException.h>

#include "UnboundedRegExpEmpty.h"

#include "../formal/FormalRegExpStructure.h"

namespace regexp {

/**
 * \brief Represents unbounded regular expression structure. Regular expression is stored as a tree of UnboundedRegExpElements.
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpStructure final {
	/**
	 * The root of the regular expression tree
	 */
	ext::smart_ptr < UnboundedRegExpElement < SymbolType > > m_structure;

public:
	/**
	 * \brief Creates a new instance of the expression structure. Defaultly created structure is empty expression
	 */
	explicit UnboundedRegExpStructure ( );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit UnboundedRegExpStructure ( UnboundedRegExpElement < SymbolType > && structure );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit UnboundedRegExpStructure ( const UnboundedRegExpElement < SymbolType > & structure );

	/**
	 * \brief Creates a new instance based on formal representation. The new instance contains structure converted to unbounded representation.
	 *
	 * \param other the structure in formal representation
	 */
	explicit UnboundedRegExpStructure ( const FormalRegExpStructure < SymbolType > & other );

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the unbounded regular expression tree
	 */
	const UnboundedRegExpElement < SymbolType > & getStructure ( ) const;

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the unbounded regular expression tree
	 */
	UnboundedRegExpElement < SymbolType > & getStructure ( );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( UnboundedRegExpElement < SymbolType > && param );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( const UnboundedRegExpElement < SymbolType > & structure );

	/**
	 * \brief Strucuture printer to the stream
	 *
	 * \param out the output stream
	 * \param structure the structure to print
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const UnboundedRegExpStructure < SymbolType > & structure ) {
		out << structure.getStructure ( );
		return out;
	}

	/**
	 * \brief Three way comparison operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return the strong ordering between the the first and the second parameter
	 */
	friend std::strong_ordering operator <=> ( const UnboundedRegExpStructure & first, const UnboundedRegExpStructure & second ) {
		return first.getStructure() <=> second.getStructure();
	}

	/**
	 * \brief Equality operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return true if the first is equal to the second, false otherwise
	 */
	friend bool operator == ( const UnboundedRegExpStructure & first, const UnboundedRegExpStructure & second ) {
		return first.getStructure() == second.getStructure();
	}

	/**
	 * \brief Performs the type normalization of the regexp structure.
	 *
	 * \return the normalized structure
	 */
	UnboundedRegExpStructure < DefaultSymbolType > normalize ( ) && {
		return UnboundedRegExpStructure < DefaultSymbolType > ( std::move ( * std::move ( getStructure ( ) ).normalize ( ) ) );
	}
};

template < class SymbolType >
UnboundedRegExpStructure < SymbolType >::UnboundedRegExpStructure ( UnboundedRegExpElement < SymbolType > && structure ) : m_structure ( nullptr ) {
	setStructure ( std::move ( structure ) );
}

template < class SymbolType >
UnboundedRegExpStructure < SymbolType >::UnboundedRegExpStructure ( const UnboundedRegExpElement < SymbolType > & structure ) : UnboundedRegExpStructure ( ext::move_copy ( structure ) ) {
}

template < class SymbolType >
UnboundedRegExpStructure < SymbolType >::UnboundedRegExpStructure ( ) : UnboundedRegExpStructure < SymbolType > ( UnboundedRegExpEmpty < SymbolType > ( ) ) {
}

template < class SymbolType >
UnboundedRegExpStructure < SymbolType >::UnboundedRegExpStructure ( const FormalRegExpStructure < SymbolType > & other ) : UnboundedRegExpStructure ( * other.getStructure ( ).asUnbounded ( ) ) {
}

template < class SymbolType >
const UnboundedRegExpElement < SymbolType > & UnboundedRegExpStructure < SymbolType >::getStructure ( ) const {
	return * m_structure;
}

template < class SymbolType >
UnboundedRegExpElement < SymbolType > & UnboundedRegExpStructure < SymbolType >::getStructure ( ) {
	return * m_structure;
}

template < class SymbolType >
void UnboundedRegExpStructure < SymbolType >::setStructure ( const UnboundedRegExpElement < SymbolType > & structure ) {
	setStructure ( ext::move_copy ( structure ) );
}

template < class SymbolType >
void UnboundedRegExpStructure < SymbolType >::setStructure ( UnboundedRegExpElement < SymbolType > && param ) {
	this->m_structure = ext::smart_ptr < UnboundedRegExpElement < SymbolType > > ( std::move ( param ).clone ( ) );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpStructure < DefaultSymbolType >;

