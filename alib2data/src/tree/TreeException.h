#pragma once

#include <exception/CommonException.h>

namespace tree {

/**
 * Exception thrown by an tree, tree parser or tree printer.
 */
class TreeException: public exception::CommonException {
public:
	explicit TreeException(const std::string& cause);
};

} /* namespace tree */

