/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class UnorderedUnrankedTree;

} /* namespace tree */


#include <ext/iostream>
#include <ext/algorithm>

#include <alib/string>
#include <alib/set>
#include <alib/tree>

#include <core/components.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include "UnrankedTree.h"

namespace tree {

class GeneralAlphabet;

/**
 * \brief
 * Tree pattern represented in its natural representation. The representation is so called unranked, therefore it consists of unranked symbols.
 *
 * \details
 * T = ( A, C ),
 * A (Alphabet) = finite set of symbols,
 * C (Content) = tree in its natural representation
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template < class SymbolType >
class UnorderedUnrankedTree final : public core::Components < UnorderedUnrankedTree < SymbolType >, ext::set < SymbolType >, component::Set, GeneralAlphabet > {
	/**
	 * Natural representation of the pattern content.
	 */
	ext::tree < SymbolType > m_content;

	/**
	 * Checks that symbols of the pattern are present in the alphabet.
	 *
	 * \throws TreeException when some symbols of the pattern representation are not present in the alphabet
	 *
	 * \param data the pattern in its natural representation
	 */
	void checkAlphabet ( const ext::tree < SymbolType > & data ) const;

public:
	/**
	 * \brief Creates a new instance of the pattern with concrete alphabet and content.
	 *
	 * \param alphabet the initial alphabet of the pattern
	 * \param pattern the initial content in it's natural representation
	 */
	explicit UnorderedUnrankedTree ( ext::set < SymbolType > alphabet, ext::tree < SymbolType > tree );

	/**
	 * \brief Creates a new instance of the pattern with concrete content. The alphabet is deduced from the content.
	 *
	 * \param pattern the initial content in it's natural representation
	 */
	explicit UnorderedUnrankedTree ( ext::tree < SymbolType > pattern );

	/**
	 * \brief Creates a new instance of the pattern based on UnrankedTree, the alphabet is created from the content of the UnrankedTree.
	 *
	 * \param other the pattern represented as RankedNonlinearPattern
	 */
	explicit UnorderedUnrankedTree ( const UnrankedTree < SymbolType > & other );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < SymbolType > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < SymbolType > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < SymbolType > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	const ext::tree < SymbolType > & getContent ( ) const &;

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	ext::tree < SymbolType > && getContent ( ) &&;

	/**
	 * Setter of the representation of the pattern.
	 *
	 * \throws TreeException in same situations as checkAlphabet
	 *
	 * \param pattern new representation of the pattern.
	 */
	void setTree ( ext::tree < SymbolType > data );

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const UnorderedUnrankedTree & other ) const {
		return std::tie ( getAlphabet ( ), getContent ( ) ) <=> std::tie ( other.getAlphabet ( ), other.getContent ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const UnorderedUnrankedTree & other ) const {
		return std::tie ( getAlphabet ( ), getContent ( ) ) == std::tie ( other.getAlphabet ( ), other.getContent ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const UnorderedUnrankedTree & instance ) {
		out << "(UnorderedUnrankedTree ";
		out << "alphabet = " << instance.getAlphabet ( );
		out << "content = " << instance.getContent ( );
		out << ")";
		return out;
	}

	/**
	 * Nice printer of the tree natural representation
	 *
	 * \param os the output stream to print to
	 */
	void nicePrint ( ext::ostream & os ) const;
};

template < class SymbolType >
UnorderedUnrankedTree < SymbolType >::UnorderedUnrankedTree ( ext::set < SymbolType > alphabet, ext::tree < SymbolType > tree ) : core::Components < UnorderedUnrankedTree, ext::set < SymbolType >, component::Set, GeneralAlphabet > ( std::move ( alphabet ) ), m_content ( std::move ( tree ) ) {
	checkAlphabet ( m_content );
}

template < class SymbolType >
UnorderedUnrankedTree < SymbolType >::UnorderedUnrankedTree ( ext::tree < SymbolType > pattern ) : UnorderedUnrankedTree ( ext::set < SymbolType > ( pattern.prefix_begin ( ), pattern.prefix_end ( ) ), pattern ) {
}

template < class SymbolType >
UnorderedUnrankedTree < SymbolType >::UnorderedUnrankedTree ( const UnrankedTree < SymbolType > & other ) : UnorderedUnrankedTree ( other.getAlphabet ( ), other.getContent ( ) ) {
}

template < class SymbolType >
const ext::tree < SymbolType > & UnorderedUnrankedTree < SymbolType >::getContent ( ) const & {
	return m_content;
}

template < class SymbolType >
ext::tree < SymbolType > && UnorderedUnrankedTree < SymbolType >::getContent ( ) && {
	return std::move ( m_content );
}

template < class SymbolType >
void UnorderedUnrankedTree < SymbolType >::checkAlphabet ( const ext::tree < SymbolType > & data ) const {
	ext::set < SymbolType > minimalAlphabet ( data.prefix_begin ( ), data.prefix_end ( ) );
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet ( ).begin ( ), getAlphabet ( ).end ( ), ext::callback_iterator ( [ ] ( const SymbolType & ) {
			throw TreeException ( "Input symbols not in the alphabet." );
		} ) );
}

template < class SymbolType >
void UnorderedUnrankedTree < SymbolType >::setTree ( ext::tree < SymbolType > data ) {
	checkAlphabet ( data );

	this->m_content = std::move ( data );
}

template < class SymbolType >
void UnorderedUnrankedTree < SymbolType >::nicePrint ( ext::ostream & os ) const {
	m_content.nicePrint ( os );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template < class SymbolType >
class SetConstraint< tree::UnorderedUnrankedTree < SymbolType >, SymbolType, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the pattern.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::UnorderedUnrankedTree < SymbolType > & tree, const SymbolType & symbol ) {
		const ext::tree<SymbolType>& m_content = tree.getContent ( );
		return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end();
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::UnorderedUnrankedTree < SymbolType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::UnorderedUnrankedTree < SymbolType > &, const SymbolType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::UnorderedUnrankedTree < SymbolType > > {
	static tree::UnorderedUnrankedTree < > eval ( tree::UnorderedUnrankedTree < SymbolType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::tree < DefaultSymbolType > content = tree::TreeNormalize::normalizeTree ( std::move ( value ).getContent ( ) );

		return tree::UnorderedUnrankedTree < > ( std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::UnorderedUnrankedTree < >;
