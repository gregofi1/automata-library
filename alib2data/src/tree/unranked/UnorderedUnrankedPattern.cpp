#include "UnorderedUnrankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnorderedUnrankedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::UnorderedUnrankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnorderedUnrankedPattern < > > ( );

auto UnrankedUnorderedPatternFromUnrankedPattern = registration::CastRegister < tree::UnorderedUnrankedPattern < >, tree::UnrankedPattern < > > ( );

} /* namespace */
