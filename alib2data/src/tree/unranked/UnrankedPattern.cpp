#include "UnrankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedPattern < > > ( );

auto UnrankedPatternFromRankedPattern = registration::CastRegister < tree::UnrankedPattern < >, tree::RankedPattern < > > ( );

} /* namespace */
