#include "UnrankedExtendedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedExtendedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedExtendedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedExtendedPattern < > > ( );

auto UnrankedPatternFromRankedPattern = registration::CastRegister < tree::UnrankedExtendedPattern < >, tree::UnrankedPattern < > > ( );

} /* namespace */
