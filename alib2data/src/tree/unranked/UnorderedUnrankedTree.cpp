#include "UnorderedUnrankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnorderedUnrankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::UnorderedUnrankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnorderedUnrankedTree < > > ( );

auto UnrankedUnorderedTreeFromUnrankedTree = registration::CastRegister < tree::UnorderedUnrankedTree < >, tree::UnrankedTree < > > ( );

} /* namespace */
