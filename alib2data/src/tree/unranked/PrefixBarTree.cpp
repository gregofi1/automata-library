#include "PrefixBarTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixBarTree < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixBarTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixBarTree < > > ( );

auto PrefixBarTreeFromUnrankedTree = registration::CastRegister < tree::PrefixBarTree < >, tree::UnrankedTree < > > ( );

auto LinearStringFromPrefixBarTree = registration::CastRegister < string::LinearString < >, tree::PrefixBarTree < > > ( );

} /* namespace */
