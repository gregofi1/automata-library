#include "PrefixRankedExtendedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedExtendedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedExtendedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedExtendedPattern < > > ( );

auto PrefixRankedExtendedPatternFromRankedExtendedPattern = registration::CastRegister < tree::PrefixRankedExtendedPattern < >, tree::RankedExtendedPattern < > > ( );
auto PrefixRankedExtendedPatternFromPrefixRankedPattern = registration::CastRegister < tree::PrefixRankedExtendedPattern < >, tree::PrefixRankedPattern < > > ( );

auto LinearStringFromPrefixRankedPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedExtendedPattern < > > ( );

} /* namespace */
