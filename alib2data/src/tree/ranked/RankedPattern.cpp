#include "RankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::RankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedPattern < > > ( );

auto RankedPatternFromUnrankedPattern = registration::CastRegister < tree::RankedPattern < >, tree::UnrankedPattern < > > ( );

} /* namespace */
