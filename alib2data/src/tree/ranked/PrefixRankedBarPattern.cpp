#include "PrefixRankedBarPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedBarPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedBarPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedBarPattern < > > ( );

auto PrefixRankedBarPatternFromRankedPattern = registration::CastRegister < tree::PrefixRankedBarPattern < >, tree::RankedPattern < > > ( );
auto PrefixRankedBarPatternFromPrefixRankedBarTree = registration::CastRegister < tree::PrefixRankedBarPattern < >, tree::PrefixRankedBarTree < > > ( );

auto LinearStringFromPrefixRankedBarPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedBarPattern < > > ( );

} /* namespace */
