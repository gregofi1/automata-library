#include "RankedExtendedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedExtendedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::RankedExtendedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedExtendedPattern < > > ( );

auto RankedExtendedPatternFromRankedPattern = registration::CastRegister < tree::RankedExtendedPattern < >, tree::RankedPattern < > > ( );

} /* namespace */
