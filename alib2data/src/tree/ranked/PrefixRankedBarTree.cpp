#include "PrefixRankedBarTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedBarTree < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedBarTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedBarTree < > > ( );

auto PrefixRankedBarTreeFromRankedTree = registration::CastRegister < tree::PrefixRankedBarTree < >, tree::RankedTree < > > ( );

auto LinearStringFromPrefixRankedBarTree = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedBarTree < > > ( );

} /* namespace */
