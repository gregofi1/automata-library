#include "UnorderedRankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnorderedRankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::UnorderedRankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnorderedRankedTree < > > ( );

auto UnorderedRankedTreeFromRankedTree = registration::CastRegister < tree::UnorderedRankedTree < >, tree::RankedTree < > > ( );

} /* namespace */
