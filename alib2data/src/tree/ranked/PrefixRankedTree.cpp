#include "PrefixRankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedTree < > > ( );

auto PrefixRankedTreeFromRankedTree = registration::CastRegister < tree::PrefixRankedTree < >, tree::RankedTree < > > ( );
auto PrefixRankedTreeFromPostfixRankedTree = registration::CastRegister < tree::PrefixRankedTree < >, tree::PostfixRankedTree < > > ( );

auto LinearStringFromPrefixRankedTree = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedTree < > > ( );

} /* namespace */
