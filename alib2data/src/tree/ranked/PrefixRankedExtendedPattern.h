/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class PrefixRankedExtendedPattern;

} /* namespace tree */

#include <ext/algorithm>

#include <alib/set>
#include <alib/vector>
#include <alib/tree>
#include <alib/deque>

#include <core/components.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/WildcardSymbol.h>
#include <alphabet/NodeWildcardSymbol.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include <string/LinearString.h>

#include "RankedExtendedPattern.h"
#include "PrefixRankedPattern.h"

namespace tree {

class GeneralAlphabet;
class SubtreeWildcard;
class NodeWildcard;

/**
 * \brief
 * Tree pattern represented as linear sequece as result of preorder traversal. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer. Additionally the pattern contains a special wildcard symbol representing any subtree.
 *
 * \details
 * T = (A, C, W \in A, N \in A),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = linear representation of the pattern content
 * W (Wildcard) = special symbol representing any subtree
 * N (Node Wildcards) = special symbol representing any label
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class PrefixRankedExtendedPattern final : public core::Components < PrefixRankedExtendedPattern < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, NodeWildcards >, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > {
	/**
	 * Linear representation of the pattern content.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > m_Data;

	/**
	 * Checker of validity of the representation of the ettern
	 *
	 * \throws TreeException when new pattern representation is not valid
	 */
	void arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data );

public:
	/**
	 * \brief Creates a new instance of the pattern with concrete alphabet, content, and wildcard.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param alphabet the initial alphabet of the pattern
	 * \param data the initial pattern in linear representation
	 */
	explicit PrefixRankedExtendedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nodeWildcards, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param data the initial pattern in linear representation
	 */
	explicit PrefixRankedExtendedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nodeWildcards, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the pattern based on the PrefixRankedTree. The linear representation is copied from tree. Alphabet as well. Subtree wildcard is defaultly constructed.
	 *
	 * \param pattern representation of a pattern.
	 */
	explicit PrefixRankedExtendedPattern ( const PrefixRankedPattern < SymbolType > & pattern );

	/**
	 * \brief Creates a new instance of the pattern based on the RankedPattern. The linear representation is constructed by preorder traversal on the tree parameter.
	 *
	 * \param pattern representation of a pattern.
	 */
	explicit PrefixRankedExtendedPattern ( const RankedExtendedPattern < SymbolType > & pattern );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getNodeWildcards ( ) const & {
		return this->template accessComponent < NodeWildcards > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getNodeWildcards ( ) && {
		return std::move ( this->template accessComponent < NodeWildcards > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendNodeWildcards ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < NodeWildcards > ( ).add ( symbols );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	const common::ranked_symbol < SymbolType > & getSubtreeWildcard ( ) const & {
		return this->template accessComponent < SubtreeWildcard > ( ).get ( );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	common::ranked_symbol < SymbolType > && getSubtreeWildcard ( ) && {
		return std::move ( this->template accessComponent < SubtreeWildcard > ( ).get ( ) );
	}

	/**
	 * Getter of the pattern representation.
	 *
	 * \return List of symbols forming the linear representation of the pattern.
	 */
	const ext::vector < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the pattern representation.
	 *
	 * \return List of symbols forming the linear representation of the pattern.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > && getContent ( ) &&;

	/**
	 * Setter of the representation of the pattern.
	 *
	 * \throws TreeException when new pattern representation is not valid or when symbol of the representation are not present in the alphabet
	 *
	 * \param data new List of symbols forming the representation of the pattern.
	 */
	void setContent ( ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * @return true if pattern is an empty word (vector length is 0). The method is present to allow compatibility with strings. Tree is never empty in this datatype.
	 */
	bool isEmpty ( ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const PrefixRankedExtendedPattern & other ) const {
		return std::tie ( m_Data, getAlphabet ( ), getSubtreeWildcard ( ), getNodeWildcards ( ) ) <=> std::tie ( other.m_Data, other.getAlphabet ( ), other.getSubtreeWildcard ( ), other.getNodeWildcards ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const PrefixRankedExtendedPattern & other ) const {
		return std::tie ( m_Data, getAlphabet ( ), getSubtreeWildcard ( ), getNodeWildcards ( ) ) == std::tie ( other.m_Data, other.getAlphabet ( ), other.getSubtreeWildcard ( ), other.getNodeWildcards ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const PrefixRankedExtendedPattern & instance ) {
		out << "(PrefixRankedExtendedPattern ";
		out << "alphabet = " << instance.getAlphabet ( );
		out << "content = " << instance.getContent ( );
		out << "subtreeWildcard = " << instance.getSubtreeWildcard ( );
		out << "nodeWildcards = " << instance.getNodeWildcards ( );
		out << ")";
		return out;
	}

	/**
	 * \brief Creates a new instance of the string from a linear representation of a tree
	 *
	 * \returns tree casted to string
	 */
	explicit operator string::LinearString < common::ranked_symbol < SymbolType > > ( ) const {
		return string::LinearString < common::ranked_symbol < SymbolType > > ( getAlphabet ( ), getContent ( ) );
	}
};

template < class SymbolType >
PrefixRankedExtendedPattern < SymbolType >::PrefixRankedExtendedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nodeWildcards, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data ) : core::Components < PrefixRankedExtendedPattern, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, NodeWildcards >, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > ( std::move ( alphabet ), std::move ( nodeWildcards ), std::move ( subtreeWildcard ) ) {
	setContent ( std::move ( data ) );
}

template < class SymbolType >
PrefixRankedExtendedPattern < SymbolType >::PrefixRankedExtendedPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nodeWildcards, ext::vector < common::ranked_symbol < SymbolType > > data ) : PrefixRankedExtendedPattern ( subtreeWildcard, nodeWildcards, ext::set < common::ranked_symbol < SymbolType > > ( data.begin ( ), data.end ( ) ) + ext::set < common::ranked_symbol < SymbolType > > { subtreeWildcard } + nodeWildcards, data ) {
}

template < class SymbolType >
PrefixRankedExtendedPattern < SymbolType >::PrefixRankedExtendedPattern ( const PrefixRankedPattern < SymbolType > & pattern ) : PrefixRankedExtendedPattern ( pattern.getSubtreeWildcard ( ), { }, pattern.getAlphabet ( ) + ext::set < common::ranked_symbol < SymbolType > > { pattern.getSubtreeWildcard ( ) }, pattern.getContent ( ) ) {
}

template < class SymbolType >
PrefixRankedExtendedPattern < SymbolType >::PrefixRankedExtendedPattern ( const RankedExtendedPattern < SymbolType > & pattern ) : PrefixRankedExtendedPattern ( pattern.getSubtreeWildcard ( ), pattern.getNodeWildcards ( ), pattern.getAlphabet ( ), TreeAuxiliary::treeToPrefix ( pattern.getContent ( ) ) ) {
}

template < class SymbolType >
const ext::vector < common::ranked_symbol < SymbolType > > & PrefixRankedExtendedPattern < SymbolType >::getContent ( ) const & {
	return this->m_Data;
}

template < class SymbolType >
ext::vector < common::ranked_symbol < SymbolType > > && PrefixRankedExtendedPattern < SymbolType >::getContent ( ) && {
	return std::move ( this->m_Data );
}

template < class SymbolType >
void PrefixRankedExtendedPattern < SymbolType >::setContent ( ext::vector < common::ranked_symbol < SymbolType > > data ) {
	arityChecksum ( data );

	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.begin ( ), data.end ( ) );
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet ( ).begin ( ), getAlphabet ( ).end ( ), ext::callback_iterator ( [ ] ( const common::ranked_symbol < SymbolType > & ) {
			throw TreeException ( "Input symbols not in the alphabet." );
		} ) );

	this->m_Data = std::move ( data );
}

template < class SymbolType >
void PrefixRankedExtendedPattern < SymbolType >::arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data ) {
	if ( std::accumulate ( data.begin ( ), data.end ( ), 1, [ ] ( int current, const common::ranked_symbol < SymbolType > & symbol ) {
				return current + symbol.getRank ( ) - 1;
			} ) != 0 )
		throw TreeException ( "The string does not form a tree" );
}

template < class SymbolType >
bool PrefixRankedExtendedPattern < SymbolType >::isEmpty ( ) const {
	return this->m_Data.empty ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedExtendedPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the pattern.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::PrefixRankedExtendedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::vector < common::ranked_symbol < SymbolType > > & content = pattern.getContent ( );

		return std::find ( content.begin ( ), content.end ( ), symbol ) != content.end ( ) || pattern.template accessComponent < tree::SubtreeWildcard > ( ).get ( ) == symbol || pattern.template accessComponent < tree::NodeWildcards > ( ).get ( ).contains ( symbol );
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::PrefixRankedExtendedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::PrefixRankedExtendedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper class specifying constraints for the pattern's internal node wildcards component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedExtendedPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::NodeWildcards > {
public:
	/**
	 * Any node wildcard may be changed to a regular node.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns false
	 */
	static bool used ( const tree::PrefixRankedExtendedPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return false;
	}

	/**
	 * Node wildcards must already be in the alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is in the alphabet of the pattern
	 */
	static bool available ( const tree::PrefixRankedExtendedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).contains ( symbol );
	}

	/**
	 * Node wildcard cannot be a subtree wildcard at the same time.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol is already a subtree wildcard
	 */
	static void valid ( const tree::PrefixRankedExtendedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		if ( pattern.template accessComponent < tree::SubtreeWildcard > ( ).get ( ) == symbol )
			throw tree::TreeException ( "NodeWildcard is already a SubtreeWildcard" );
	}
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class ElementConstraint< tree::PrefixRankedExtendedPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::SubtreeWildcard > {
public:
	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is already in the alphabet of the pattern
	 */
	static bool available ( const tree::PrefixRankedExtendedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).contains ( symbol );
	}

	/**
	 * Subtree wildcard needs to have zero arity. Also subtree wildcard may not already be a node wildcard.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity or it is a node wildcard
	 */
	static void valid ( const tree::PrefixRankedExtendedPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol) {
		if( symbol.getRank() != 0 )
			throw tree::TreeException ( "SubtreeWildcard symbol has nonzero arity" );
		if ( pattern.template accessComponent < tree::NodeWildcards > ( ).get ( ).contains ( symbol ) )
			throw tree::TreeException ( "SubtreeWildcard is already a NodeWildcard" );
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::PrefixRankedExtendedPattern < SymbolType > > {
	static tree::PrefixRankedExtendedPattern < > eval ( tree::PrefixRankedExtendedPattern < SymbolType > && value ) {
		common::ranked_symbol < DefaultSymbolType > wildcard = alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( value ).getSubtreeWildcard ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > nodeWildcards = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getNodeWildcards ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::vector < common::ranked_symbol < DefaultSymbolType > > content = alphabet::SymbolNormalize::normalizeRankedSymbols ( std::move ( value ).getContent ( ) );

		return tree::PrefixRankedExtendedPattern < > ( std::move ( wildcard ), std::move ( nodeWildcards ), std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::PrefixRankedExtendedPattern < >;

