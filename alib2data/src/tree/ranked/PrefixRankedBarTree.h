/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class PrefixRankedBarTree;

} /* namespace tree */

#include <ext/algorithm>

#include <alib/set>
#include <alib/vector>
#include <alib/tree>
#include <alib/deque>

#include <core/components.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/BarSymbol.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include <string/LinearString.h>

#include "RankedTree.h"

namespace tree {

class GeneralAlphabet;
class BarSymbols;

/**
 * \brief
 * Tree structure represented as linear sequece as result of preorder traversal with additional bar symbols. The representation is so called ranked, therefore it consists of ranked symbols (bars are ranked as well). The rank of the ranked symbol needs to be compatible with unsigned integer.
 *
 * The bars represent end mark of all subtrees in the notation.
 *
 * \details
 * T = (A, B \subset A, C),
 * A (Alphabet) = finite set of ranked symbols,
 * B (Bars) = finite set of ranked symbols representing bars,
 * C (Content) = linear representation of the tree content
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class PrefixRankedBarTree final : public core::Components < PrefixRankedBarTree < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, BarSymbols > > {
	/**
	 * Linear representation of the tree content.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > m_Data;

	/**
	 * Checker of validity of the representation of the tree
	 *
	 * \throws TreeException when new tree representation is not valid
	 */
	void arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data );

public:
	/**
	 * \brief Creates a new instance of the tree with concrete alphabet, bars, and content.
	 *
	 * \param bars the initial bar set
	 * \param alphabet the initial alphabet of the tree
	 * \param data the initial tree in linear representation
	 */
	explicit PrefixRankedBarTree ( ext::set < common::ranked_symbol < SymbolType > > bars, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the tree based on the content and bar set, the alphabet is implicitly created from the content.
	 *
	 * \param bars the initial bar set
	 * \param data the initial tree in linear representation
	 */
	explicit PrefixRankedBarTree ( ext::set < common::ranked_symbol < SymbolType > > bars, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the tree based on the RankedTree. The linear representation is constructed (including bars) by preorder traversal on the tree parameter. Symbol part of bars is provided as a parameter.
	 *
	 * \param barBase the symbol part of all bars
	 * \param tree RankedTree representation of a tree.
	 */
	explicit PrefixRankedBarTree ( SymbolType barBase, const RankedTree < SymbolType > & tree );

	/**
	 * \brief Creates a new instance of the tree based on the RankedTree. The linear representation is constructed (including bars) by preorder traversal on the tree parameter. Bar symbols are created using some default value.
	 *
	 * \param tree RankedTree representation of a tree.
	 */
	explicit PrefixRankedBarTree ( const RankedTree < > & tree );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the tree
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the tree
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the bar set.
	 *
	 * \returns the bar set of the tree
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getBars ( ) const & {
		return this->template accessComponent < BarSymbols > ( ).get ( );
	}

	/**
	 * Getter of the bar set.
	 *
	 * \returns the bar set of the tree
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getBars ( ) && {
		return std::move ( this->template accessComponent < BarSymbols > ( ).get ( ) );
	}

	/**
	 * Adder of symbols to a bar set.
	 *
	 * \param symbols the new symbols to be added to the bar set
	 */
	void extendBars ( const ext::set < common::ranked_symbol < SymbolType > > & bars ) {
		this->template accessComponent < BarSymbols > ( ).add ( bars );
	}

	/**
	 * Getter of the tree representation.
	 *
	 * \return List of symbols forming the linear representation of the tree.
	 */
	const ext::vector < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the tree representation.
	 *
	 * \return List of symbols forming the linear representation of the tree.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > && getContent ( ) && ;

	/**
	 * Setter of the representation of the tree.
	 *
	 * \throws TreeException when new tree representation is not valid or when symbol of the representation are not present in the alphabet
	 *
	 * \param data new List of symbols forming the representation of the tree.
	 */
	void setContent ( ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * @return true if tree is an empty word (vector length is 0). The method is present to allow compatibility with strings. Tree is never empty in this datatype.
	 */
	bool isEmpty ( ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const PrefixRankedBarTree & other ) const {
		return std::tie ( m_Data, getAlphabet(), getBars() ) <=> std::tie ( other.m_Data, other.getAlphabet(), other.getBars() );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const PrefixRankedBarTree & other ) const {
		return std::tie ( m_Data, getAlphabet(), getBars() ) == std::tie ( other.m_Data, other.getAlphabet(), other.getBars() );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const PrefixRankedBarTree & instance ) {
		out << "(PrefixRankedBarTree";
		out << " alphabet = " << instance.getAlphabet ( );
		out << " bars = " << instance.getBars ( );
		out << " content = " << instance.getContent ( );
		out << ")";
		return out;
	}

	/**
	 * \brief Creates a new instance of the string from a linear representation of a tree
	 *
	 * \returns tree casted to string
	 */
	explicit operator string::LinearString < common::ranked_symbol < SymbolType > > ( ) const {
		return string::LinearString < common::ranked_symbol < SymbolType > > ( getAlphabet ( ), getContent ( ) );
	}
};

template < class SymbolType >
PrefixRankedBarTree < SymbolType >::PrefixRankedBarTree ( ext::set < common::ranked_symbol < SymbolType > > bars, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data ) : core::Components < PrefixRankedBarTree, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, BarSymbols > > ( std::move ( alphabet ), std::move ( bars ) ) {
	setContent ( std::move ( data ) );
}

template < class SymbolType >
PrefixRankedBarTree < SymbolType >::PrefixRankedBarTree ( ext::set < common::ranked_symbol < SymbolType > > bars, ext::vector < common::ranked_symbol < SymbolType > > data ) : PrefixRankedBarTree ( bars, bars + ext::set < common::ranked_symbol < SymbolType > > ( data.begin ( ), data.end ( ) ), data ) {
}

template < class SymbolType >
PrefixRankedBarTree < SymbolType >::PrefixRankedBarTree ( SymbolType barBase, const RankedTree < SymbolType > & tree ) : PrefixRankedBarTree ( TreeAuxiliary::computeBars ( tree.getAlphabet ( ), barBase ), tree.getAlphabet ( ) + TreeAuxiliary::computeBars ( tree.getAlphabet ( ), barBase ), TreeAuxiliary::treeToPrefix ( tree.getContent ( ), barBase ) ) {
}

template < class SymbolType >
PrefixRankedBarTree < SymbolType >::PrefixRankedBarTree ( const RankedTree < > & tree ) : PrefixRankedBarTree ( alphabet::BarSymbol::instance < SymbolType > ( ), tree ) {
}

template < class SymbolType >
const ext::vector < common::ranked_symbol < SymbolType > > & PrefixRankedBarTree < SymbolType >::getContent ( ) const & {
	return this->m_Data;
}

template < class SymbolType >
ext::vector < common::ranked_symbol < SymbolType > > && PrefixRankedBarTree < SymbolType >::getContent ( ) && {
	return std::move ( this->m_Data );
}

template < class SymbolType >
void PrefixRankedBarTree < SymbolType >::setContent ( ext::vector < common::ranked_symbol < SymbolType > > data ) {
	arityChecksum ( data );

	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.begin ( ), data.end ( ) );
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet ( ).begin ( ), getAlphabet ( ).end ( ), ext::callback_iterator ( [ ] ( const common::ranked_symbol < SymbolType > & ) {
			throw TreeException ( "Input symbols not in the alphabet." );
		} ) );

	this->m_Data = std::move ( data );
}

template < class SymbolType >
void PrefixRankedBarTree < SymbolType >::arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data ) {
	struct Data {
		int terminals;
		int bars;
		int types;
	};

	Data accRes = std::accumulate ( data.begin ( ), data.end ( ), Data { 1, 1, 0 }, [ * this ] ( const Data & current, const common::ranked_symbol < SymbolType > & symbol ) {
			if ( getBars ( ).contains ( symbol ) )
				return Data { current.terminals, static_cast < int > ( current.bars + symbol.getRank ( ) - 1 ), current.types - 1 };
			else
				return Data { static_cast < int > ( current.terminals + symbol.getRank ( ) - 1 ), current.bars, current.types + 1 };
		} );

	if ( accRes.terminals != 0 || accRes.bars != 0 || accRes.types != 0 )
		throw TreeException ( "The string does not form a tree" );
}

template < class SymbolType >
bool PrefixRankedBarTree < SymbolType >::isEmpty ( ) const {
	return this->m_Data.empty ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the tree's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedBarTree < SymbolType >, common::ranked_symbol < SymbolType >, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the tree.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::PrefixRankedBarTree < SymbolType > & tree, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::vector < common::ranked_symbol < SymbolType > > & content = tree.getContent ( );

		return std::find ( content.begin ( ), content.end ( ), symbol ) != content.end ( );
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::PrefixRankedBarTree < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::PrefixRankedBarTree < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper class specifying constraints for the tree's internal bar set component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedBarTree < SymbolType >, common::ranked_symbol < SymbolType >, tree::BarSymbols > {
public:
	/**
	 * Returns true if the symbol is still used in the tree.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::PrefixRankedBarTree < SymbolType > & tree, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::vector < common::ranked_symbol < SymbolType > > & content = tree.getContent ( );

		return std::find ( content.begin ( ), content.end ( ), symbol ) != content.end ( );
	}

	/**
	 * Determines whether the symbol is available in the tree's alphabet.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is already in the bar set of the tree
	 */
	static bool available ( const tree::PrefixRankedBarTree < SymbolType > & tree, const common::ranked_symbol < SymbolType > & symbol ) {
		return tree.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * All symbols are valid as a bar symbol of the tree.
	 *
	 * \param tree the tested tree
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::PrefixRankedBarTree < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::PrefixRankedBarTree < SymbolType > > {
	static tree::PrefixRankedBarTree < > eval ( tree::PrefixRankedBarTree < SymbolType > && value ) {
		ext::set < common::ranked_symbol < DefaultSymbolType > > bars = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getBars ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::vector < common::ranked_symbol < DefaultSymbolType > > content = alphabet::SymbolNormalize::normalizeRankedSymbols ( std::move ( value ).getContent ( ) );

		return tree::PrefixRankedBarTree < > ( std::move ( bars ), std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::PrefixRankedBarTree < >;

