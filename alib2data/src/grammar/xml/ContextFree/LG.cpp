#include "LG.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::LG < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::LG < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::LG < > > ( );

} /* namespace */
