#include "ContextPreservingUnrestrictedGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::ContextPreservingUnrestrictedGrammar < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::ContextPreservingUnrestrictedGrammar < > > ( );

} /* namespace */
