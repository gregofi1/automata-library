#include "RightRG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::RightRG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::RightRG < > > ( );

} /* namespace */
