#include "LeftRG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LeftRG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LeftRG < > > ( );

} /* namespace */
