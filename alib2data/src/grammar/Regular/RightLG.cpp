#include "RightLG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::RightLG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::RightLG < > > ( );

} /* namespace */
