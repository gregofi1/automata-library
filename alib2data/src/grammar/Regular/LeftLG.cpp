#include "LeftLG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LeftLG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LeftLG < > > ( );

} /* namespace */
