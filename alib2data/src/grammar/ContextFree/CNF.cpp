#include "CNF.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::CNF < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::CNF < > > ( );

} /* namespace */
