#include "EpsilonFreeCFG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::EpsilonFreeCFG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::EpsilonFreeCFG < > > ( );

} /* namespace */
