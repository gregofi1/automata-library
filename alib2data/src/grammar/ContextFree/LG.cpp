#include "LG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LG < > > ( );

} /* namespace */
