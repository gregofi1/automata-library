#include "CFG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::CFG < >;

namespace {

auto CFGEpsilonFreeCFG = registration::CastRegister < grammar::CFG < >, grammar::EpsilonFreeCFG < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < grammar::CFG < > > ( );

} /* namespace */
