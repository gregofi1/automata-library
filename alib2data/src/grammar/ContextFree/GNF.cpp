#include "GNF.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::GNF < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::GNF < > > ( );

} /* namespace */
