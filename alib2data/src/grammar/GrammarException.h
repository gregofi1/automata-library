#pragma once

#include <exception/CommonException.h>

namespace grammar {

/**
 * Exception thrown by an grammar, grammar parser or grammar printer.
 */
class GrammarException: public exception::CommonException {
public:
	explicit GrammarException(const std::string& cause);
};

} /* namespace grammar */

