#include "NonContractingGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::NonContractingGrammar < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::NonContractingGrammar < > > ( );

} /* namespace */
