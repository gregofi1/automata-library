#include "CSG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::CSG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::CSG < > > ( );

} /* namespace */
