/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/memory>
#include "FormalRTEElement.h"

#include "FormalRTEEmpty.h"

#include <exception/CommonException.h>

namespace rte {

/**
 * \brief Represents unbounded regular expression structure. Regular expression is stored as a tree of UnboundedRegExpElements.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTEStructure final {
	/**
	 * The root of the regular tree expression
	 */
	ext::smart_ptr < FormalRTEElement < SymbolType > > m_structure;

public:
	/**
	 * \brief Creates a new instance of the expression structure. Defaultly created structure is empty expression
	 */
	explicit FormalRTEStructure ( );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit FormalRTEStructure ( FormalRTEElement < SymbolType > && structure );

	/**
	 * \brief Creates a new instance of the expression structure. The new instance contains structure as provided by the parameter.
	 *
	 * \param structure the expression structure.
	 */
	explicit FormalRTEStructure ( const FormalRTEElement < SymbolType > & structure );

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the unbounded regular expression tree
	 */
	const FormalRTEElement < SymbolType > & getStructure ( ) const;

	/**
	 * Getter of the root of the structure.
	 *
	 * \return Root node of the unbounded regular expression tree
	 */
	FormalRTEElement < SymbolType > & getStructure ( );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( FormalRTEElement < SymbolType > && param );

	/**
	 * Sets the root node of the expression structure
	 *
	 * \param structure new root node of the structure
	 */
	void setStructure ( const FormalRTEElement < SymbolType > & structure );

	/**
	 * \brief Strucuture printer to the stream
	 *
	 * \param out the output stream
	 * \param structure the structure to print
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const FormalRTEStructure < SymbolType > & structure ) {
		out << structure.getStructure ( );
		return out;
	}

	/**
	 * \brief Three way comparison operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return the strong ordering between the the first and the second parameter
	 */
	friend std::strong_ordering operator <=> ( const FormalRTEStructure < SymbolType > & first, const FormalRTEStructure < SymbolType > & second ) {
		return first.getStructure() <=> second.getStructure();
	}

	/**
	 * \brief Equality operator implementation for the expression structure.
	 *
	 * \param first the first instance
	 * \param second the second instance
	 *
	 * \return true if the first is equal to the second, false otherwise
	 */
	friend bool operator == ( const FormalRTEStructure < SymbolType > & first, const FormalRTEStructure < SymbolType > & second ) {
		return first.getStructure() == second.getStructure();
	}

	/**
	 * \brief Performs the type normalization of the rte structure.
	 *
	 * \return the normalized structure
	 */
	FormalRTEStructure < DefaultSymbolType > normalize ( ) && {
		return FormalRTEStructure < DefaultSymbolType > ( std::move ( * std::move ( getStructure ( ) ).normalize ( ) ) );
	}

};

template < class SymbolType >
FormalRTEStructure < SymbolType >::FormalRTEStructure ( FormalRTEElement < SymbolType > && structure ) : m_structure ( nullptr ) {
	setStructure ( std::move ( structure ) );
}

template < class SymbolType >
FormalRTEStructure < SymbolType >::FormalRTEStructure ( const FormalRTEElement < SymbolType > & structure ) : FormalRTEStructure ( ext::move_copy ( structure ) ) {
}

template < class SymbolType >
FormalRTEStructure < SymbolType >::FormalRTEStructure ( ) : FormalRTEStructure ( FormalRTEEmpty < SymbolType > ( ) ) {
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTEStructure < SymbolType >::getStructure ( ) const {
	return * m_structure;
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTEStructure < SymbolType >::getStructure ( ) {
	return * m_structure;
}

template < class SymbolType >
void FormalRTEStructure < SymbolType >::setStructure ( const FormalRTEElement < SymbolType > & structure ) {
	setStructure ( ext::move_copy ( structure ) );
}

template < class SymbolType >
void FormalRTEStructure < SymbolType >::setStructure ( FormalRTEElement < SymbolType > && param ) {
	this->m_structure = ext::smart_ptr < FormalRTEElement < SymbolType > > ( std::move ( param ).clone ( ) );
}

} /* namespace rte */

extern template class rte::FormalRTEStructure < DefaultSymbolType >;

