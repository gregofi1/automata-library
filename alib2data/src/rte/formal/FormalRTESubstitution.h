/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "FormalRTEElement.h"
#include "FormalRTESymbolSubst.h"

#include <exception/CommonException.h>
#include <ext/utility>

namespace rte {

/**
 * \brief Represents the concatenation operator in the regular tree expression. The node must have exactly two children.
 *
 * The structure is derived from BinaryNode that provides the children accessors.
 *
 * The node can be visited by the FormalRTEElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTESubstitution : public ext::BinaryNode < FormalRTEElement < SymbolType > > {
	/**
	 * The substitution symbol of the node. The symbol will be substitued in left tree by right
	 */
	FormalRTESymbolSubst < SymbolType > m_substitutionSymbol;

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &&
	 */
	void accept ( typename FormalRTEElement < SymbolType >::RValueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to substitute in, tree to substitue by and a symbol representing place of substitution
	 *
	 * \param left the tree to substitute in
	 * \param right the tree to substitute by
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTESubstitution ( FormalRTEElement < SymbolType > && left, FormalRTEElement < SymbolType > && right, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to substitute in, tree to substitue by and a symbol representing place of substitution
	 *
	 * \param left the tree to substitute in
	 * \param right the tree to substitute by
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTESubstitution ( const FormalRTEElement < SymbolType > & left, const FormalRTEElement < SymbolType > & right, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTESubstitution < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTESubstitution < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * Getter of the tree to substitute in
	 *
	 * \return tree to substitute in
	 */
	const FormalRTEElement < SymbolType > & getLeftElement ( ) const;

	/**
	 * Getter of the tree to substitute by
	 *
	 * \return tree to substitute by
	 */
	const FormalRTEElement < SymbolType > & getRightElement ( ) const;

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	const FormalRTESymbolSubst < SymbolType > & getSubstitutionSymbol ( ) const;

	/**
	 * Getter of the tree to substitute in
	 *
	 * \return tree to substitute in
	 */
	FormalRTEElement < SymbolType > & getLeftElement ( );

	/**
	 * Getter of the tree to substitute by
	 *
	 * \return tree to substitute by
	 */
	FormalRTEElement < SymbolType > & getRightElement ( );

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	FormalRTESymbolSubst < SymbolType > & getSubstitutionSymbol ( );

	/**
	 * Setter of the tree to substitute in
	 *
	 * \param element the tree to substitute in
	 */
	void setLeftElement ( const FormalRTEElement < SymbolType > & element );

	/**
	 * Setter of the tree to substitute by
	 *
	 * \param element the tree to substitute by
	 */
	void setLeftElement ( FormalRTEElement < SymbolType > && element );

	/**
	 * Setter of the tree to substitute in
	 *
	 * \param element the tree to substitute in
	 */
	void setRightElement ( const FormalRTEElement < SymbolType > & element );

	/**
	 * Setter of the tree to substitute by
	 *
	 * \param element the tree to substitute by
	 */
	void setRightElement ( FormalRTEElement < SymbolType > && element );

	/**
	 * Setter of the substitution symbol
	 *
	 * \param element the substitution symbol
	 */
	void setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRTESubstitution < SymbolType > & ) const;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRTESubstitution < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( ext::ostream & ) const
	 */
	void operator >>( ext::ostream & out ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > normalize ( ) && override {
		FormalRTESymbolSubst < DefaultSymbolType > subst ( alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( m_substitutionSymbol ).getSymbol ( ) ) );
		return ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > ( new FormalRTESubstitution < DefaultSymbolType > ( std::move ( * std::move ( getLeftElement ( ) ).normalize ( ) ), std::move ( * std::move ( getRightElement ( ) ).normalize ( ) ), std::move ( subst ) ) );
	}
};

template < class SymbolType >
FormalRTESubstitution < SymbolType >::FormalRTESubstitution ( FormalRTEElement < SymbolType > && left, FormalRTEElement < SymbolType > && right, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : ext::BinaryNode < FormalRTEElement < SymbolType > > ( std::move ( left ), std::move ( right ) ), m_substitutionSymbol ( std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
FormalRTESubstitution < SymbolType >::FormalRTESubstitution ( const FormalRTEElement < SymbolType > & left, const FormalRTEElement < SymbolType > & right, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : FormalRTESubstitution ( ext::move_copy ( left ), ext::move_copy ( right ), std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTESubstitution < SymbolType >::getLeftElement ( ) const {
	return this->getLeft ( );
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTESubstitution < SymbolType >::getRightElement ( ) const {
	return this->getRight ( );
}

template < class SymbolType >
const FormalRTESymbolSubst < SymbolType > & FormalRTESubstitution < SymbolType >::getSubstitutionSymbol ( ) const {
	return m_substitutionSymbol;
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTESubstitution < SymbolType >::getLeftElement ( ) {
	return this->getLeft ( );
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTESubstitution < SymbolType >::getRightElement ( ) {
	return this->getRight ( );
}

template < class SymbolType >
FormalRTESymbolSubst < SymbolType > & FormalRTESubstitution < SymbolType >::getSubstitutionSymbol ( ) {
	return m_substitutionSymbol;
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::setLeftElement ( const FormalRTEElement < SymbolType > & element ) {
	setLeftElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::setLeftElement ( FormalRTEElement < SymbolType > && element ) {
	this->setLeft ( std::move ( element ) );
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::setRightElement ( const FormalRTEElement < SymbolType > & element ) {
	setRightElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::setRightElement ( FormalRTEElement < SymbolType > && element ) {
	this->setRight ( std::move ( element ) );
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol ) {
	m_substitutionSymbol = std::move ( symbol );
}

template < class SymbolType >
FormalRTESubstitution < SymbolType > * FormalRTESubstitution < SymbolType >::clone ( ) const & {
	return new FormalRTESubstitution ( * this );
}

template < class SymbolType >
FormalRTESubstitution < SymbolType > * FormalRTESubstitution < SymbolType >::clone ( ) && {
	return new FormalRTESubstitution ( std::move ( * this ) );
}

template < class SymbolType >
std::strong_ordering FormalRTESubstitution < SymbolType >::operator <=> ( const FormalRTESubstitution < SymbolType > & other ) const {
	auto first = ext::tie ( getLeftElement ( ), getRightElement ( ), getSubstitutionSymbol ( ) );
	auto second = ext::tie ( other.getLeftElement ( ), other.getRightElement ( ), other.getSubstitutionSymbol ( ) );

	return first <=> second;
}

template < class SymbolType >
bool FormalRTESubstitution < SymbolType >::operator == ( const FormalRTESubstitution < SymbolType > & other ) const {
	auto first = ext::tie ( getLeftElement ( ), getRightElement ( ), getSubstitutionSymbol ( ) );
	auto second = ext::tie ( other.getLeftElement ( ), other.getRightElement ( ), other.getSubstitutionSymbol ( ) );

	return first == second;
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(FormalRTESubstitution";
	out << " " << m_substitutionSymbol;
	out << " " << getLeftElement ( );
	out << " " << getRightElement ( );
	out << ")";
}

template < class SymbolType >
bool FormalRTESubstitution < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const {
	if ( getLeftElement ( ).testSymbol ( symbol ) ) return true;

	if ( getRightElement ( ).testSymbol ( symbol ) ) return true;

	if ( m_substitutionSymbol.getSymbol ( ) == symbol ) return true;

	return false;
}

template < class SymbolType >
void FormalRTESubstitution < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	alphabetK.insert ( m_substitutionSymbol.getSymbol ( ) );
	getLeftElement ( ).computeMinimalAlphabet ( alphabetF, alphabetK );
	getRightElement ( ).computeMinimalAlphabet ( alphabetF, alphabetK );
}

template < class SymbolType >
bool FormalRTESubstitution < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	return alphabetK.count ( getSubstitutionSymbol ( ).getSymbol ( ) ) > 0 && getLeftElement ( ).checkAlphabet ( alphabetF, alphabetK ) && getRightElement ( ).checkAlphabet ( alphabetF, alphabetK );
}

} /* namespace rte */

extern template class rte::FormalRTESubstitution < DefaultSymbolType >;

