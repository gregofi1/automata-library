#include "DFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::DFTA < > > ( );

} /* namespace */
