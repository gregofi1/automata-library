#include "EpsilonNFTA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::EpsilonNFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::EpsilonNFTA < > > ( );

auto EpsilonNFTAFromDFTA = registration::CastRegister < automaton::EpsilonNFTA < >, automaton::DFTA < > > ( );
auto EpsilonNFTAFromNFTA = registration::CastRegister < automaton::EpsilonNFTA < >, automaton::NFTA < > > ( );

} /* namespace */
