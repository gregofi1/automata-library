#include "UnorderedDFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::UnorderedDFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::UnorderedDFTA < > > ( );

} /* namespace */
