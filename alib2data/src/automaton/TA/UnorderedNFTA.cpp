#include "UnorderedNFTA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::UnorderedNFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::UnorderedNFTA < > > ( );

auto NFTAFromDFTA = registration::CastRegister < automaton::UnorderedNFTA < >, automaton::UnorderedDFTA < > > ( );

} /* namespace */
