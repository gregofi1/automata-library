#include "NondeterministicZAutomaton.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NondeterministicZAutomaton < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NondeterministicZAutomaton < > > ( );

} /* namespace */
