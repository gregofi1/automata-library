#include "ArcFactoredNondeterministicZAutomaton.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::ArcFactoredNondeterministicZAutomaton < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ArcFactoredNondeterministicZAutomaton < > > ( );

auto ArcFactoredNondeterministicZAutomatonFromArcFactoredDeterministicZAutomaton = registration::CastRegister < automaton::ArcFactoredNondeterministicZAutomaton < >, automaton::ArcFactoredDeterministicZAutomaton < > > ( );

} /* namespace */
