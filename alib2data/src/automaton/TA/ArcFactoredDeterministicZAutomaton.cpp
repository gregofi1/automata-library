#include "ArcFactoredDeterministicZAutomaton.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::ArcFactoredDeterministicZAutomaton < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ArcFactoredDeterministicZAutomaton < > > ( );

} /* namespace */
