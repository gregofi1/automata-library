#pragma once

#include <type_traits>

namespace automaton {

/**
 * \brief Wrapper around any automaton type.
 */
class Automaton;

} /* namespace automaton */

