#include "NPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NPDA < > > ( );

} /* namespace */
