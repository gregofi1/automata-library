#include "VisiblyPushdownDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::VisiblyPushdownDPDA < > > ( );

} /* namespace */
