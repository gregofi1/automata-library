/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <ext/algorithm>

#include <alib/multimap>
#include <alib/set>
#include <alib/vector>

#include <core/components.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <core/normalize.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonNormalize.h>

namespace automaton {

class InputAlphabet;
class PushdownStoreAlphabet;
class BottomOfTheStackSymbol;
class States;
class FinalStates;
class InitialStates;

/**
 * \brief
 * Nondeterministic real time height deterministic pushdown automaton. Accepts subset of context free languages.

 * \details
 * Definition
 * A = (Q, T, G, I, Z, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols
 * G (PushdownStoreAlphabet) = finite set of pushdown store symbol
 * I (InitialState) = initial state,
 * Z (BottomOfTheStackSymbol) = initial pushdown store symbol
 * \delta is split to three disjoint parts
 *   - \delta_{call} of the form A \times a -> B \times g, where A, B \in Q, a \in T \cup { \eps }, and g \in G
 *   - \delta_{return} of the form A \times a \times g -> B, where A, B \in Q, a \in T \cup { \eps }, and g \in G
 *   - \delta_{local} of the form A \times a -> B, where A, B \in Q, a \in T \cup { \eps }
 * F (FinalStates) = set of final states
 *
 * \tparam InputSymbolTypeT used for the terminal alphabet
 * \tparam PushdownSymbolTypeT used for the pushdown store alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template < class InputSymbolTypeT = DefaultSymbolType, class PushdownStoreSymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType >
class RealTimeHeightDeterministicNPDA final : public core::Components < RealTimeHeightDeterministicNPDA < InputSymbolTypeT, PushdownStoreSymbolTypeT, StateTypeT >, ext::set < InputSymbolTypeT >, component::Set, InputAlphabet, ext::set < PushdownStoreSymbolTypeT >, component::Set, PushdownStoreAlphabet, PushdownStoreSymbolTypeT, component::Value, BottomOfTheStackSymbol, ext::set < StateTypeT >, component::Set, std::tuple < States, InitialStates, FinalStates > > {
public:
	typedef InputSymbolTypeT InputSymbolType;
	typedef PushdownStoreSymbolTypeT PushdownStoreSymbolType;
	typedef StateTypeT StateType;

private:
	/**
	 * Call transition function as mapping from a state times an input symbol or epsilon on the left hand side to a state times pushdown store symbol on the right hand side.
	 */
	ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > callTransitions;

	/**
	 * Return transition function as mapping from a state times an input symbol or epsilon times pushdown store symbol on the left hand side to a state on the right hand side.
	 */
	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > returnTransitions;

	/**
	 * Local transition function as mapping from a state times an input symbol or epsilon on the left hand side to a state on the right hand side.
	 */
	ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > localTransitions;

public:
	/**
	 * \brief Creates a new instance of the automaton with a concrete set of states, call, return, and local alphabets, pushdown store alphabet, initial state, bottom of the stack symbol, and a set of final states.
	 *
	 * \param states the initial set of states of the automaton
	 * \param callAlphabet the initial input alphabet
	 * \param returnAlphabet the initial input alphabet
	 * \param localAlphabet the initial input alphabet
	 * \param pushdownStoreAlphabet the initial set of symbols used in the pushdown store by the automaton
	 * \param initialState the initial state of the automaton
	 * \param bottomOfTheStackSymbol the initial pushdown symbol of the automaton
	 * \param finalStates the initial set of final states of the automaton
	 */
	explicit RealTimeHeightDeterministicNPDA ( ext::set < StateType > states, ext::set < InputSymbolType > inputAlphabet, ext::set < PushdownStoreSymbolType > pushdownStoreAlphabet, ext::set < StateType > initialStates, PushdownStoreSymbolType bottomOfTheStackSymbol, ext::set < StateType > finalStates );

	/**
	 * \brief Creates a new instance of the automaton with a concrete initial state and bottom of the stack symbol.
	 *
	 * \param initialState the initial state of the automaton
	 * \param bottomOfTheStackSymbol the bottom of the stack symbol of the automaton
	 */
	explicit RealTimeHeightDeterministicNPDA ( PushdownStoreSymbolType bottomOfTheStackSymbol );

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	const ext::set < StateType > & getStates ( ) const & {
		return this-> template accessComponent < States > ( ).get ( );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	ext::set < StateType > && getStates ( ) && {
		return std::move ( this-> template accessComponent < States > ( ).get ( ) );
	}

	/**
	 * Adder of a state.
	 *
	 * \param state the new state to be added to a set of states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addState ( StateType state ) {
		return this-> template accessComponent < States > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of states.
	 *
	 * \param states completely new set of states
	 */
	void setStates ( ext::set < StateType > states ) {
		this-> template accessComponent < States > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a state.
	 *
	 * \param state a state to be removed from a set of states
	 *
	 * \returns true if the state was indeed removed
	 */
	bool removeState ( const StateType & state ) {
		return this-> template accessComponent < States > ( ).remove ( state );
	}

	/**
	 * Getter of initial state.
	 *
	 * \returns the states of the automaton
	 */
	const ext::set < StateType > & getInitialStates ( ) const & {
		return this->template accessComponent < InitialStates > ( ).get ( );
	}

	/**
	 * Getter of initial state.
	 *
	 * \returns the states of the automaton
	 */
	ext::set < StateType > && getInitialStates ( ) && {
		return std::move ( this->template accessComponent < InitialStates > ( ).get ( ) );
	}

	/**
	 * Adder of an initial state.
	 *
	 * \param state the new state to be added to a set of initial states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addInitialState ( StateType state ) {
		return this->template accessComponent < InitialStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of initial states.
	 *
	 * \param states completely new set of initial states
	 */
	void setInitialStates ( ext::set < StateType > states ) {
		this->template accessComponent < InitialStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of an initial state.
	 *
	 * \param state a state to be removed from a set of initial states
	 *
	 * \returns true if the state was indeed removed
	 */
	bool removeInitialState ( const StateType & state ) {
		return this->template accessComponent < InitialStates > ( ).remove ( state );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	const ext::set < StateType > & getFinalStates ( ) const & {
		return this-> template accessComponent < FinalStates > ( ).get ( );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	ext::set < StateType > && getFinalStates ( ) && {
		return std::move ( this-> template accessComponent < FinalStates > ( ).get ( ) );
	}

	/**
	 * Adder of a final state.
	 *
	 * \param state the new state to be added to a set of final states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addFinalState ( StateType state ) {
		return this-> template accessComponent < FinalStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of final states.
	 *
	 * \param states completely new set of final states
	 */
	void setFinalStates ( ext::set < StateType > states ) {
		this-> template accessComponent < FinalStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a final state.
	 *
	 * \param state a state to be removed from a set of final states
	 *
	 * \returns true if the state was indeed removed
	 */
	bool removeFinalState ( const StateType & state ) {
		return this-> template accessComponent < FinalStates > ( ).remove ( state );
	}

	/**
	 * Getter of the pushdown store alphabet.
	 *
	 * \returns the pushdown store alphabet of the automaton
	 */
	const ext::set < PushdownStoreSymbolType > & getPushdownStoreAlphabet ( ) const & {
		return this->template accessComponent < PushdownStoreAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the pushdown store alphabet.
	 *
	 * \returns the pushdown store alphabet of the automaton
	 */
	ext::set < PushdownStoreSymbolType > && getPushdownStoreAlphabet ( ) && {
		return std::move ( this->template accessComponent < PushdownStoreAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a pushdown store symbol.
	 *
	 * \param symbol the new symbol to be added to a pushdown store alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addPushdownStoreSymbol ( PushdownStoreSymbolType symbol ) {
		return this->template accessComponent < PushdownStoreAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder of pushdown store symbols.
	 *
	 * \param symbols new symbols to be added to a pushdown store alphabet
	 */
	void addPushdownStoreSymbols ( ext::set < PushdownStoreSymbolType > symbols ) {
		this->template accessComponent < PushdownStoreAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of a pushdown store alphabet.
	 *
	 * \param symbols completely new pushdown store alphabet
	 */
	void setPushdownStoreAlphabet ( ext::set < PushdownStoreSymbolType > symbols ) {
		this->template accessComponent < PushdownStoreAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an pushdown store symbol.
	 *
	 * \param symbol a symbol to be removed from a pushdown store alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	bool removePushdownStoreSymbol ( const PushdownStoreSymbolType & symbol ) {
		return this->template accessComponent < PushdownStoreAlphabet > ( ).remove ( symbol );
	}

	/**
	 * Getter of the bottom of the stack symbol.
	 *
	 * \returns the bottom of the stack symbol of the automaton
	 */
	const PushdownStoreSymbolType & getBottomOfTheStackSymbol ( ) const & {
		return this->template accessComponent < BottomOfTheStackSymbol > ( ).get ( );
	}

	/**
	 * Getter of the bottom of the stack symbol.
	 *
	 * \returns the bottom of the stack symbol of the automaton
	 */
	PushdownStoreSymbolType && getBottomOfTheStackSymbol ( ) && {
		return std::move ( this->template accessComponent < BottomOfTheStackSymbol > ( ).get ( ) );
	}

	/**
	 * Setter of the bottom of the stack symbol.
	 *
	 * \param symbol new bottom of the stack symbol of the automaton
	 *
	 * \returns true if the bottom of the stack symbol was indeed changed
	 */
	bool setBottomOfTheStackSymbol ( PushdownStoreSymbolType symbol ) {
		return this->template accessComponent < BottomOfTheStackSymbol > ( ).set ( std::move ( symbol ) );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	const ext::set < InputSymbolType > & getInputAlphabet ( ) const & {
		return this-> template accessComponent < InputAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	ext::set < InputSymbolType > && getInputAlphabet ( ) && {
		return std::move ( this-> template accessComponent < InputAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a input symbol.
	 *
	 * \param symbol the new symbol to be added to an input alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addInputSymbol ( InputSymbolType symbol ) {
		return this-> template accessComponent < InputAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder of input symbols.
	 *
	 * \param symbols new symbols to be added to an input alphabet
	 */
	void addInputSymbols ( ext::set < InputSymbolType > symbols ) {
		this-> template accessComponent < InputAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of input alphabet.
	 *
	 * \param symbols completely new input alphabet
	 */
	void setInputAlphabet ( ext::set < InputSymbolType > symbols ) {
		this-> template accessComponent < InputAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an input symbol.
	 *
	 * \param symbol a symbol to be removed from an input alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removeInputSymbol ( const InputSymbolType & symbol ) {
		this-> template accessComponent < InputAlphabet > ( ).remove ( symbol );
	}

	/**
	 * \brief Adds a call transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in T \cup { \eps }, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addCallTransition ( StateType from, common::symbol_or_epsilon < InputSymbolType > input, StateType to, PushdownStoreSymbolType push );

	/**
	 * \brief Adds a call transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in T, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addCallTransition ( StateType from, InputSymbolType input, StateType to, PushdownStoreSymbolType push );

	/**
	 * \brief Adds a call transition to the automaton.
	 *
	 * \details The transition is in a form A \times \eps -> B \times g, where A, B \in Q, and g \in G
	 *
	 * \param from the source state (A)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addCallTransition ( StateType from, StateType to, PushdownStoreSymbolType push );

	/**
	 * \brief Adds a return transition to the automaton.
	 *
	 * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in T \cup { \eps }, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addReturnTransition ( StateType from, common::symbol_or_epsilon < InputSymbolType > input, PushdownStoreSymbolType pop, StateType to );

	/**
	 * \brief Adds a return transition to the automaton.
	 *
	 * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in T, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addReturnTransition ( StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to );

	/**
	 * \brief Adds a return transition to the automaton.
	 *
	 * \details The transition is in a form A \times \eps \times g -> B, where A, B \in Q, and g \in G
	 *
	 * \param from the source state (A)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addReturnTransition ( StateType from, PushdownStoreSymbolType pop, StateType to );

	/**
	 * \brief Adds a local transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T \cup { \eps }
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addLocalTransition ( StateType from, common::symbol_or_epsilon < InputSymbolType > input, StateType to );

	/**
	 * \brief Adds a local transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T
	 *
	 * \param from the source state (A)
	 * \param input the call input symbol (a)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addLocalTransition ( StateType from, InputSymbolType input, StateType to );

	/**
	 * \brief Adds a local transition to the automaton.
	 *
	 * \details The transition is in a form A \times \eps -> B, where A, B \in Q
	 *
	 * \param from the source state (A)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components or when the transition would cause nondeterminism
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addLocalTransition ( StateType from, StateType to );

	/**
	 * \brief Removes a call transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in T \cup { \eps }, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeCallTransition ( const StateType & from, const common::symbol_or_epsilon < InputSymbolType > & input, const StateType & to, const PushdownStoreSymbolType & push );

	/**
	 * \brief Removes a call transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in T, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeCallTransition ( const StateType & from, const InputSymbolType & input, const StateType & to, const PushdownStoreSymbolType & push );

	/**
	 * \brief Removes a call transition from the automaton.
	 *
	 * \details The transition is in a form A \times \eps -> B \times g, where A, B \in Q, and g \in G
	 *
	 * \param from the source state (A)
	 * \param to the target state (B)
	 * \param push symbol to be pushed to the pushdown store on the transition use (g)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeCallTransition ( const StateType & from, const StateType & to, const PushdownStoreSymbolType & push );

	/**
	 * \brief Removes a return transition from the automaton.
	 *
	 * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in T \cup { \eps }, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeReturnTransition ( const StateType & from, const common::symbol_or_epsilon < InputSymbolType > & input, const PushdownStoreSymbolType & pop, const StateType & to );

	/**
	 * \brief Removes a return transition from the automaton.
	 *
	 * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in T, and g \in G
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeReturnTransition ( const StateType & from, const InputSymbolType & input, const PushdownStoreSymbolType & pop, const StateType & to );

	/**
	 * \brief Removes a return transition from the automaton.
	 *
	 * \details The transition is in a form A \times \eps \times g -> B, where A, B \in Q, and g \in G
	 *
	 * \param from the source state (A)
	 * \param pop symbol to be poped to the pushdown store on the transition use (g)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeReturnTransition ( const StateType & from, const PushdownStoreSymbolType & pop, const StateType & to );

	/**
	 * \brief Removes a local transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T \cup { \eps }
	 *
	 * \param from the source state (A)
	 * \param input the input symbol or epsilon (a)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeLocalTransition ( const StateType & from, const common::symbol_or_epsilon < InputSymbolType > & input, const StateType & to );

	/**
	 * \brief Removes a local transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T
	 *
	 * \param from the source state (A)
	 * \param input the input symbol (a)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeLocalTransition ( const StateType & from, const InputSymbolType & input, const StateType & to );

	/**
	 * \brief Removes a local transition from the automaton.
	 *
	 * \details The transition is in a form A \times \eps -> B, where A, B \in Q
	 *
	 * \param from the source state (A)
	 * \param to the target state (B)
	 *
	 * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeLocalTransition ( const StateType & from, const StateType & to );

	/**
	 * Get the call transition function of the automaton in its natural form.
	 *
	 * \returns call transition function of the automaton
	 */
	const ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > & getCallTransitions ( ) const &;

	/**
	 * Get the call transition function of the automaton in its natural form.
	 *
	 * \returns call transition function of the automaton
	 */
	ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > && getCallTransitions ( ) &&;

	/**
	 * Get the return transition function of the automaton in its natural form.
	 *
	 * \returns return transition function of the automaton
	 */
	const ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > & getReturnTransitions ( ) const &;

	/**
	 * Get the return transition function of the automaton in its natural form.
	 *
	 * \returns return transition function of the automaton
	 */
	ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > && getReturnTransitions ( ) &&;

	/**
	 * Get the local transition function of the automaton in its natural form.
	 *
	 * \returns local transition function of the automaton
	 */
	const ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > & getLocalTransitions ( ) const &;

	/**
	 * Get the local transition function of the automaton in its natural form.
	 *
	 * \returns local transition function of the automaton
	 */
	ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > && getLocalTransitions ( ) &&;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const RealTimeHeightDeterministicNPDA & other ) const {
		return std::tie(getStates(), getInputAlphabet(), getInitialStates(), getFinalStates(), getPushdownStoreAlphabet(), getBottomOfTheStackSymbol(), callTransitions, returnTransitions, localTransitions) <=> std::tie(other.getStates(), other.getInputAlphabet(), other.getInitialStates(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getBottomOfTheStackSymbol(), other.callTransitions, other.returnTransitions, other.localTransitions);
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const RealTimeHeightDeterministicNPDA & other ) const {
		return std::tie(getStates(), getInputAlphabet(), getInitialStates(), getFinalStates(), getPushdownStoreAlphabet(), getBottomOfTheStackSymbol(), callTransitions, returnTransitions, localTransitions) == std::tie(other.getStates(), other.getInputAlphabet(), other.getInitialStates(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getBottomOfTheStackSymbol(), other.callTransitions, other.returnTransitions, other.localTransitions);
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const RealTimeHeightDeterministicNPDA & instance ) {
		return out << "(RealTimeHeightDeterministicNPDA"
			   << " states = " << instance.getStates ( )
			   << " inputAlphabet = " << instance.getInputAlphabet ( )
			   << " initialStates = " << instance.getInitialStates ( )
			   << " finalStates = " << instance.getFinalStates ( )
			   << " pushdownStoreAlphabet = " << instance.getPushdownStoreAlphabet ( )
			   << " bottomOfTheStackSymbol = " << instance.getBottomOfTheStackSymbol ( )
			   << " callTransitions = " << instance.getCallTransitions ( )
			   << " returnTransitions = " << instance.getReturnTransitions ( )
			   << " localTransitions = " << instance.getLocalTransitions ( )
			   << ")";
	}
};

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::RealTimeHeightDeterministicNPDA ( ext::set < StateType > states, ext::set < InputSymbolType > inputAlphabet, ext::set < PushdownStoreSymbolType > pushdownStoreAlphabet, ext::set < StateType > initialStates, PushdownStoreSymbolType bottomOfTheStackSymbol, ext::set < StateType > finalStates ) : core::Components < RealTimeHeightDeterministicNPDA, ext::set < InputSymbolType >, component::Set, InputAlphabet, ext::set < PushdownStoreSymbolType >, component::Set, PushdownStoreAlphabet, PushdownStoreSymbolType, component::Value, BottomOfTheStackSymbol, ext::set < StateType >, component::Set, std::tuple < States, InitialStates, FinalStates > > ( std::move ( inputAlphabet ), std::move ( pushdownStoreAlphabet ), std::move ( bottomOfTheStackSymbol ), std::move ( states ), std::move ( initialStates ), std::move ( finalStates ) ) {
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::RealTimeHeightDeterministicNPDA(PushdownStoreSymbolType bottomOfTheStackSymbol) : RealTimeHeightDeterministicNPDA ( ext::set < StateType > { }, ext::set < InputSymbolType > { }, ext::set < PushdownStoreSymbolType > { bottomOfTheStackSymbol }, ext::set < StateType > {}, bottomOfTheStackSymbol, ext::set < StateType > { }) {
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addCallTransition(StateType from, common::symbol_or_epsilon < InputSymbolType > input, StateType to, PushdownStoreSymbolType push) {
	if (! getStates().count(from)) {
		throw AutomatonException("State \"" + ext::to_string ( from ) + "\" doesn't exist.");
	}

	if ( ! input.is_epsilon ( ) && ! getInputAlphabet().count(input.getSymbol ( ))) {
		throw AutomatonException("Input symbol \"" + ext::to_string ( input ) + "\" doesn't exist.");
	}

	if (! getStates().count(to)) {
		throw AutomatonException("State \"" + ext::to_string ( to ) + "\" doesn't exist.");
	}

	if (! getPushdownStoreAlphabet().count(push)) {
		throw AutomatonException("Pushdown store symbol \"" + ext::to_string ( push ) + "\" doesn't exist.");
	}

	auto upper_bound = callTransitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = callTransitions.lower_bound ( ext::tie ( from, input ) );

	ext::pair < StateType, PushdownStoreSymbolType > value = ext::make_pair ( std::move ( to ), std::move ( push ) );

	auto iter = std::lower_bound ( lower_bound, upper_bound, value, [ ] ( const auto & transition, const auto & target ) { return transition.second < target; } );
	if ( iter != upper_bound && value >= iter->second )
		return false;

	ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > > key ( std::move ( from ), std::move ( input ) );

	callTransitions.insert ( iter, std::make_pair ( std::move ( key ), std::move ( value ) ) );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addCallTransition(StateType from, StateType to, PushdownStoreSymbolType push) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return addCallTransition(std::move(from), std::move(inputVariant), std::move(to), std::move(push));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addCallTransition(StateType from, InputSymbolType input, StateType to, PushdownStoreSymbolType push) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(input);
	return addCallTransition(std::move(from), std::move(inputVariant), std::move(to), std::move(push));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addReturnTransition(StateType from, common::symbol_or_epsilon < InputSymbolType > input, PushdownStoreSymbolType pop, StateType to) {
	if (! getStates().count(from)) {
		throw AutomatonException("State \"" + ext::to_string ( from ) + "\" doesn't exist.");
	}

	if ( ! input.is_epsilon ( ) && !getInputAlphabet().count(input.getSymbol ( ))) {
		throw AutomatonException("Input symbol \"" + ext::to_string ( input ) + "\" doesn't exist.");
	}

	if (! getStates().count(to)) {
		throw AutomatonException("State \"" + ext::to_string ( to ) + "\" doesn't exist.");
	}

	if (! getPushdownStoreAlphabet().count(pop)) {
		throw AutomatonException("Pushdown store symbol \"" + ext::to_string ( pop ) + "\" doesn't exist.");
	}

	auto upper_bound = returnTransitions.upper_bound ( ext::tie ( from, input, pop ) );
	auto lower_bound = returnTransitions.lower_bound ( ext::tie ( from, input, pop ) );

	auto iter = std::lower_bound ( lower_bound, upper_bound, to, [ ] ( const auto & transition, const auto & target ) { return transition.second < target; } );
	if ( iter != upper_bound && to >= iter->second )
		return false;

	ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType > key ( std::move ( from ), std::move ( input ), std::move ( pop ) );

	returnTransitions.insert ( iter, std::make_pair ( std::move ( key ), std::move ( to ) ) );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addReturnTransition(StateType from, PushdownStoreSymbolType pop, StateType to) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return addReturnTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addReturnTransition(StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(std::move(input));
	return addReturnTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addLocalTransition(StateType from, common::symbol_or_epsilon < InputSymbolType > input, StateType to) {
	if (! getStates().count(from)) {
		throw AutomatonException("State \"" + ext::to_string ( from ) + "\" doesn't exist.");
	}

	if ( ! input.is_epsilon ( ) && ! getInputAlphabet().count(input.getSymbol ( ))) {
		throw AutomatonException("Input symbol \"" + ext::to_string ( input ) + "\" doesn't exist.");
	}

	if (! getStates().count(to)) {
		throw AutomatonException("State \"" + ext::to_string ( to ) + "\" doesn't exist.");
	}

	auto upper_bound = localTransitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = localTransitions.lower_bound ( ext::tie ( from, input ) );

	auto iter = std::lower_bound ( lower_bound, upper_bound, to, [ ] ( const auto & transition, const auto & target ) { return transition.second < target; } );
	if ( iter != upper_bound && to >= iter->second )
		return false;

	ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > > key ( std::move ( from ), std::move ( input ) );

	localTransitions.insert ( iter, std::make_pair ( std::move ( key ), std::move ( to ) ) );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addLocalTransition(StateType from, StateType to) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return addLocalTransition(std::move(from), std::move(inputVariant), std::move(to));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::addLocalTransition(StateType from, InputSymbolType input, StateType to) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(std::move(input));
	return addLocalTransition(std::move(from), std::move(inputVariant), std::move(to));
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeCallTransition(const StateType& from, const common::symbol_or_epsilon < InputSymbolType >& input, const StateType& to, const PushdownStoreSymbolType& push) {
	auto upper_bound = callTransitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = callTransitions.lower_bound ( ext::tie ( from, input ) );
	auto iter = std::find_if ( lower_bound, upper_bound, [ & ] ( const auto & transition ) { return transition.second.first == to && transition.second.second == push; } );
	if ( iter == upper_bound )
		return false;

	callTransitions.erase ( iter );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeCallTransition(const StateType& from, const StateType& to, const PushdownStoreSymbolType& push) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return removeCallTransition(from, inputVariant, to, push);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeCallTransition(const StateType& from, const InputSymbolType& input, const StateType& to, const PushdownStoreSymbolType& push) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(input);
	return removeCallTransition(from, inputVariant, to, push);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeReturnTransition(const StateType& from, const common::symbol_or_epsilon < InputSymbolType >& input, const PushdownStoreSymbolType& pop, const StateType& to) {
	auto upper_bound = returnTransitions.upper_bound ( ext::tie ( from, input, pop ) );
	auto lower_bound = returnTransitions.lower_bound ( ext::tie ( from, input, pop ) );
	auto iter = std::find_if ( lower_bound, upper_bound, [ & ] ( const auto & transition ) { return transition.second == to; } );
	if ( iter == upper_bound )
		return false;

	returnTransitions.erase ( iter );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeReturnTransition(const StateType& from, const PushdownStoreSymbolType& pop, const StateType& to) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return removeReturnTransition(from, inputVariant, pop, to);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeReturnTransition(const StateType& from, const InputSymbolType& input, const PushdownStoreSymbolType& pop, const StateType& to) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(input);
	return removeReturnTransition(from, inputVariant, pop, to);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeLocalTransition(const StateType& from, const common::symbol_or_epsilon < InputSymbolType >& input, const StateType& to) {
	auto upper_bound = localTransitions.upper_bound ( ext::tie ( from, input ) );
	auto lower_bound = localTransitions.lower_bound ( ext::tie ( from, input ) );
	auto iter = std::find_if ( lower_bound, upper_bound, [ & ] ( const auto & transition ) { return transition.second == to; } );
	if ( iter == upper_bound )
		return false;

	localTransitions.erase ( iter );
	return true;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeLocalTransition(const StateType& from, const StateType& to) {
	auto inputVariant = common::symbol_or_epsilon < InputSymbolType > ( );
	return removeLocalTransition(from, inputVariant, to);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
bool RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::removeLocalTransition(const StateType& from, const InputSymbolType& input, const StateType& to) {
	common::symbol_or_epsilon < InputSymbolType > inputVariant(input);
	return removeLocalTransition(from, inputVariant, to);
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
const ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > & RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getCallTransitions ( ) const & {
	return callTransitions;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > && RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getCallTransitions ( ) && {
	return std::move ( callTransitions );
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
const ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > & RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getReturnTransitions ( ) const & {
	return returnTransitions;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
ext::multimap < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > && RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getReturnTransitions ( ) && {
	return std::move ( returnTransitions );
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
const ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > & RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getLocalTransitions ( ) const & {
	return localTransitions;
}

template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
ext::multimap < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > && RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >::getLocalTransitions ( ) && {
	return std::move ( localTransitions );
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, InputSymbolType, automaton::InputAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const InputSymbolType & symbol ) {
		for ( const std::pair<const ext::pair<StateType, common::symbol_or_epsilon < InputSymbolType >>, ext::pair < StateType, PushdownStoreSymbolType > >& callTransition : automaton.getCallTransitions())
			if ( ! callTransition.first.second.is_epsilon ( ) && symbol == callTransition.first.second.getSymbol ( ))
				return true;

		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType>, StateType >& returnTransition : automaton.getReturnTransitions())
			if ( ! std::get<1>(returnTransition.first).is_epsilon ( ) && symbol == std::get<1>(returnTransition.first).getSymbol ( ))
				return true;

		for ( const std::pair<const ext::pair<StateType, common::symbol_or_epsilon < InputSymbolType >>, StateType >& localTransition : automaton.getLocalTransitions())
			if ( ! localTransition.first.second.is_epsilon ( ) && symbol == localTransition.first.second.getSymbol ( ))
				return true;

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the input alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const InputSymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as input symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const InputSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, PushdownStoreSymbolType, automaton::PushdownStoreAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const PushdownStoreSymbolType & symbol ) {
		if(automaton.getBottomOfTheStackSymbol() == symbol)
			return true;

		for ( const std::pair<const ext::pair<StateType, common::symbol_or_epsilon < InputSymbolType >>, ext::pair < StateType, PushdownStoreSymbolType > >& callTransition : automaton.getCallTransitions())
			if (symbol == callTransition.second.second)
				return true;

		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType>, StateType >& returnTransition : automaton.getReturnTransitions())
			if (symbol == std::get<2>(returnTransition.first))
				return true;

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the pushdown store alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as pushdown store symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal bottom of the stack symbol element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class ElementConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, PushdownStoreSymbolType, automaton::BottomOfTheStackSymbol > {
public:
	/**
	 * Determines whether the bottom of the stack symbol is available in the automaton's pushdown store alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the pushdown store symbol is already in the pushdown store alphabet of the automaton
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const PushdownStoreSymbolType & symbol ) {
		return automaton.template accessComponent < automaton::PushdownStoreAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * All pushdown store symbols are valid as a bottom of the stack symbol of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const PushdownStoreSymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::States > {
public:
	/**
	 * Returns true if the state is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is used, false othervise
	 */
	static bool used ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		if ( automaton.getInitialStates ( ).count ( state ) )
			return true;

		if ( automaton.getFinalStates ( ).count ( state ) )
			return true;

		for ( const std::pair<const ext::pair<StateType, common::symbol_or_epsilon < InputSymbolType >>, ext::pair<StateType, PushdownStoreSymbolType > >& callTransition : automaton.getCallTransitions()) {
			if (state == callTransition.first.first)
				return true;

			if(callTransition.second.first == state)
				return true;
		}

		for ( const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType>, StateType >& returnTransition : automaton.getReturnTransitions()) {
			if (state == std::get<0>(returnTransition.first))
				return true;

			if(returnTransition.second == state)
				return true;
		}

		for ( const std::pair<const ext::pair<StateType, common::symbol_or_epsilon < InputSymbolType >>, StateType >& localTransition : automaton.getLocalTransitions()) {
			if (state == localTransition.first.first)
				return true;

			if(localTransition.second == state)
				return true;
		}

		return false;
	}

	/**
	 * Returns true as all states are possibly available to be elements of the states.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
		return true;
	}

	/**
	 * All states are valid as a state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::FinalStates > {
public:
	/**
	 * Returns false. Final state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as a final state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal initial component element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
class SetConstraint< automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType >, StateType, automaton::InitialStates > {
public:
	/**
	 * Returns false. Initial state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < automaton::States > ( ).get ( ).count ( state );
	}

	/**
	 * All states are valid as an initial state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the automaton with default template parameters or unmodified instance if the template parameters were already the default ones
 */
template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
struct normalize < automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > > {
	static automaton::RealTimeHeightDeterministicNPDA < > eval ( automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getInputAlphabet ( ) );
		ext::set < DefaultSymbolType > pushdownAlphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getPushdownStoreAlphabet ( ) );
		DefaultSymbolType bottomOfTheStackSymbol = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( value ).getBottomOfTheStackSymbol ( ) );
		ext::set < DefaultStateType > states = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getStates ( ) );
		ext::set < DefaultStateType > initialStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getInitialStates ( ) );
		ext::set < DefaultStateType > finalStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getFinalStates ( ) );

		automaton::RealTimeHeightDeterministicNPDA < > res ( std::move ( states ), std::move ( alphabet ), std::move ( pushdownAlphabet ), std::move ( initialStates ), std::move ( bottomOfTheStackSymbol ), std::move ( finalStates ) );

		for ( std::pair < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, ext::pair < StateType, PushdownStoreSymbolType > > && transition : ext::make_mover ( std::move ( value ).getCallTransitions ( ) ) ) {
			DefaultStateType to = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.second.first ) );
			DefaultSymbolType push = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( transition.second.second ) );

			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.first.first ) );
			common::symbol_or_epsilon < DefaultSymbolType > input = automaton::AutomatonNormalize::normalizeSymbolEpsilon ( std::move ( transition.first.second ) );
			res.addCallTransition ( std::move ( from ), std::move ( input ), std::move ( to ), std::move ( push ) );
		}

		for ( std::pair < ext::tuple < StateType, common::symbol_or_epsilon < InputSymbolType >, PushdownStoreSymbolType >, StateType > && transition : ext::make_mover ( std::move ( value ).getReturnTransitions ( ) ) ) {
			DefaultStateType to = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.second ) );

			DefaultSymbolType pop = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( std::get < 2 > ( transition.first ) ) );
			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( std::get < 0 > ( transition.first ) ) );
			common::symbol_or_epsilon < DefaultSymbolType > input = automaton::AutomatonNormalize::normalizeSymbolEpsilon ( std::move ( std::get < 1 > ( transition.first ) ) );

			res.addReturnTransition ( std::move ( from ), std::move ( input ), std::move ( pop ), std::move ( to ) );
		}

		for ( std::pair < ext::pair < StateType, common::symbol_or_epsilon < InputSymbolType > >, StateType > && transition : ext::make_mover ( std::move ( value ).getLocalTransitions ( ) ) ) {
			DefaultStateType to = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.second ) );

			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.first.first ) );
			common::symbol_or_epsilon < DefaultSymbolType > input = automaton::AutomatonNormalize::normalizeSymbolEpsilon ( std::move ( transition.first.second ) );

			res.addLocalTransition ( std::move ( from ), std::move ( input ), std::move ( to ) );
		}

		return res;
	}
};

} /* namespace core */

extern template class automaton::RealTimeHeightDeterministicNPDA < >;

