#include "RealTimeHeightDeterministicDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::RealTimeHeightDeterministicDPDA < > > ( );

} /* namespace */
