#include "SinglePopDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::SinglePopDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::SinglePopDPDA < > > ( );

} /* namespace */
