#include "RealTimeHeightDeterministicNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::RealTimeHeightDeterministicNPDA < > > ( );

} /* namespace */
