#include "VisiblyPushdownNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::VisiblyPushdownNPDA < > > ( );

} /* namespace */
