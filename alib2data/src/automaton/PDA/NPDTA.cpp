#include "NPDTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NPDTA < > > ( );

} /* namespace */
