#include "DPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::DPDA < > > ( );

} /* namespace */
