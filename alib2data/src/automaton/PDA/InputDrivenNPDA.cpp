#include "InputDrivenNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::InputDrivenNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::InputDrivenNPDA < > > ( );

} /* namespace */
