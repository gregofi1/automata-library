#include "OneTapeDTM.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::OneTapeDTM < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::OneTapeDTM < > > ( );

} /* namespace */
