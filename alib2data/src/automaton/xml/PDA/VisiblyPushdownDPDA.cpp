#include "VisiblyPushdownDPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::VisiblyPushdownDPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::VisiblyPushdownDPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::VisiblyPushdownDPDA < > > ( );

} /* namespace */
