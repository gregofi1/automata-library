#include "RealTimeHeightDeterministicDPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::RealTimeHeightDeterministicDPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::RealTimeHeightDeterministicDPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::RealTimeHeightDeterministicDPDA < > > ( );

} /* namespace */
