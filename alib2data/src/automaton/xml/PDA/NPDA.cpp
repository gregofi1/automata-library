#include "NPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::NPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::NPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::NPDA < > > ( );

} /* namespace */
