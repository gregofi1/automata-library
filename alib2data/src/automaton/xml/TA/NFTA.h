/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/xmlApi.hpp>
#include <automaton/TA/NFTA.h>
#include "../common/AutomatonFromXMLParser.h"
#include "../common/AutomatonToXMLComposer.h"

namespace core {

template < class SymbolType, class StateType >
struct xmlApi < automaton::NFTA < SymbolType, StateType > > {
	/**
	 * \brief The XML tag name of class.
	 *
	 * \details Intentionaly a static member function to be safe in the initialisation before the main function starts.
	 *
	 * \returns string representing the XML tag name of the class
	 */
	static std::string xmlTagName() {
		return "NFTA";
	}

	/**
	 * \brief Tests whether the token stream starts with this type
	 *
	 * \params input the iterator to sequence of xml tokens to test
	 *
	 * \returns true if the token stream iterator points to opening tag named with xml tag name of this type, false otherwise.
	 */
	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the automaton
	 */
	static automaton::NFTA < SymbolType, StateType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Helper for parsing of individual transitions of the automaton from a sequence of xml tokens.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 * \params automaton the automaton to add the rule to
	 */
	static void parseTransition ( ext::deque < sax::Token >::iterator & input, automaton::NFTA < SymbolType, StateType > & automaton );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param out the sink for new xml tokens representing the automaton
	 * \param automaton the automaton to compose
	 */
	static void compose ( ext::deque < sax::Token > & out, const automaton::NFTA < SymbolType, StateType > & automaton );

	/**
	 * Helper for composing transitions of the automaton to a sequence of xml tokens.
	 *
	 * \param out the sink for xml tokens representing the rules of the automaton
	 * \param automaton the automaton to compose
	 */
	static void composeTransitions ( ext::deque < sax::Token > & out, const automaton::NFTA < SymbolType, StateType > & automaton );
};

template < class SymbolType, class StateType >
automaton::NFTA < SymbolType, StateType > xmlApi < automaton::NFTA < SymbolType, StateType > >::parse(ext::deque<sax::Token>::iterator& input) {
	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	ext::set<StateType> states = automaton::AutomatonFromXMLParser::parseStates<StateType>(input);
	ext::set<common::ranked_symbol < SymbolType >> inputSymbols = automaton::AutomatonFromXMLParser::parseRankedInputAlphabet<common::ranked_symbol < SymbolType >>(input);
	ext::set<StateType> finalStates = automaton::AutomatonFromXMLParser::parseFinalStates<StateType>(input);

	automaton::NFTA < SymbolType, StateType > automaton;
	automaton.setStates(std::move(states));
	automaton.setInputAlphabet(std::move(inputSymbols));
	automaton.setFinalStates(std::move(finalStates));

	automaton::AutomatonFromXMLParser::parseTransitions ( input, automaton );

	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return automaton;
}

template < class SymbolType, class StateType >
void xmlApi < automaton::NFTA < SymbolType, StateType > >::parseTransition(ext::deque<sax::Token>::iterator& input, automaton::NFTA < SymbolType, StateType > & automaton) {
	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, "transition");
	common::ranked_symbol < SymbolType > inputSymbol = automaton::AutomatonFromXMLParser::parseTransitionInputSymbol<common::ranked_symbol < SymbolType >>(input);
	ext::vector<StateType> from = automaton::AutomatonFromXMLParser::parseTransitionFromMultiple<StateType>(input);
	StateType to = automaton::AutomatonFromXMLParser::parseTransitionTo<StateType>(input);
	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, "transition");

	automaton.addTransition(std::move(inputSymbol), std::move(from), std::move(to));
}

template < class SymbolType, class StateType >
void xmlApi < automaton::NFTA < SymbolType, StateType > >::compose(ext::deque<sax::Token>& out, const automaton::NFTA < SymbolType, StateType > & automaton ) {
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );

	automaton::AutomatonToXMLComposer::composeStates(out, automaton.getStates());
	automaton::AutomatonToXMLComposer::composeRankedInputAlphabet(out, automaton.getInputAlphabet());
	automaton::AutomatonToXMLComposer::composeFinalStates(out, automaton.getFinalStates());
	composeTransitions(out, automaton);

	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

template < class SymbolType, class StateType >
void xmlApi < automaton::NFTA < SymbolType, StateType > >::composeTransitions(ext::deque<sax::Token>& out, const automaton::NFTA < SymbolType, StateType > & automaton ) {
	out.emplace_back(sax::Token("transitions", sax::Token::TokenType::START_ELEMENT));
	for(const auto& transition : automaton.getTransitions()) {
		out.emplace_back(sax::Token("transition", sax::Token::TokenType::START_ELEMENT));

		automaton::AutomatonToXMLComposer::composeTransitionInputSymbol(out, transition.first.first);
		automaton::AutomatonToXMLComposer::composeTransitionFrom(out, transition.first.second);
		automaton::AutomatonToXMLComposer::composeTransitionTo(out, transition.second);

		out.emplace_back(sax::Token("transition", sax::Token::TokenType::END_ELEMENT));
	}

	out.emplace_back(sax::Token("transitions", sax::Token::TokenType::END_ELEMENT));
}

} /* namespace core */

