#include "MultiInitialStateNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::MultiInitialStateNFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::MultiInitialStateNFA < > > ( );

auto multiInitialStateNFAFromDFA = registration::CastRegister < automaton::MultiInitialStateNFA < >, automaton::DFA < > > ( );
auto multiInitialStateNFAFromNFA = registration::CastRegister < automaton::MultiInitialStateNFA < >, automaton::NFA < > > ( );

} /* namespace */
