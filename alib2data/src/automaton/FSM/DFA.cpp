#include "DFA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::DFA < > > ( );

} /* namespace */
