#include "MultiInitialStateEpsilonNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::MultiInitialStateEpsilonNFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::MultiInitialStateEpsilonNFA < > > ( );

auto multiInitialStateEpsilonNFAFromDFA = registration::CastRegister < automaton::MultiInitialStateEpsilonNFA < >, automaton::DFA < > > ( );
auto multiInitialStateEpsilonNFAFromNFA = registration::CastRegister < automaton::MultiInitialStateEpsilonNFA < >, automaton::NFA < > > ( );
auto multiInitialStateEpsilonNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::MultiInitialStateEpsilonNFA < >, automaton::MultiInitialStateNFA < > > ( );
auto multiInitialStateEpsilonNFAFromEpsilonNFA = registration::CastRegister < automaton::MultiInitialStateEpsilonNFA < >, automaton::EpsilonNFA < > > ( );

} /* namespace */
