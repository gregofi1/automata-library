#include "CompactDFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class automaton::CompactDFA < >;

namespace {

auto components = registration::ComponentRegister < automaton::CompactDFA < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < automaton::CompactDFA < > > ( );

auto compactNFAFromDFA = registration::CastRegister < automaton::CompactDFA < >, automaton::DFA < > > ( );

} /* namespace */
