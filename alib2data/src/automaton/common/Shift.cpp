#include "Shift.h"
#include "../AutomatonException.h"

namespace automaton {

automaton::Shift shiftFromString ( const std::string & shift ) {
	if ( shift == "LEFT" )
		return automaton::Shift::LEFT;
	else if ( shift == "RIGHT" )
		return automaton::Shift::RIGHT;
	else if ( shift == "NONE" )
		return automaton::Shift::NONE;
	else
		throw AutomatonException("Invalid shift value");
}

std::string shiftToString ( automaton::Shift shift ) {
	switch(shift) {
	case automaton::Shift::LEFT:
		return "LEFT";
	case automaton::Shift::RIGHT:
		return "RIGHT";
	case automaton::Shift::NONE:
		return "NONE";
	}
	throw AutomatonException("Invalid shift value");
}

ext::ostream& operator<<(ext::ostream& out, const automaton::Shift&) {

	return out;
}

} /* namepsace automaton */
