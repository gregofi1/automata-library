#pragma once

#include <alib/string>

namespace automaton {

/**
 * Represent movement of the reading head in the Turing machine.
 */
enum class Shift {
	LEFT, RIGHT, NONE
};

std::string shiftToString ( automaton::Shift shift );
automaton::Shift shiftFromString ( const std::string & shift );

ext::ostream& operator<<(ext::ostream& out, const automaton::Shift&);

} /* namespace automaton */

