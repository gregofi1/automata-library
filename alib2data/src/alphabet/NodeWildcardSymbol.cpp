#include "NodeWildcardSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

NodeWildcardSymbol::NodeWildcardSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const NodeWildcardSymbol & ) {
	return out << "(NodeWildcardSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::NodeWildcardSymbol > ( );

} /* namespace */
