#include "BlankSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BlankSymbol::BlankSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const BlankSymbol & ) {
	return out << "(Blank symbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BlankSymbol > ( );

} /* namespace */
