#include "EndSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

EndSymbol::EndSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const EndSymbol & ) {
	return out << "(EndSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::EndSymbol > ( );

} /* namespace */
