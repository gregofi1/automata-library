#include "VariablesBarSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

VariablesBarSymbol::VariablesBarSymbol ( ) = default;

ext::ostream & operator << ( ext::ostream & out, const VariablesBarSymbol & ) {
	return out << "(Variables bar symbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::VariablesBarSymbol > ( );

} /* namespace */
