#include "StartSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

StartSymbol::StartSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const StartSymbol & ) {
	return out << "(StartSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::StartSymbol > ( );

} /* namespace */
