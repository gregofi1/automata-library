#include "NonlinearVariableSymbol.h"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::NonlinearVariableSymbol < > > ( );

} /* namespace */
