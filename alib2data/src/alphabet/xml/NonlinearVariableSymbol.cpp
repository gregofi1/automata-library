#include "NonlinearVariableSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::NonlinearVariableSymbol < > > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::NonlinearVariableSymbol < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::NonlinearVariableSymbol < > > ( );

} /* namespace */
