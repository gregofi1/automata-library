#include "WildcardSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::WildcardSymbol xmlApi < alphabet::WildcardSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::WildcardSymbol ( );
}

bool xmlApi < alphabet::WildcardSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::WildcardSymbol >::xmlTagName ( ) {
	return "WildcardSymbol";
}

void xmlApi < alphabet::WildcardSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::WildcardSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::WildcardSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::WildcardSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::WildcardSymbol > ( );

} /* namespace */
