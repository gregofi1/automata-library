#include "InitialSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::InitialSymbol xmlApi < alphabet::InitialSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::InitialSymbol ( );
}

bool xmlApi < alphabet::InitialSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::InitialSymbol >::xmlTagName ( ) {
	return "InitialSymbol";
}

void xmlApi < alphabet::InitialSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::InitialSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::InitialSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::InitialSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::InitialSymbol > ( );

} /* namespace */
