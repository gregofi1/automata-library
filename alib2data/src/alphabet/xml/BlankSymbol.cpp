#include "BlankSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::BlankSymbol xmlApi < alphabet::BlankSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::BlankSymbol ( );
}

bool xmlApi < alphabet::BlankSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::BlankSymbol >::xmlTagName ( ) {
	return "BlankSymbol";
}

void xmlApi < alphabet::BlankSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::BlankSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::BlankSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::BlankSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::BlankSymbol > ( );

} /* namespace */
