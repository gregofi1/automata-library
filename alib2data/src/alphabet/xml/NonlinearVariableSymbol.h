#pragma once

#include <alphabet/NonlinearVariableSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < class T >
struct xmlApi < alphabet::NonlinearVariableSymbol < T > > {
	static alphabet::NonlinearVariableSymbol < T > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::NonlinearVariableSymbol < T > & input );
};

template < typename T >
alphabet::NonlinearVariableSymbol < T > xmlApi < alphabet::NonlinearVariableSymbol < T > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	T data = core::xmlApi < T >::parse ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );

	return alphabet::NonlinearVariableSymbol < T > ( data );
}

template < typename T >
bool xmlApi < alphabet::NonlinearVariableSymbol < T > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < typename T >
std::string xmlApi < alphabet::NonlinearVariableSymbol < T > >::xmlTagName ( ) {
	return "NonlinearVariableSymbol";
}

template < typename T >
void xmlApi < alphabet::NonlinearVariableSymbol < T > >::compose ( ext::deque < sax::Token > & output, const alphabet::NonlinearVariableSymbol < T > & input ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < T >::compose ( output, input.getSymbol ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

