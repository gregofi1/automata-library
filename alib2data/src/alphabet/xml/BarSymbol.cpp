#include "BarSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::BarSymbol xmlApi < alphabet::BarSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::BarSymbol ( );
}

bool xmlApi < alphabet::BarSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::BarSymbol >::xmlTagName ( ) {
	return "BarSymbol";
}

void xmlApi < alphabet::BarSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::BarSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::BarSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::BarSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::BarSymbol > ( );

} /* namespace */
