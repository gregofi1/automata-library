#include "BottomOfTheStackSymbol.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::BottomOfTheStackSymbol xmlApi < alphabet::BottomOfTheStackSymbol >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::BottomOfTheStackSymbol ( );
}

bool xmlApi < alphabet::BottomOfTheStackSymbol >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::BottomOfTheStackSymbol >::xmlTagName ( ) {
	return "BottomOfTheStackSymbol";
}

void xmlApi < alphabet::BottomOfTheStackSymbol >::compose ( ext::deque < sax::Token > & output, const alphabet::BottomOfTheStackSymbol & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::BottomOfTheStackSymbol > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::BottomOfTheStackSymbol > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::BottomOfTheStackSymbol > ( );

} /* namespace */
