#include "WildcardSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

WildcardSymbol::WildcardSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const WildcardSymbol & ) {
	return out << "(WildcardSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::WildcardSymbol > ( );

} /* namespace */
