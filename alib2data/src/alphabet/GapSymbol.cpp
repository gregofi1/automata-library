#include "GapSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

GapSymbol::GapSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const GapSymbol & ) {
	return out << "(GapSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::GapSymbol > ( );

} /* namespace */
