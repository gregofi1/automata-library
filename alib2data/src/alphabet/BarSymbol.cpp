#include "BarSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BarSymbol::BarSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const BarSymbol & ) {
	return out << "(Bar symbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BarSymbol > ( );

} /* namespace */
