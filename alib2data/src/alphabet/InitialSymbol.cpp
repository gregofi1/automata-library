#include "InitialSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

InitialSymbol::InitialSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const InitialSymbol & ) {
	return out << "(InitialSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::InitialSymbol > ( );

} /* namespace */
