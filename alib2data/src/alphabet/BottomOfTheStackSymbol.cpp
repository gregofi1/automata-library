#include "BottomOfTheStackSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BottomOfTheStackSymbol::BottomOfTheStackSymbol() = default;

ext::ostream & operator << ( ext::ostream & out, const BottomOfTheStackSymbol & ) {
	return out << "(BottomOfTheStackSymbol)";
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BottomOfTheStackSymbol > ( );

} /* namespace */
