#include "SuffixAutomaton.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::SuffixAutomaton < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::SuffixAutomaton < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::SuffixAutomaton < > > ( );

} /* namespace */
