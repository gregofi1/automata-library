#include "SuffixArray.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::SuffixArray < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::SuffixArray < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::SuffixArray < > > ( );

} /* namespace */
