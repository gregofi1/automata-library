#include "SuffixTrie.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::SuffixTrie < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::SuffixTrie < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::SuffixTrie < > > ( );

} /* namespace */
