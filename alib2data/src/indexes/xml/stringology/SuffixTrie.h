/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <indexes/stringology/SuffixTrie.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <object/xml/Void.h>
#include <primitive/xml/Unsigned.h>
#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsOptional.h>
#include <container/xml/ObjectsTrie.h>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::SuffixTrie < SymbolType > > {
	static indexes::stringology::SuffixTrie < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixTrie < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::SuffixTrie < SymbolType > xmlApi < indexes::stringology::SuffixTrie < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::set < SymbolType > edgeAlphabet = core::xmlApi < ext::set < SymbolType > >::parse ( input );
	ext::trie < SymbolType, std::optional < unsigned > > root = core::xmlApi < ext::trie < SymbolType, std::optional < unsigned > > >::parse ( input );
	indexes::stringology::SuffixTrie < SymbolType > res ( std::move ( edgeAlphabet ), std::move ( root ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::SuffixTrie < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::SuffixTrie < SymbolType > >::xmlTagName ( ) {
	return "SuffixTrie";
}

template < class SymbolType >
void xmlApi < indexes::stringology::SuffixTrie < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixTrie < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::set < SymbolType > >::compose ( output, index.getAlphabet ( ) );
	core::xmlApi < ext::trie < SymbolType, std::optional < unsigned > > >::compose ( output, index.getRoot ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

