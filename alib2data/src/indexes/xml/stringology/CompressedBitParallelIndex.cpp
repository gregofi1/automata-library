#include "CompressedBitParallelIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::CompressedBitParallelIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::CompressedBitParallelIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::CompressedBitParallelIndex < > > ( );

} /* namespace */
