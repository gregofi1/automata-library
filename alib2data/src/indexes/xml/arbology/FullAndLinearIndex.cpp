#include "FullAndLinearIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::arbology::FullAndLinearIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::arbology::FullAndLinearIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::arbology::FullAndLinearIndex < > > ( );

} /* namespace */
