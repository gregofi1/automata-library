/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <indexes/arbology/NonlinearFullAndLinearIndex.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Integer.h>
#include <container/xml/ObjectsVector.h>

#include <alphabet/xml/RankedSymbol.h>

#include <indexes/xml/stringology/PositionHeap.h>

namespace core {

template < class SymbolType, template < typename > class StringIndex >
struct xmlApi < indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > > {
	static indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > & index );
};

template < class SymbolType, template < typename > class StringIndex >
indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > xmlApi < indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	StringIndex < common::ranked_symbol < SymbolType > > stringIndex = core::xmlApi < StringIndex < common::ranked_symbol < SymbolType > > >::parse ( input );
	ext::vector < int > jumps = core::xmlApi < ext::vector < int > >::parse ( input );
	ext::vector < unsigned > repeats = core::xmlApi < ext::vector < unsigned > >::parse ( input );

	indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > res ( std::move ( stringIndex ), std::move ( jumps ), std::move ( repeats ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType, template < typename > class StringIndex >
bool xmlApi < indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType, template < typename > class StringIndex >
std::string xmlApi < indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > >::xmlTagName ( ) {
	return "NonlinearFullAndLinearIndex";
}

template < class SymbolType, template < typename > class StringIndex >
void xmlApi < indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > >::compose ( ext::deque < sax::Token > & output, const indexes::arbology::NonlinearFullAndLinearIndex < SymbolType, StringIndex > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < StringIndex < common::ranked_symbol < SymbolType > > >::compose ( output, index.getStringIndex ( ) );
	core::xmlApi < ext::vector < int > >::compose ( output, index.getJumps ( ) );
	core::xmlApi < ext::vector < unsigned > >::compose ( output, index.getRepeats ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

