#include "NonlinearFullAndLinearIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::arbology::NonlinearFullAndLinearIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::arbology::NonlinearFullAndLinearIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::arbology::NonlinearFullAndLinearIndex < > > ( );

} /* namespace */
