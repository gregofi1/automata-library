#include "BitSetIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::BitSetIndex < > > ( );

} /* namespace */
