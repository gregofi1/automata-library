#include "NonlinearFullAndLinearIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::NonlinearFullAndLinearIndex < > > ( );

} /* namespace */
