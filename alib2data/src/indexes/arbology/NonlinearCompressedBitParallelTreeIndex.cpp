#include "NonlinearCompressedBitParallelTreeIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < > > ( );

} /* namespace */
