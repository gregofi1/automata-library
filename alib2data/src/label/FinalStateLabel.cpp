#include "FinalStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FinalStateLabel::FinalStateLabel() = default;

ext::ostream & operator << ( ext::ostream & out, const FinalStateLabel & ) {
	return out << "(FinalStateLabel)";
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::FinalStateLabel > ( );

} /* namespace */
