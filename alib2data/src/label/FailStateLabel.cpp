#include "FailStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FailStateLabel::FailStateLabel() = default;

ext::ostream & operator << ( ext::ostream & out, const FailStateLabel & ) {
	return out << "(FailStateLabel)";
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::FailStateLabel > ( );

} /* namespace */
