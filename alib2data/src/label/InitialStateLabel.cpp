#include "InitialStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

InitialStateLabel::InitialStateLabel() = default;

ext::ostream & operator << ( ext::ostream & out, const InitialStateLabel & ) {
	return out << "(InitialStateLabel)";
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::InitialStateLabel > ( );

} /* namespace */
