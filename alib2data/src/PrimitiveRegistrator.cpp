#include <registry/ContainerRegistry.hpp>

#include <registration/XmlRegistration.hpp>
#include <registry/XmlContainerParserRegistry.hpp>
#include <registration/ValuePrinterRegistration.hpp>

#include <primitive/xml/UnsignedLong.h>
#include <primitive/xml/Unsigned.h>
#include <primitive/xml/Bool.h>

#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsMap.h>
#include <container/xml/ObjectsVector.h>
#include <container/xml/ObjectsVariant.h>
#include <container/xml/ObjectsTrie.h>

#include <common/ranked_symbol.hpp>
#include <alphabet/xml/RankedSymbol.h>

#include <string/xml/LinearString.h>

#include <object/xml/Void.h>

namespace {

class PrimitiveRegistrator {
	registration::XmlWriterRegister < ext::set < common::ranked_symbol < object::Object > > > member1;
	registration::XmlWriterRegister < ext::vector < ext::map < std::pair < object::Object, object::Object >, ext::map < object::Object, object::Object > > > > member2;
	registration::XmlWriterRegister < ext::vector < ext::vector < ext::set < object::Object > > > > member3;
	registration::XmlWriterRegister < ext::map < common::ranked_symbol < object::Object >, size_t > > member4;
	registration::XmlWriterRegister < ext::set < string::LinearString < > > > member5;
	registration::XmlWriterRegister < ext::trie < DefaultSymbolType, bool > > member6;
	registration::XmlWriterRegister < ext::set < ext::pair < string::LinearString < >, unsigned int > > > member7;

	registration::ValuePrinterRegister < ext::set < string::LinearString < > > > member8;
	registration::ValuePrinterRegister < ext::set < ext::pair < string::LinearString < >, unsigned int > > > member9;

public:
	PrimitiveRegistrator ( ) {
		abstraction::ContainerRegistry::registerSet < common::ranked_symbol < object::Object > > ( "RankedSymbol" );
		abstraction::XmlContainerParserRegistry::registerSet < common::ranked_symbol < object::Object > > ( "RankedSymbol" );

		abstraction::XmlParserRegistry::registerXmlParser < object::Object > ( "DefaultStateType" );

		core::xmlApi < object::Object >::template registerXmlWriter < ext::set < common::ranked_symbol < object::Object > > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::set < common::ranked_symbol < ext::pair < object::Object, unsigned int > > > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < common::ranked_symbol < ext::pair < object::Object, unsigned int> > > ( );
	}

	~PrimitiveRegistrator ( ) {
		abstraction::ContainerRegistry::unregisterSet ( "RankedSymbol" );
		abstraction::XmlContainerParserRegistry::unregisterSet ( "RankedSymbol" );

		abstraction::XmlParserRegistry::unregisterXmlParser ( "DefaultStateType" );

		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::set < common::ranked_symbol < object::Object > > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::set < common::ranked_symbol < ext::pair < object::Object, unsigned int > > > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < common::ranked_symbol < ext::pair < object::Object, unsigned int> > > ( );
	}
};

auto primitiveRegistrator = PrimitiveRegistrator ( );

} /* namespace */
