project(alt-libdata VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)

# TODO: Make LibXml2 interface

alt_library(alib2data
	DEPENDS alib2std alib2common alib2xml LibXml2::LibXml2
	TEST_DEPENDS LibXml2::LibXml2
)
