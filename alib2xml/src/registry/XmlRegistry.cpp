#include <registry/XmlRegistry.h>

#include <registry/XmlComposerRegistry.hpp>
#include <registry/XmlParserRegistry.hpp>
#include <registry/XmlContainerParserRegistry.hpp>

namespace abstraction {

ext::set < std::string > XmlRegistry::listDataTypes ( ) {
	return XmlComposerRegistry::list ( );
}

ext::set < std::string > XmlRegistry::listDataTypeGroup ( const std::string & group ) {
	return XmlComposerRegistry::listGroup ( group );
}

std::unique_ptr < abstraction::OperationAbstraction > XmlRegistry::getXmlComposerAbstraction ( const std::string & param ) {
	return XmlComposerRegistry::getAbstraction ( param );
}

std::unique_ptr < abstraction::OperationAbstraction > XmlRegistry::getXmlParserAbstraction ( const std::string & type ) {
	return XmlParserRegistry::getAbstraction ( type );
}

std::unique_ptr < abstraction::OperationAbstraction > XmlRegistry::getXmlContainerParserAbstraction ( const std::string & container, const std::string & type ) {
	return XmlContainerParserRegistry::getAbstraction ( container, type );
}

} /* namespace abstraction */
