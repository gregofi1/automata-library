#pragma once

#include <ext/functional>
#include <ext/memory>
#include <ext/typeinfo>

#include <alib/list>
#include <alib/string>
#include <alib/set>
#include <alib/map>

#include <exception/CommonException.h>
#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class XmlContainerParserRegistry {
public:
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Params >
	class SetEntryImpl : public Entry {
	public:
		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, ext::list < ext::pair < std::string, std::unique_ptr < Entry > > > > & getEntries ( );

public:
	static void unregisterSet ( const std::string & param );

	template < class ParamTypes >
	static void unregisterSet ( ) {
		std::string param = ext::to_string < typename std::decay < ParamTypes >::type > ( );
		unregisterSet ( param );
	}

	static void registerSet ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamTypes >
	static void registerSet ( std::string param ) {
		registerSet ( std::move ( param ), std::make_unique < SetEntryImpl < ParamTypes > > ( ) );
	}

	template < class ParamTypes >
	static void registerSet ( ) {
		std::string param = ext::to_string < typename std::decay < ParamTypes >::type > ( );
		registerSet < ParamTypes > ( std::move ( param ) );
	}

	static bool hasAbstraction ( const std::string & container );

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & container, const std::string & type );

	static ext::set < std::string > listOverloads ( const std::string & container );

	static ext::set < std::string > list ( );
};

} /* namespace abstraction */

#include <abstraction/XmlParserAbstraction.hpp>

namespace abstraction {

template < class Param >
std::unique_ptr < abstraction::OperationAbstraction > XmlContainerParserRegistry::SetEntryImpl < Param >::getAbstraction ( ) const {
	return std::make_unique < abstraction::XmlParserAbstraction < ext::set < Param > > > ( );
}

} /* namespace abstraction */

