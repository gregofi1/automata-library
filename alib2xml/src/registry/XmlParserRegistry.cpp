#include <registry/XmlParserRegistry.hpp>

#include <exception/CommonException.h>

namespace abstraction {

ext::map < std::string, std::unique_ptr < XmlParserRegistry::Entry > > & XmlParserRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > parsers;
	return parsers;
}

void XmlParserRegistry::unregisterXmlParser ( const std::string & result ) {
	if ( getEntries ( ).erase ( result ) == 0u )
		throw std::invalid_argument ( "Entry " + result + " not registered." );
}

void XmlParserRegistry::registerXmlParser ( std::string result, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( result ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > XmlParserRegistry::getAbstraction ( const std::string & typeName ) {
	auto type = getEntries ( ).find ( typeName );
	if ( type == getEntries ( ).end ( ) )
		throw exception::CommonException ( "Entry " + typeName + " not available." );

	return type->second->getAbstraction ( );
}

} /* namespace abstraction */
