#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <sax/SaxComposeInterface.h>

namespace abstraction {

class XmlTokensComposerAbstraction : virtual public NaryOperationAbstraction < const ext::deque < sax::Token > &, const std::string & >, virtual public ValueOperationAbstraction < void > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param2 = std::get < 0 > ( this->getParams ( ) );
		const std::shared_ptr < abstraction::Value > & param1 = std::get < 1 > ( this->getParams ( ) );
		sax::SaxComposeInterface::composeFile ( abstraction::retrieveValue < const std::string & > ( param1 ), abstraction::retrieveValue < const ext::deque < sax::Token > & > ( param2 ) );
		return std::make_shared < abstraction::Void > ( );
	}

};

} /* namespace abstraction */

