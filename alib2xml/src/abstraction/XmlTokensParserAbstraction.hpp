#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <sax/SaxParseInterface.h>

namespace abstraction {

class XmlTokensParserAbstraction : virtual public NaryOperationAbstraction < const std::string & >, virtual public ValueOperationAbstraction < ext::deque < sax::Token > > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ext::deque < sax::Token > > > ( sax::SaxParseInterface::parseFile ( abstraction::retrieveValue < const std::string & > ( param ) ), true );
	}

};

} /* namespace abstraction */

