#pragma once

#include <alib/optional>
#include <exception/CommonException.h>
#include <core/xmlApi.hpp>

namespace core {

template < typename T >
struct xmlApi < std::optional < T > > {
	static std::optional < T > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const std::optional < T > & input );
};

template < typename T >
std::optional < T > xmlApi < std::optional < T > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	if ( sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, "void" ) ) {
		++input;
		sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "void" );
		return std::optional < T > ( );
	} else {
		return std::optional < T > ( core::xmlApi < T >::parse ( input ) );
	}
}

template < typename T >
bool xmlApi < std::optional < T > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, "void" ) || xmlApi < T >::first ( input );
}

template < typename T >
std::string xmlApi < std::optional < T > >::xmlTagName ( ) {
	throw exception::CommonException ( "Optional does not have xmlTagName." );
}

template < typename T >
void xmlApi < std::optional < T > >::compose ( ext::deque < sax::Token > & output, const std::optional < T > & input ) {
	if ( ! input ) {
		output.emplace_back("void", sax::Token::TokenType::START_ELEMENT);
		output.emplace_back("void", sax::Token::TokenType::END_ELEMENT);
	} else {
		core::xmlApi < T >::compose ( output, input.value ( ) );
	}
}

} /* namespace core */

