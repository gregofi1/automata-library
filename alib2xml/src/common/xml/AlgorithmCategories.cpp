#include "AlgorithmCategories.hpp"

namespace core {

abstraction::AlgorithmCategories::AlgorithmCategory xmlApi < abstraction::AlgorithmCategories::AlgorithmCategory >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	std::string data = sax::FromXMLParserHelper::popTokenData(input, sax::Token::TokenType::CHARACTER);
	abstraction::AlgorithmCategories::AlgorithmCategory res = abstraction::AlgorithmCategories::algorithmCategory ( data );
	sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );

	return res;
}

bool xmlApi < abstraction::AlgorithmCategories::AlgorithmCategory >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < abstraction::AlgorithmCategories::AlgorithmCategory >::xmlTagName ( ) {
	return "AlgorithmCategory";
}

void xmlApi < abstraction::AlgorithmCategories::AlgorithmCategory >::compose ( ext::deque < sax::Token > & output, abstraction::AlgorithmCategories::AlgorithmCategory category ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT);
	output.emplace_back ( abstraction::AlgorithmCategories::toString ( category ), sax::Token::TokenType::CHARACTER);
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
