#pragma once

#include <libxml/xmlreader.h>
#include <alib/deque>
#include "Token.h"

namespace sax {

/**
 * This class performs token parsing of file, string, or stream containing XML. Contains callback
 * method for libxml SAX parser.
 */
class SaxParseInterface {
	static int xmlSAXUserParse(xmlTextReaderPtr reader, ext::deque<Token>& out);
public:
	/**
	 * Parses the string containing XML.
	 * @param xmlIn input XML
	 * @param out parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static void parseMemory(const std::string& xmlIn, ext::deque<Token>& out);

	/**
	 * Parses the string containing XML.
	 * @param xmlIn input XML
	 * @return parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static ext::deque < Token > parseMemory ( const std::string & xmlIn );

	/**
	 * Parses the file containing XML.
	 * @param filename input XML
	 * @param out parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. file doesn't exist, XML is not valid)
	 */
	static void parseFile(const std::string& filename, ext::deque<Token>& out);

	/**
	 * Parses the file containing XML.
	 * @param filename input XML
	 * @return parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. file doesn't exist, XML is not valid)
	 */
	static ext::deque < Token > parseFile ( const std::string& filename );

	/**
	 * Parses the XML from stdin.
	 * @param out parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static void parseStdin(ext::deque<Token>& out);

	/**
	 * Parses the XML from stdin.
	 * @return parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static ext::deque < Token > parseStdin ( );

	/**
	 * Parses the XML from stream.
	 * @param in input XML
	 * @param out parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static void parseStream(ext::istream& in, ext::deque<Token>& out);

	/**
	 * Parses the XML from stream.
	 * @param in input XML
	 * @return parsed list of xml tokens
	 * @throws CommonException when an error occurs (e.g. XML is not valid)
	 */
	static ext::deque < Token > parseStream ( ext::istream & in );
};

} /* namespace sax */

