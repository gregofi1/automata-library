#include <registration/XmlRegistration.hpp>
#include <registry/XmlContainerParserRegistry.hpp>
#include <registry/ContainerRegistry.hpp>

#include <primitive/xml/Character.h>
#include <primitive/xml/Double.h>
#include <primitive/xml/Integer.h>
#include <primitive/xml/Unsigned.h>
#include <primitive/xml/Bool.h>
#include <primitive/xml/UnsignedLong.h>
#include <primitive/xml/String.h>

#include <container/xml/ObjectsVector.h>
#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsVariant.h>
#include <container/xml/ObjectsPair.h>
#include <container/xml/ObjectsMap.h>

namespace {

class PrimitiveRegistrator {
	registration::XmlWriterRegister < ext::vector < size_t > > member1;
	registration::XmlWriterRegister < ext::vector < unsigned > > member2;
	registration::XmlWriterRegister < ext::vector < int > > member3;
	registration::XmlWriterRegister < ext::set < size_t > > member4;
	registration::XmlWriterRegister < ext::set < unsigned > > member5;
	registration::XmlWriterRegister < ext::set < int > > member6;
	registration::XmlWriterRegister < ext::map < object::Object, size_t > > member7;
	registration::XmlWriterRegister < object::Object > member8;
	registration::XmlWriterRegister < ext::set < ext::pair < unsigned, unsigned > > > member9;

public:
	PrimitiveRegistrator ( ) {
		abstraction::XmlContainerParserRegistry::registerSet < int > ( );
		abstraction::XmlParserRegistry::registerXmlParser < int > ( "int" );
		abstraction::XmlParserRegistry::registerXmlParser < ext::set < ext::pair < object::Object, object::Object > > > ( "pair_set" );

		abstraction::ContainerRegistry::registerSet < object::Object > ( "Object" );
		abstraction::XmlContainerParserRegistry::registerSet < object::Object > ( "Object" );
		abstraction::XmlParserRegistry::registerXmlParser < object::Object > ( "Object" );

		core::xmlApi < object::Object >::template registerXmlWriter < ext::set < ext::pair < object::Object, object::Object > > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::pair < ext::set < ext::pair < object::Object, object::Object > >, object::Object > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::vector < ext::variant < object::Object, object::Object > > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::pair < unsigned int, unsigned int > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::pair < object::Object, unsigned int > > ( );
		core::xmlApi < object::Object >::template registerXmlWriter < ext::set < char > > ( );
	}

	~PrimitiveRegistrator ( ) {
		abstraction::XmlContainerParserRegistry::unregisterSet < int > ( );
		abstraction::XmlParserRegistry::unregisterXmlParser ( "int" );
		abstraction::XmlParserRegistry::unregisterXmlParser ( "pair_set" );

		abstraction::ContainerRegistry::unregisterSet ( "Object" );
		abstraction::XmlContainerParserRegistry::unregisterSet ( "Object" );
		abstraction::XmlParserRegistry::unregisterXmlParser ( "Object" );

		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::set < ext::pair < object::Object, object::Object > > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::pair < ext::set < ext::pair < object::Object, object::Object > >, object::Object > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::vector < ext::variant < object::Object, object::Object > > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::pair < unsigned int, unsigned int > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::pair < object::Object, unsigned int > > ( );
		core::xmlApi < object::Object >::template unregisterXmlWriter < ext::set < char > > ( );
	}
};

auto primitiveRegistrator = PrimitiveRegistrator ( );

} /* namespace */
