#include "Unsigned.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

unsigned xmlApi < unsigned >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	unsigned data = ext::from_string < unsigned > ( sax::FromXMLParserHelper::popTokenData ( input, sax::Token::TokenType::CHARACTER ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return data;
}

bool xmlApi < unsigned >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < unsigned >::xmlTagName ( ) {
	return "Unsigned";
}

void xmlApi < unsigned >::compose ( ext::deque < sax::Token > & output, unsigned data ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( ext::to_string ( data ), sax::Token::TokenType::CHARACTER );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < unsigned > ( );
auto xmlRead = registration::XmlReaderRegister < unsigned > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, unsigned > ( );

} /* namespace */
