#include "Bool.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

bool xmlApi < bool >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	std::string tmp = sax::FromXMLParserHelper::popTokenData ( input, sax::Token::TokenType::CHARACTER );
	bool data;
	if ( tmp == "true" )
		data = true;
	else if ( tmp == "false" )
		data = false;
	else
		throw exception::CommonException ( "Invalid boolean value" );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );

	return data;
}

bool xmlApi < bool >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < bool >::xmlTagName ( ) {
	return "Bool";
}

void xmlApi < bool >::compose ( ext::deque < sax::Token > & output, bool data ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT);
	if ( data )
		output.emplace_back ( "true", sax::Token::TokenType::CHARACTER );
	else
		output.emplace_back ( "false", sax::Token::TokenType::CHARACTER );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < bool > ( );
auto xmlRead = registration::XmlReaderRegister < bool > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, bool > ( );

} /* namespace */
