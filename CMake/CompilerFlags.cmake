# Compiler flags handling
# -----------------------
include(CheckCXXCompilerFlag)

if(NOT ${CMAKE_CXX_COMPILER_ID} MATCHES "(Clang|GNU)")
	message(WARNING "Compiler ${CMAKE_CXX_COMPILER_ID} may not be supported.")
endif ()

set(CMAKE_CXX_STANDARD 20)
set(CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# Avoid name collisions in QT
add_definitions(-DQT_NO_FOREACH)

# If backtrace is available, use it
find_package(Backtrace)
if(NOT(CMAKE_BUILD_TYPE MATCHES RELEASE) AND BACKTRACE_EXISTS)
	add_definitions(-DBACKTRACE)
endif()

# Add some basic compiler options
add_compile_options(-Wall -Wextra -pedantic -pipe -Wshadow )
add_compile_options(-Wnon-virtual-dtor -Wcast-align -Wunused -Woverloaded-virtual -Wnull-dereference -Wdouble-promotion -Wformat=2 -Wpointer-arith -Wcast-qual -Wdelete-non-virtual-dtor -Wredundant-decls -Wold-style-cast)
set(CMAKE_CXX_FLAGS_DEBUG "-Werror ${CMAKE_CXX_FLAGS_DEBUG}")

# -Wsign-conversion -Wconversion
if(${CMAKE_CXX_COMPILER_ID} MATCHES "(GNU)")
	add_compile_options(-Wlogical-op)
	# -Wuseless-cast # buggy; compains about creation of a temporary...
	# -Wduplicated-cond -Wduplicated-branches # todo
endif ()
if(${CMAKE_CXX_COMPILER_ID} MATCHES "(Clang)")
	# add_compile_options(-Wlifetime) # enable once it becomes available
endif ()


# Use -Og (or -O0) in Debug
if(CMAKE_BUILD_TYPE MATCHES DEBUG)
	CHECK_CXX_COMPILER_FLAG("-Og" COMPILER_HAS_OG)
	if(COMPILER_HAS_OG)
		add_compile_options(-Og)
	else()
		add_compile_options(-O0)
	endif()
endif()

# Use -Wsuggest-override if available (GNU)
CHECK_CXX_COMPILER_FLAG("-Wsuggest-override" COMPILER_HAS_SUGGEST_OVERRIDE)
if(COMPILER_HAS_SUGGEST_OVERRIDE)
	add_compile_options(-Wsuggest-override)
endif()

# Add preprocessor definitions
if (CMAKE_BUILD_TYPE MATCHES Debug)
    add_compile_definitions(DEBUG)
else()  # Release, RelWithDebInfo, MinSizeRel
	add_compile_definitions(RELEASE)
endif()

# set linker flags
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-no-as-needed")
