find_path(TCLAP_INCLUDE_DIR NAMES tclap/CmdLine.h)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Tclap
	FOUND_VAR TCLAP_FOUND
	REQUIRED_VARS TCLAP_INCLUDE_DIR
	)

mark_as_advanced(TCLAP_INCLUDE_DIR)
