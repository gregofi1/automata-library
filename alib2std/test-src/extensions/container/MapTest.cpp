#include <catch2/catch.hpp>
#include <ext/map>
#include <ext/algorithm>
#include <ext/functional>

namespace {
	class Moveable {
		int& m_moves;
		int& m_copies;

		public:
		Moveable(int& moves, int& copies) : m_moves(moves), m_copies(copies) {
			m_moves = 0;
			m_copies = 0;
		}

		Moveable(const Moveable& src) : m_moves(src.m_moves), m_copies(src.m_copies) {
			m_copies++;
		}

		Moveable(Moveable&& src) : m_moves(src.m_moves), m_copies(src.m_copies) {
			m_moves++;
		}

		Moveable & operator = ( const Moveable & ) {
			m_copies ++;
			return * this;
		}

		Moveable & operator = ( Moveable && ) {
			m_moves ++;
			return * this;
		}

		auto operator <=> (const Moveable&) const {
			return std::strong_ordering::equal;
		}
	};
}
TEST_CASE ( "Map", "[unit][std][container]" ) {
	SECTION ( "Basic" ) {
		int moves;
		int copies;

		ext::map<Moveable, Moveable> map;
		map.insert ( Moveable(moves, copies ), Moveable(moves, copies) );
		ext::map<Moveable, Moveable> map2;

		for( std::pair < Moveable, Moveable > && moveablePair : ext::make_mover ( map ) ) {
			map2.insert(std::move(moveablePair));
		}

		bool res = map2.insert_or_assign ( Moveable ( moves, copies ), Moveable ( moves, copies ) ).second;

		CHECK ( res == false );

		CHECK ( copies == 0 );
	}

	SECTION ( "Move Wrapper" ) {
		int moves;
		int copies;

		Moveable value ( moves, copies );

		ext::map < ext::reference_wrapper < Moveable >, Moveable > map;
		map.insert ( std::make_pair ( ext::reference_wrapper < Moveable > ( value ), Moveable(moves, copies) ) );

		ext::map<Moveable, Moveable> map2;

		ext::reference_mover < ext::map < ext::reference_wrapper < Moveable >, Moveable > > && __range1 = ext::make_mover ( map );

		auto __begin1 = __range1.begin ( );
		auto __end1 =__range1.end ( );

		for(; __begin1 != __end1; __begin1.operator++()) {
			std::pair < ext::reference_wrapper < Moveable >, Moveable > && moveablePair = __begin1.operator*();
			map2.insert(std::move(moveablePair));
		}

		bool res = map2.insert_or_assign ( Moveable ( moves, copies ), Moveable ( moves, copies ) ).second;

		CHECK ( res == false );

		CHECK ( copies == 0 );
	}

	SECTION ( "At" ) {
		ext::map < int, std::string > map;
		map.insert ( 1, "one" );

		{
			std::string defaultValue = "default";
			std::string & value = map.at ( 0, defaultValue );

			CHECK ( value == "default" );
		}

		{
			const std::string defaultValue = "default";
			const std::string & value = map.at ( 0, defaultValue );

			CHECK ( value == "default" );
		}
	}

	CHECK ( ( std::tuple_size < const std::pair < int, int > >::value == 2 ) );
}
