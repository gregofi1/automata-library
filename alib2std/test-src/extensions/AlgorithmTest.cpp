#include <catch2/catch.hpp>

#include <ext/algorithm>
#include <ext/set>
#include <ext/vector>
#include <ext/list>

TEST_CASE ( "Algorithm Test", "[unit][std][bits]" ) {
	SECTION ( "Exclude" ) {
		{
			int array1[] = {1, 2, 3};
			int array2[] = {2, 3, 4};

			CHECK(ext::excludes(array1, array1 + sizeof(array1) / sizeof(int), array2, array2 + sizeof(array2) / sizeof(int)) == false);
		}

		{
			int array1[] = {1, 3, 5};
			int array2[] = {2, 4, 6, 8};

			CHECK(ext::excludes(array1, array1 + sizeof(array1) / sizeof(int), array2, array2 + sizeof(array2) / sizeof(int)) == true);
			CHECK(ext::excludes(array2, array2 + sizeof(array2) / sizeof(int), array1, array1 + sizeof(array1) / sizeof(int)) == true);
		}

		{
			int array1[] = {1, 3, 5, 8};
			int array2[] = {2, 4, 6, 8};

			CHECK(ext::excludes(array1, array1 + sizeof(array1) / sizeof(int), array2, array2 + sizeof(array2) / sizeof(int)) == false);
			CHECK(ext::excludes(array2, array2 + sizeof(array2) / sizeof(int), array1, array1 + sizeof(array1) / sizeof(int)) == false);
		}

		{
			int array1[] = {0};
			int array2[] = {2, 4, 6, 8};

			CHECK(ext::excludes(array1, array1 + sizeof(array1) / sizeof(int), array2, array2 + sizeof(array2) / sizeof(int)) == true);
			CHECK(ext::excludes(array2, array2 + sizeof(array2) / sizeof(int), array1, array1 + sizeof(array1) / sizeof(int)) == true);

			CHECK(ext::excludes(array1, array1, array2, array2) == true);
			CHECK(ext::excludes(array2, array2, array1, array1) == true);
		}
	}

	SECTION ( "Transform" ) {
		ext::set<int> in = {1, 2, 3, 4};
		ext::set<long> ref = {2, 3, 4, 5};

		/*	std::function<long(const int&)> lamb = [](const int& elem) { return (long) elem + 1; };
			ext::set<long> out = ext::transform<long>(in, lamb); //*/

		ext::set<long> out = ext::transform<long>(in, [](const int& elem) { return static_cast < long > ( elem + 1 ); });

		CHECK(ref == out);
	}

	SECTION ( "FindRange" ) {
		{
			std::string str = "abc<dee<fd<<>>>th>::rrr";

			std::pair < std::string::iterator, std::string::iterator > range = ext::find_range ( str.begin ( ), str.end ( ), '<', '>' );

			CHECK ( range.first - str.begin ( ) == 3 );
			CHECK ( range.second - str.begin ( ) == 18 );

			str.erase ( range.first, range.second );

			CHECK ( str == "abc::rrr" );
		}
		{
			std::string str = "aaa::abc<dee<fd<<>>>th>";

			std::pair < std::string::iterator, std::string::iterator > range = ext::find_range ( str.begin ( ), str.end ( ), '<', '>' );

			CHECK ( range.first - str.begin ( ) == 8 );
			CHECK ( range.second - str.begin ( ) == 23 );

			str.erase ( range.first, range.second );

			CHECK ( str == "aaa::abc" );
		}
		{
			std::string str = "<dee<fd<<>>>th>aaa";

			std::pair < std::string::iterator, std::string::iterator > range = ext::find_range ( str.begin ( ), str.end ( ), '<', '>' );

			CHECK ( range.first - str.begin ( ) == 0 );
			CHECK ( range.second - str.begin ( ) == 15 );

			str.erase ( range.first, range.second );

			CHECK ( str == "aaa" );
		}
		{
			std::string str = "<dee<fd<<>>>th>";

			std::pair < std::string::iterator, std::string::iterator > range = ext::find_range ( str.begin ( ), str.end ( ), '<', '>' );

			CHECK ( range.first - str.begin ( ) == 0 );
			CHECK ( range.second - str.begin ( ) == 15 );

			str.erase ( range.first, range.second );
			CHECK ( str == "" );
		}

		{
			std::string str = "<dee<fd<<>>>th";

			std::pair < std::string::iterator, std::string::iterator > range = ext::find_range ( str.begin ( ), str.end ( ), '<', '>' );

			CHECK ( range.first - str.begin ( ) == 14 );
			CHECK ( range.second - str.begin ( ) == 14 );
		}
	}

	SECTION ( "Symmetric Difference" ) {
		ext::set < int > a { 1,    3,    5, 6, 7, 8, 9 };
		ext::set < int > b { 1, 2, 3, 4, 5,    7, 8    };

		ext::vector < int > a_b { 6, 9 };
		ext::vector < int > b_a { 2, 4 };

		ext::vector < int > a_b2;
		ext::vector < int > b_a2;

		ext::set_symmetric_difference ( a.begin ( ), a.end ( ), b.begin ( ), b.end ( ), std::back_inserter ( a_b2 ), std::back_inserter ( b_a2 ) );

		CHECK ( a_b == a_b2 );
		CHECK ( b_a == b_a2 );
	}

	SECTION ( "Range Contains Iterator" ) {
		std::list < int > test = { 1, 2, 3, 4, 5, 6 };
		std::list < int > other = { 1, 2, 3, 4, 5, 6, 7 };
		std::list < char > other2 = { 'a', 'b', 'c' };

		std::list < int >::iterator iter = test.begin ( );
		iter++;
		iter++;

		CHECK ( ext::range_contains_iterator ( test.begin ( ), test.end ( ), iter ) );
		CHECK ( ! ext::range_contains_iterator ( other.begin ( ), other.end ( ), iter ) );
		CHECK ( ! ext::range_contains_iterator ( other2.begin ( ), other2.end ( ), iter ) );
	}
}
