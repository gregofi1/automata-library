#include <iomanip>
#include "ExceptionHandler.hpp"

namespace {
	constexpr int CLI_RESULT_ON_UNKNOWN_EXCEPTION = 127;
}

namespace alib {

ExceptionHandler::NestedException::NestedException ( std::string type_, std::string desc_ ) : type ( std::move ( type_ ) ), desc ( std::move ( desc_ ) ) { }

ExceptionHandler::NestedException::NestedException ( std::string type_ ) : type ( std::move ( type_ ) ) { }

ext::ostream & operator << ( ext::ostream & os, const ExceptionHandler::NestedException & exception ) {
	if ( exception.desc.has_value ( ) ) {
		os << "[" << exception.type << "]: " << exception.desc.value ( );
	} else {
		os << exception.type;
	}
	return os;
}

std::vector < std::function < int ( alib::ExceptionHandler::NestedExceptionContainer & ) > > & ExceptionHandler::handlers ( ) {
	static std::vector < std::function < int ( NestedExceptionContainer & ) > > res = {
		[ ] ( NestedExceptionContainer & output ) {
			try {
				throw;
			} catch ( const std::exception & e ) {
				output.push_back ( "Standard exception", e.what ( ) );
				return rethrow_if_nested ( output, e, 3 );
			}
		}
	};
	return res;
}

int ExceptionHandler::rethrow_if_nested ( NestedExceptionContainer & output, const std::exception & e, int result ) {
	try {
		std::rethrow_if_nested ( e );
		return result;
	} catch ( ... ) {
		return handlerRec ( output, handlers ( ).rbegin ( ) );
	}
}

int ExceptionHandler::handlerRec ( NestedExceptionContainer & output, const std::vector < std::function < int ( NestedExceptionContainer & ) > >::const_reverse_iterator & iter ) {
	if ( iter == handlers ( ).rend ( ) ) {
		output.push_back ( "Unhandled exception" );
		return CLI_RESULT_ON_UNKNOWN_EXCEPTION;
	}

	try {
		return ( * iter ) ( output );
	} catch ( ... ) {
		return handlerRec ( output, std::next ( iter ) );
	}
}

int ExceptionHandler::handle ( NestedExceptionContainer & output ) {
	return handlerRec ( output, handlers ( ).rbegin ( ) );
}

int ExceptionHandler::handle ( ext::ostream & output ) {
	NestedExceptionContainer exceptions;
	int ret = ExceptionHandler::handle ( exceptions );

	for ( size_t i = 0; i < exceptions.getExceptions ( ).size ( ); i++ ) {
		output << i << " " << exceptions.getExceptions ( ) [ i ] << std::endl;
	}

	return ret;
}

} /* namespace alib */
