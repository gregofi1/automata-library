#pragma once

#include <sstream>

#include <ext/ostream>
#include <ext/istream>

namespace ext {

class ostringstream : public ostream {
public:
	ostringstream ( );

	ostringstream ( ostringstream && rhs ) noexcept;

	~ostringstream ( ) noexcept override;

	ostringstream & operator = ( ostringstream && ) noexcept;

	void swap ( ostringstream & other );

	std::string str ( ) const &;

	void str ( const std::string & str );

	std::stringbuf * rdbuf ( );

	const std::stringbuf * rdbuf ( ) const;
};

class istringstream : public istream {
public:
	istringstream ( const std::string & data = "" );

	istringstream ( istringstream && rhs ) noexcept;

	~istringstream ( ) noexcept override;

	istringstream & operator = ( istringstream && ) noexcept;

	void swap ( istringstream & other );

	std::string str ( ) const &;

	void str ( const std::string & str );

	std::stringbuf * rdbuf ( );

	const std::stringbuf * rdbuf ( ) const;
};

} /* namespace ext */
