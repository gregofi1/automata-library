/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <compare>

namespace ext {

/**
 * \brief
 * Implementation of managed value mimicking the behavior of the underlying value. The class is designed to fire change events when the value is modified.
 *
 * \tparam T the type of stored values
 */
template < class T >
class managed_value {
	/**
	 * \brief
	 * The inner values holder.
	 */
	T m_data;

	std::vector < std::function < void ( const T & ) > > changeCallbacks;

	void fireChange ( const T & element ) {
		for ( const std::function < void ( const T & ) > & callback : changeCallbacks ) {
			callback ( element );
		}
	}

public:
	void addChangeCallback ( const std::function < void ( const T & ) > & callback ) {
		changeCallbacks.push_back ( callback );
	}

	/**
	 * \brief
	 * The type of values in the set.
	 */
	typedef T value_type;

	/**
	 * Constructor from range of values.
	 *
	 * \tparam Iterator the type of range iterator
	 *
	 * \param range the source range
	 */
	managed_value ( const T & value ) : m_data ( value ) {
	}

	/**
	 * Constructor from range of values.
	 *
	 * \tparam Iterator the type of range iterator
	 *
	 * \param range the source range
	 */
	managed_value ( T && value ) : m_data ( std::move ( value ) ) {
	}

	/**
	 * \brief
	 * Copy constructor.
	 *
	 * \param x the other linear set instance
	 */
	managed_value ( const managed_value & x ) : m_data ( x.m_data ) {
	}

	/**
	 * \brief
	 * Move constructor.
	 *
	 * \param x the other linear set instance
	 */
	managed_value ( managed_value && x ) : m_data ( std::move ( x.m_data ) ) {
	}

	/**
	 * \brief
	 * The destructor of the linear set.
	 */
	~managed_value ( ) {
	}

	/**
	 * \brief
	 * Copy operator of assignmet.
	 *
	 * \param x the other instance
	 *
	 * \return the modified instance
	 */
	managed_value & operator= ( const managed_value & x ) {
		m_data = x.m_data;
		fireChange ( m_data );
		return *this;
	}

	/**
	 * \brief
	 * Move operator of assignmet.
	 *
	 * \param x the other instance
	 *
	 * \return the modified instance
	 */
	managed_value& operator= ( managed_value && x ) {
		m_data = std::move ( x.m_data );
		fireChange ( m_data );
		return *this;
	}

	/**
	 * \brief
	 * Swaps two instances of linear set
	 *
	 * \param x the other instance to swap with
	 */
	void swap ( managed_value & x ) {
		// intentionally swaped values the change fired is before its execution
		this->fireChange ( x.getValue ( ) );
		x.fireChange ( this->getValue ( ) );

		using std::swap;
		swap ( m_data, x.m_data );
	}

	/**
	 * \brief
	 * Compares two set instances by less relation.
	 *
	 * \param other the second instance to compare
	 *
	 * \return instance of compare category given by the type T representing the comparison of the value in this and value in other instance
	 */
	auto operator <=> ( const managed_value < T > & other ) const = default;

};

/**
 * \brief
 * Specialisation of swap for linear set
 *
 * \tparam T the value of the set
 *
 * \param x the first instance
 * \param y the second instance
 */
template < class T >
void swap ( ext::managed_value < T > & x, ext::managed_value < T > & y) {
	x.swap ( y );
}

/**
 * \brief
 * Operator to print the set to the output stream.
 *
 * \param out the output stream
 * \param value the array to print
 *
 * \tparam T the type of values inside the array
 *
 * \return the output stream from the \p out
 */
template< class T >
std::ostream & operator << ( std::ostream & out, const ext::managed_value < T > & value ) {
	out << value.getValue ( );
	return out;
}

} /* namespace ext */
