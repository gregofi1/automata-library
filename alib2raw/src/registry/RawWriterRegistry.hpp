#pragma once

#include <ext/memory>
#include <ext/typeinfo>

#include <alib/string>
#include <alib/set>
#include <alib/map>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class RawWriterRegistry {
public:
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Param >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterRawWriter ( const std::string & param );

	template < class ParamType >
	static void unregisterRawWriter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		unregisterRawWriter ( param );
	}

	static void registerRawWriter ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static void registerRawWriter ( std::string param ) {
		registerRawWriter ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static void registerRawWriter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		registerRawWriter < ParamType > ( std::move ( param ) );
	}

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & param );
};

} /* namespace abstraction */

#include <abstraction/RawWriterAbstraction.hpp>

namespace abstraction {

template < class Param >
std::unique_ptr < abstraction::OperationAbstraction > RawWriterRegistry::EntryImpl < Param >::getAbstraction ( ) const {
	return std::make_unique < abstraction::RawWriterAbstraction < const Param & > > ( );
}


} /* namespace abstraction */

