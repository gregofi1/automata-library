#include <registry/RawReaderRegistry.hpp>
#include <exception/CommonException.h>

namespace abstraction {

ext::map < std::string, std::unique_ptr < RawReaderRegistry::Entry > > & RawReaderRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > readers;
	return readers;
}

void RawReaderRegistry::unregisterRawReader ( const std::string & type ) {
	if ( getEntries ( ).erase ( type ) == 0u )
		throw std::invalid_argument ( "Entry " + type + " not registered." );
}

void RawReaderRegistry::registerRawReader ( std::string type, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( type ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > RawReaderRegistry::getAbstraction ( const std::string & type ) {
	auto iter = std::find_if ( getEntries ( ).begin ( ), getEntries ( ).end ( ), [ & ] ( const std::pair < const std::string, std::unique_ptr < Entry > > & entry ) { return ext::is_same_type ( entry.first, type ); } );
	if ( iter == getEntries ( ).end ( ) ) {
		throw exception::CommonException ( "Entry " + type + " not available." );
	} else {
		return iter->second->getAbstraction ( );
	}
}

} /* namespace abstraction */
