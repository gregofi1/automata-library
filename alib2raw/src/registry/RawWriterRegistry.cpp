#include <registry/RawWriterRegistry.hpp>
#include <exception/CommonException.h>

namespace abstraction {

ext::map < std::string, std::unique_ptr < RawWriterRegistry::Entry > > & RawWriterRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > writers;
	return writers;
}

void RawWriterRegistry::unregisterRawWriter ( const std::string & param ) {
	if ( getEntries ( ).erase ( param ) == 0u )
		throw std::invalid_argument ( "Entry " + param + " not registered." );
}

void RawWriterRegistry::registerRawWriter ( std::string param, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( param ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > RawWriterRegistry::getAbstraction ( const std::string & param ) {
	auto type = getEntries ( ).find ( param );
	if ( type == getEntries ( ).end ( ) )
		throw exception::CommonException ( "Entry " + param + " not available." );

	return type->second->getAbstraction ( );
}

} /* namespace abstraction */
