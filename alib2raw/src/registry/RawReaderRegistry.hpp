#pragma once

#include <ext/memory>
#include <ext/typeinfo>

#include <alib/string>
#include <alib/map>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class RawReaderRegistry {
public:
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Return >
	class EntryImpl : public Entry {
	public:
		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterRawReader ( const std::string & type );

	template < class ReturnType >
	static void unregisterRawReader ( ) {
		std::string type = ext::to_string < ReturnType > ( );
		unregisterRawReader ( type );
	}

	static void registerRawReader ( std::string type, std::unique_ptr < Entry > entry );

	template < class ReturnType >
	static void registerRawReader ( std::string type ) {
		registerRawReader ( std::move ( type ), std::unique_ptr < Entry > ( new EntryImpl < ReturnType > ( ) ) );
	}

	template < class ReturnType >
	static void registerRawReader ( ) {
		std::string type = ext::to_string < ReturnType > ( );
		registerRawReader < ReturnType > ( std::move ( type ) );
	}

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & type );
};

} /* namespace abstraction */

#include <abstraction/RawReaderAbstraction.hpp>

namespace abstraction {

template < class Return >
std::unique_ptr < abstraction::OperationAbstraction > RawReaderRegistry::EntryImpl < Return >::getAbstraction ( ) const {
	return std::make_unique < abstraction::RawReaderAbstraction < Return > > ( );
}

} /* namespace abstraction */

