#include "UnrankedTree.h"

#include <registration/RawRegistration.hpp>

namespace {

auto treeWrite = registration::RawWriterRegister < tree::UnrankedTree < > > ( );
auto treeReader = registration::RawReaderRegister < tree::UnrankedTree < > > ( );

} /* namespace */
