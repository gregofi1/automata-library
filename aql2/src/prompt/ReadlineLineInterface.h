#pragma once

#include <string>

#include <readline/LineInterface.h>

class ReadlineLineInterface final : public cli::LineInterface {
	bool m_allowHistory;

	bool readline ( std::string & line, bool first ) override;

	void lineCallback ( const std::string & line ) const override;

public:
	explicit ReadlineLineInterface ( bool allowHistory ) : m_allowHistory ( allowHistory ) {
	}
};

