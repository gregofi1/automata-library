#include "ReadlinePromptHistory.h"

#include <readline/history.h>

#include <cstring>
#include <array>

static constexpr std::array < std::pair < char, char >, 8 > esc_char_list = { { { '\a', 'a'}, {'\b', 'b'}, {'\f', 'f'}, {'\n', 'n'}, {'\r', 'r'}, {'\t', 't'}, {'\v', 'v'}, {'\\', '\\'} } };

char * ReadlinePromptHistory::descape ( const char * buffer ) {
	size_t l = strlen ( buffer );

	char * dest = static_cast < char * > ( malloc ( ( l + 1 ) * sizeof ( char ) ) );
	char * ptr = dest;

	for ( size_t i = 0; i < l; ++ i ) {
		if ( buffer [ i ] == '\\' ) {
			++ i;
			auto find_by_escaped = [ & ] ( const std::pair < char, char > & esc_pair ) { return esc_pair.second == buffer [ i ]; };
			auto iter = std::find_if ( esc_char_list.begin ( ), esc_char_list.end ( ), find_by_escaped );
			if ( iter == esc_char_list.end ( ) ) {
				free ( dest );
				return strdup ( buffer );
			} else {
				* ptr ++ = iter->first;
			}
		} else {
			* ptr ++ = buffer [ i ];
		}
	}
	* ptr = '\0';

	return dest;
}

char * ReadlinePromptHistory::escape ( const char * buffer){
	size_t l = strlen ( buffer );

	char * dest = static_cast < char * > ( malloc ( ( l * 2 + 1 ) * sizeof ( char ) ) );
	char * ptr = dest;

	for ( size_t i = 0; i < l; ++ i ) {
		auto find_by_actual = [ & ] ( const std::pair < char, char > & esc_pair ) { return esc_pair.first == buffer [ i ]; };
		auto iter = std::find_if ( esc_char_list.begin ( ), esc_char_list.end ( ), find_by_actual );
		if ( iter == esc_char_list.end ( ) ) {
			* ptr ++ = buffer [ i ];
		} else {
			* ptr ++ = '\\';
			* ptr ++ = iter->second;
		}
	}
	* ptr = '\0';

	return dest;
}

template < class Callable >
void ReadlinePromptHistory::history_transform ( Callable callable ) {
	HIST_ENTRY ** history = history_list ( );
	if ( history ) {
		int i = 0;
		while ( * history ) {
			char * tmp = callable ( ( * history )->line );
			HIST_ENTRY * old = replace_history_entry ( i, tmp, ( * history )->data );
			free_history_entry ( old );
			free ( tmp );
			++ history;
			++ i;
		}
	}
}

ReadlinePromptHistory::ReadlinePromptHistory ( std::string history_file ) : m_history_file ( std::move ( history_file ) ) {
	ReadlinePromptHistory::readHistory ( m_history_file );
}

ReadlinePromptHistory::~ ReadlinePromptHistory ( ) {
	ReadlinePromptHistory::writeHistory ( m_history_file );
	clear_history ( );
	free ( history_list ( ) ); // Note: this may not be entiery safe, it depends on the implementation of readline (but 5.0 through 8.2-alpha) are safe.
}

void ReadlinePromptHistory::readHistory ( const std::string & history_file ) {
	read_history ( history_file.c_str ( ) );
	ReadlinePromptHistory::history_transform ( ReadlinePromptHistory::descape );
}

void ReadlinePromptHistory::writeHistory ( const std::string & history_file ) {
	ReadlinePromptHistory::history_transform ( ReadlinePromptHistory::escape );
	write_history ( history_file.c_str ( ) );
}

void ReadlinePromptHistory::addHistory ( const std::string & line ) {
	add_history ( line.c_str ( ) );
}
