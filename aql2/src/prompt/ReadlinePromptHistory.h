#pragma once

#include <string>

class ReadlinePromptHistory {
	static char * descape ( const char * buffer );

	static char * escape ( const char * buffer);

	template < class Callable >
	static void history_transform ( Callable callable );

	std::string m_history_file;

public:
	explicit ReadlinePromptHistory ( std::string history_file );

	ReadlinePromptHistory ( const ReadlinePromptHistory & ) = delete;

	ReadlinePromptHistory ( ReadlinePromptHistory && ) = delete;

	ReadlinePromptHistory & operator = ( const ReadlinePromptHistory & ) = delete;

	ReadlinePromptHistory & operator = ( ReadlinePromptHistory && ) = delete;

	~ ReadlinePromptHistory ( );

	static void readHistory ( const std::string & history_file );

	static void writeHistory ( const std::string & history_file );

	static void addHistory ( const std::string & line );

};

