CFG (
{C, D, E, F, S},
{a, b},
{S -> b | D S,
C -> D a F | b | a,
F -> S D |,
D -> F E,
E -> a | D S
},
C)
