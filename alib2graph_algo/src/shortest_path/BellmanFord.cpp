// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "BellmanFord.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

auto BellmanFord1 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedUndirectedGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFord2 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedUndirectedMultiGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFord3 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedDirectedGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFord4 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedDirectedMultiGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFord5 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedMixedGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFord6 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                   ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                   const graph::WeightedMixedMultiGraph<> &,
                                                   const DefaultNodeType &,
                                                   const DefaultNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFordGrid1 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                       ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                 DefaultWeightType>,
                                                       const grid::WeightedSquareGrid4<> &,
                                                       const DefaultSquareGridNodeType &,
                                                       const DefaultSquareGridNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

auto BellmanFordGrid2 = registration::AbstractRegister<graph::shortest_path::BellmanFord,
                                                       ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                 DefaultWeightType>,
                                                       const grid::WeightedSquareGrid8<> &,
                                                       const DefaultSquareGridNodeType &,
                                                       const DefaultSquareGridNodeType &>(graph::shortest_path::BellmanFord::findPathRegistration);

}

