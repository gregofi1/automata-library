// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "GreedyBestFS.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------

auto GreedyBestFS1 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedUndirectedGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFS2 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedUndirectedMultiGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFS3 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedDirectedGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFS4 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedDirectedMultiGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFS5 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedMixedGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFS6 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                    ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                    const graph::WeightedMixedMultiGraph<> &,
                                                    const DefaultNodeType &,
                                                    const DefaultNodeType &,
                                                    std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                    const DefaultNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFSGrid1 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                        ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                  DefaultWeightType>,
                                                        const grid::WeightedSquareGrid4<> &,
                                                        const DefaultSquareGridNodeType &,
                                                        const DefaultSquareGridNodeType &,
                                                        std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                        const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

auto GreedyBestFSGrid2 = registration::AbstractRegister<graph::shortest_path::GreedyBestFS,
                                                        ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                  DefaultWeightType>,
                                                        const grid::WeightedSquareGrid8<> &,
                                                        const DefaultSquareGridNodeType &,
                                                        const DefaultSquareGridNodeType &,
                                                        std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                        const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::GreedyBestFS::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------

}
