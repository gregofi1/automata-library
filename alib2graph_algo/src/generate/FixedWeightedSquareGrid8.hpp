// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <grid/GridClasses.hpp>

namespace graph {

namespace generate {

class FixedWeightedSquareGrid8 {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  static grid::WeightedSquareGrid8<int, edge::WeightedEdge<ext::pair<int, int>, double> > weighted_grid();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================


// ---------------------------------------------------------------------------------------------------------------------

} // namespace generate

} // namespace graph

