// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <graph/GraphClasses.hpp>

namespace graph {

namespace generate {

class FixedWeightedUndirectedGraph {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  static graph::WeightedUndirectedGraph<int, edge::WeightedEdge<int, double> > weighted_undirected();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================


} // namespace generate

} // namespace graph

