// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <cmath>
#include <alib/pair>
#include <functional>

namespace graph {

namespace heuristic {

class EuclideanDistance {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  template<typename TCoordinate>
  static double euclideanDistance(const ext::pair<TCoordinate, TCoordinate> &goal,
                                  const ext::pair<TCoordinate, TCoordinate> &node);

// ---------------------------------------------------------------------------------------------------------------------

  template<typename TCoordinate>
  static std::function<double(const ext::pair<TCoordinate, TCoordinate> &,
                              const ext::pair<TCoordinate, TCoordinate> &)> euclideanDistanceFunction();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

template<typename TCoordinate>
double EuclideanDistance::euclideanDistance(const ext::pair<TCoordinate, TCoordinate> &goal,
                                            const ext::pair<TCoordinate, TCoordinate> &node) {
  return std::sqrt(
      (node.first - goal.first) * (node.first - goal.first) +
          (node.second - goal.second) * (node.second - goal.second));
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate>
std::function<double(const ext::pair<TCoordinate, TCoordinate> &,
                     const ext::pair<TCoordinate, TCoordinate> &)> EuclideanDistance::euclideanDistanceFunction() {
  return euclideanDistance < TCoordinate >;
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace heuristic

} // namespace graph

