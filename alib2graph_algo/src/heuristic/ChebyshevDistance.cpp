// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "ChebyshevDistance.hpp"

#include <registration/AlgoRegistration.hpp>
#include <common/DefaultTypes.hpp>

namespace {

auto ChebyshevDistance = registration::AbstractRegister<graph::heuristic::ChebyshevDistance,
                                                        std::function<DefaultCoordinateType(const ext::pair<
                                                            DefaultCoordinateType,
                                                            DefaultCoordinateType> &,
                                                                                            const ext::pair<
                                                                                                DefaultCoordinateType,
                                                                                                DefaultCoordinateType> &)> >(
    graph::heuristic::ChebyshevDistance::chebyshevDistanceFunction);

}
