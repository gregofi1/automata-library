// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include <functional>

namespace graph {

namespace heuristic {

class EuclideanDistanceNoSQRT {
// ---------------------------------------------------------------------------------------------------------------------

 public:

  template<typename TCoordinate>
  static double euclideanDistanceNoSQRT(const ext::pair<TCoordinate, TCoordinate> &goal,
                                        const ext::pair<TCoordinate, TCoordinate> &node);

// ---------------------------------------------------------------------------------------------------------------------

  template<typename TCoordinate>
  static std::function<double(const ext::pair<TCoordinate, TCoordinate> &,
                              const ext::pair<TCoordinate, TCoordinate> &)> euclideanDistanceNoSQRTFunction();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

template<typename TCoordinate>
double EuclideanDistanceNoSQRT::euclideanDistanceNoSQRT(const ext::pair<TCoordinate, TCoordinate> &goal,
                                                        const ext::pair<TCoordinate, TCoordinate> &node) {
  return (node.first - goal.first) * (node.first - goal.first) +
      (node.second - goal.second) * (node.second - goal.second);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate>
std::function<double(const ext::pair<TCoordinate, TCoordinate> &,
                     const ext::pair<TCoordinate,
                                     TCoordinate> &)> EuclideanDistanceNoSQRT::euclideanDistanceNoSQRTFunction() {
  return euclideanDistanceNoSQRT < TCoordinate >;
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace heuristic

} // namespace graph

