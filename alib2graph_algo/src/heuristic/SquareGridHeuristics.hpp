// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include "ChebyshevDistance.hpp"
#include "DiagonalDistance.hpp"
#include "EuclideanDistance.hpp"
#include "EuclideanDistanceNoSQRT.hpp"
#include "ManhattenDistance.hpp"

