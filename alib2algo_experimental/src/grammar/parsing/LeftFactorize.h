#pragma once

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class LeftFactorize {
public:
	static void leftFactorize ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal );

};

} /* namespace parsing */

} /* namespace grammar */

