#include "LL1ParseTable.h"

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto LL1ParseTableCFG = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::CFG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableEpsilonFreeCFG = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::EpsilonFreeCFG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableGNF = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::GNF < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableCNF = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::CNF < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableLG  = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableLeftLG  = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LeftLG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableLeftRG  = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::LeftRG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableRightLG = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::RightLG < > & > ( grammar::parsing::LL1ParseTable::parseTable );
auto LL1ParseTableRightRG = registration::AbstractRegister < grammar::parsing::LL1ParseTable, ext::map < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType >, ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > >, const grammar::RightRG < > & > ( grammar::parsing::LL1ParseTable::parseTable );

} /* namespace */
