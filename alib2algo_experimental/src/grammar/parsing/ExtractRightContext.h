#pragma once

#include <grammar/ContextFree/CFG.h>
#include <alib/set>

namespace grammar {

namespace parsing {

class ExtractRightContext {
public:
	static void extractRightContext ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const ext::set < DefaultSymbolType > & nonterminals );

};

} /* namespace parsing */

} /* namespace grammar */

