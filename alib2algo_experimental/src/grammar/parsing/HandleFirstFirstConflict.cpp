#include "HandleFirstFirstConflict.h"
#include "First.h"
#include "LeftFactorize.h"
#include "CornerSubstitution.h"

#include <grammar/ContextFree/CFG.h>

namespace grammar::parsing {

void HandleFirstFirstConflict::handleFirstFirstConflict ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal, const ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > & rhsds ) {
	for ( const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & rhs : rhsds )
		if ( ( !rhs.empty ( ) ) && grammar.getNonterminalAlphabet ( ).contains ( rhs[0] ) && First::first ( grammar, rhs ).contains ( ext::vector < DefaultSymbolType > { terminal } ) ) {
			CornerSubstitution::cornerSubstitution ( grammar, terminal, nonterminal );
			return;
		}

	LeftFactorize::leftFactorize ( grammar, terminal, nonterminal );
}

} /* namespace grammar::parsing */
