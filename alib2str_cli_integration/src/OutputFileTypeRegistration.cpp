#include <registration/OutputFileTypeRegistration.hpp>

#include <registry/StringWriterRegistry.hpp>

#include <abstraction/PackingAbstraction.hpp>

#include <registry/Registry.h>
#include <common/AlgorithmCategories.hpp>

namespace {

	std::unique_ptr < abstraction::OperationAbstraction > dummy3 ( const std::string & typehint ) {
		ext::vector < std::unique_ptr < abstraction::OperationAbstraction > > abstractions;

		abstractions.push_back ( abstraction::StringWriterRegistry::getAbstraction ( typehint ) );

		ext::vector < std::string > templateParams;
		ext::vector < std::string > paramTypes { ext::to_string < std::string > ( ), ext::to_string < std::string > ( ) };
		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
		ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > paramTypeQualifiers { abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ), abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ) };

		abstractions.push_back ( abstraction::Registry::getAlgorithmAbstraction ( "cli::builtin::WriteFile", templateParams, paramTypes, paramTypeQualifiers, category ) );

		std::unique_ptr < abstraction::PackingAbstraction < 2 > > res = std::make_unique < abstraction::PackingAbstraction < 2 > > ( std::move ( abstractions ), 1 );
		res->setInnerConnection ( 0, 1, 1 );
		res->setOuterConnection ( 0, 1, 0 ); // filename
		res->setOuterConnection ( 1, 0, 0 ); // data

		return res;
	}

auto stringOutputFileHandler = registration::OutputFileRegister ( "string", dummy3 );

}
