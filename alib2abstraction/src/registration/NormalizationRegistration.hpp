#pragma once

#include <registry/NormalizeRegistry.hpp>

#include <ext/registration>

namespace registration {

class NormalizationRegisterEmpty {
};

template < class ReturnType >
using NormalizationRegisterImplBase = std::conditional_t < std::is_same < ReturnType, core::normalizationResult < ReturnType > >::value, NormalizationRegisterEmpty, ext::Register < std::list < std::unique_ptr < abstraction::NormalizeRegistry::Entry > >::const_iterator > >;

template < class ReturnType >
class NormalizationRegisterImpl : public NormalizationRegisterImplBase < ReturnType > {
	static NormalizationRegisterImplBase < ReturnType > setup ( ) {
		if constexpr ( std::is_same < ReturnType, core::normalizationResult < ReturnType > >::value ) {
			return NormalizationRegisterImplBase < ReturnType > ( );
		} else {
			return NormalizationRegisterImplBase < ReturnType > ( [ ] ( ) {
				return abstraction::NormalizeRegistry::registerNormalize < ReturnType > ( );
			}, [ ] ( std::list < std::unique_ptr < abstraction::NormalizeRegistry::Entry > >::const_iterator iter ) {
				abstraction::NormalizeRegistry::unregisterNormalize < ReturnType > ( iter );
			} );
		}
	}
public:
	explicit NormalizationRegisterImpl ( ) : NormalizationRegisterImplBase < ReturnType > ( setup ( ) ) {
	}
};

template < class ReturnType >
using NormalizationRegister = std::conditional_t < core::has_eval < core::normalize < ReturnType > >::value, NormalizationRegisterImpl < ReturnType >, NormalizationRegisterEmpty >;

} /* namespace registration */

