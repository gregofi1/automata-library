#pragma once

#include <ext/memory>

#include <abstraction/OperationAbstraction.hpp>
#include <common/AbstractionHelpers.hpp>

namespace abstraction {

class AnyaryOperationAbstractionImpl : virtual public OperationAbstraction {
	ext::vector < std::shared_ptr < abstraction::Value > > m_params;

protected:
	const ext::vector < std::shared_ptr < abstraction::Value > > & getParams ( ) const {
		return m_params;
	}

	virtual std::shared_ptr < abstraction::Value > run ( ) const = 0;

private:
	void attachInput ( const std::shared_ptr < abstraction::Value > & input, size_t index ) override;

	void detachInput ( size_t index ) override;

public:
	AnyaryOperationAbstractionImpl ( ) = default;

	bool inputsAttached ( ) const override;

	std::shared_ptr < abstraction::Value > eval ( ) override;

	size_t numberOfParams ( ) const override {
		return m_params.size ( );
	}

};

template < class ParamType >
class AnyaryOperationAbstraction : virtual public AnyaryOperationAbstractionImpl {
public:
	ext::type_index getParamTypeIndex ( size_t ) const override {
		return ext::type_index ( typeid ( ParamType ) );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getParamTypeQualifiers ( size_t ) const override {
		return abstraction::TypeQualifiers::typeQualifiers < ParamType > ( );
	}

};

} /* namespace abstraction */

