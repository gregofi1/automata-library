#pragma once

#include <memory>

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

template < class Type >
class ReferenceAbstraction : virtual public NaryOperationAbstraction < Type & >, virtual public ValueOperationAbstraction < Type * > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );

		return std::make_shared < abstraction::ValueHolder < Type * > > ( & retrieveValue < Type & > ( param ), true );
	}

};

} /* namespace abstraction */

