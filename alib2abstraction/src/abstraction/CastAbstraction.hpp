#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <object/ObjectFactory.h>

namespace abstraction {

template < class ReturnType, class ParamType >
class CastAbstraction : virtual public NaryOperationAbstraction < const ParamType & >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );

		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( object::ObjectFactory < ReturnType >::construct ( retrieveValue < const ParamType & > ( param ) ), true ); // FIXME revert object::ObjectFactory < ReturnType >::construct back to ReturnType since casting to object::Object will not be needed in new_normalization
	}

};

} /* namespace abstraction */

