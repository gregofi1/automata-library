#include "PackingAbstraction.hpp"

template class abstraction::PackingAbstraction < 2 >;
template class abstraction::PackingAbstraction < 3 >;

namespace abstraction {

PackingAbstractionImpl::LazyValue::LazyValue ( std::unique_ptr < abstraction::OperationAbstraction > ref ) : m_lifeReference ( std::move ( ref ) ) {
}

std::shared_ptr < abstraction::Value > PackingAbstractionImpl::LazyValue::asValue ( bool, bool ) {
	throw std::domain_error ( "Feature not available on lazy value" );
}

ext::type_index PackingAbstractionImpl::LazyValue::getTypeIndex ( ) const {
	return this->getLifeReference( )->getReturnTypeIndex ( );
}

abstraction::TypeQualifiers::TypeQualifierSet PackingAbstractionImpl::LazyValue::getTypeQualifiers ( ) const {
	return this->getLifeReference ( )->getReturnTypeQualifiers ( );
}

std::shared_ptr < abstraction::Value > PackingAbstractionImpl::LazyValue::getProxyAbstraction ( ) {
	if ( cache == nullptr )
		cache = this->getLifeReference ( )->eval ( );
	return cache;
}

const std::unique_ptr < abstraction::OperationAbstraction > & PackingAbstractionImpl::LazyValue::getLifeReference ( ) const {
	return m_lifeReference;
}

bool PackingAbstractionImpl::LazyValue::isTemporary ( ) const {
	if ( cache == nullptr )
		throw std::domain_error ( "Feature not available on unevaluated value" );
	return cache->isTemporary ( );
}

} /* namespace abstraction */
