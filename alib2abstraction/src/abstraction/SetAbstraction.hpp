#pragma once

#include <ext/memory>

#include <abstraction/AnyaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

template < class ParamType >
class SetAbstraction : virtual public AnyaryOperationAbstraction < ParamType >, virtual public ValueOperationAbstraction < ext::set < ParamType > > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		ext::set < ParamType > theSet;
		for ( const std::shared_ptr < abstraction::Value > & param : this->getParams ( ) ) {
			theSet.insert ( abstraction::retrieveValue < ParamType > ( param ) );
		}

		return std::make_shared < abstraction::ValueHolder < ext::set < ParamType > > > ( std::move ( theSet ), true );
	}

};

} /* namespace abstraction */

