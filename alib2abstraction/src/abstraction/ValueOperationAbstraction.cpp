#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

ext::type_index ValueOperationAbstraction < void >::getReturnTypeIndex ( ) const {
	return ext::type_index ( typeid ( void ) );
}

abstraction::TypeQualifiers::TypeQualifierSet ValueOperationAbstraction < void >::getReturnTypeQualifiers ( ) const {
	return abstraction::TypeQualifiers::typeQualifiers < void > ( );
}

} /* namespace abstraction */
