#pragma once

#include <ext/list>
#include <common/AlgorithmCategories.hpp>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

template < class Entry >
std::unique_ptr < abstraction::OperationAbstraction > getOverload ( const ext::list < std::unique_ptr < Entry > > & overloads, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > &, AlgorithmCategories::AlgorithmCategory category );

} /* namespace abstraction */

