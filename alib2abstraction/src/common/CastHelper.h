#pragma once

#include <string>
#include <memory>
#include <abstraction/Value.hpp>
#include <abstraction/TemporariesHolder.h>

namespace abstraction {

class CastHelper {
public:
	static std::shared_ptr < abstraction::Value > eval ( abstraction::TemporariesHolder & environment, const std::shared_ptr < abstraction::Value > & param, const std::string & type );

};

} /* namespace abstraction */

