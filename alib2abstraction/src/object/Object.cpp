#include <object/Object.h>

namespace object {

void Object::unify ( Object & other ) {
	if ( this->m_data.use_count ( ) > other.m_data.use_count ( ) )
		other.m_data = this->m_data;
	else
		this->m_data = other.m_data;
}

Object::Object ( AnyObjectBase * data ) : m_data ( data ) {
}

void Object::setData ( AnyObjectBase * data ) {
	if ( & getData ( ) == data )
		return;

	this->m_data = ext::cow_shared_ptr < AnyObjectBase > ( data );
}

const AnyObjectBase & Object::getData ( ) const & {
	return * m_data;
}

AnyObjectBase & Object::getData ( ) & {
	return * m_data;
}

AnyObjectBase && Object::getData ( ) && {
	return std::move ( * m_data );
}

void Object::setData ( const AnyObjectBase & data ) {
	setData ( data.clone ( ) );
}

void Object::setData ( AnyObjectBase && data ) {
	setData ( std::move ( data ).clone ( ) );
}

std::strong_ordering Object::operator <=> ( const Object & other ) const {
	if ( this->m_data.get ( ) == other.m_data.get ( ) )
		return std::strong_ordering::equal;

	std::strong_ordering res = ( * this->m_data ) <=> ( * other.m_data );
	if ( res == 0 )
		const_cast < Object * > ( this )->unify ( const_cast < Object & > ( other ) );

	return res;
}

bool Object::operator ==( const Object & other ) const {
	if ( this->m_data.get ( ) == other.m_data.get ( ) )
		return true;

	bool res = ( * this->m_data ) == ( * other.m_data );
	if ( res )
		const_cast < Object * > ( this )->unify ( const_cast < Object & > ( other ) );

	return res;
}

ext::ostream & operator <<( ext::ostream & os, const Object & instance ) {
	instance.getData ( ) >> os;
	return os;
}

Object::operator std::string ( ) const {
	return m_data->operator std::string ( );
}

Object & Object::operator ++ ( ) {
	this->getData ( ).increment ( 1 );
	return *this;
}

Object Object::operator ++ ( int ) {
	Object res = * this;
	++ * this;
	return res;
}

Object Object::operator += ( unsigned by ) {
	this->getData ( ).increment ( by );
	return *this;
}

unsigned Object::getId ( ) const {
	return this->getData ( ).getId ( );
}

} /* namespace object */
