/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "AnyObjectBase.h"

namespace object {

/**
 * \brief
 * Represents an adaptor of any type to a class in type hierarchy of objects in the algorithms library.
 *
 * \tparam T the type of the wrapped object.
 */
template < class T >
class AnyObject : public AnyObjectBase {
	/**
	 * \brief
	 * The wrapped object.
	 */
	T m_data;

	/**
	 * \brief
	 * The identifier of unique object
	 */
	unsigned m_id;
public:
	/**
	 * \brief
	 * Constructor of the class based on the value of the wrapped object.
	 *
	 * \param data the object to be wrapped
	 */
	explicit AnyObject ( T, unsigned id = 0);

	/**
	 * @copydoc object::AnyObjectBase::clone ( ) const &
	 */
	AnyObjectBase * clone ( ) const & override;

	/**
	 * @copydoc object::AnyObjectBase::clone ( ) &&
	 */
	AnyObjectBase * clone ( ) && override;

	/**
	 * @copydoc object::AnyObjectBase::operator <=> ( const AnyObjectBase & ) const
	 */
	std::strong_ordering operator <=> ( const AnyObjectBase & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The actual three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same containers
	 */
	std::strong_ordering operator <=> ( const AnyObject < T > & other ) const {
		auto res = this->getData ( ) <=> other.getData ( );
		if ( res == 0 )
			return m_id <=> other.m_id;
		else if ( res < 0 )
			return std::strong_ordering::less;
		else
			return std::strong_ordering::greater;
	}

	/**
	 * @copydoc object::AnyObjectBase::operator == ( const AnyObjectBase & ) const
	 */
	bool operator == ( const AnyObjectBase & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The actual equality comparsion implemention
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same containers
	 */
	bool operator == ( const AnyObject & other ) const {
		bool res = this->getData ( ) == other.getData ( );

		if ( res )
			res = m_id == other.m_id;

		return res;
	}

	/**
	 * @copydoc object::AnyObjectBase::operator >> ( ext::ostream & ) const
	 */
	void operator >>( ext::ostream & out ) const override;

	/**
	 * @copydoc object::AnyObjectBase::operator std::string ( ) const
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc object::AnyObjectBase::increment ( ) const
	 */
	void increment ( unsigned by ) override {
		m_id += by;
	}

	/**
	 * Getter of the wrapped object
	 *
	 * \return reference to the wrapped object.
	 */
	const T & getData ( ) const;

	/**
	 * @copydoc object::AnyObjectBase::getId ( ) const
	 */
	unsigned getId ( ) const override {
		return m_id;
	}
};

template < class T >
AnyObject < T >::AnyObject ( T data, unsigned id ) : m_data ( std::move ( data ) ), m_id ( id ) {
}

template < class T >
AnyObjectBase * AnyObject < T >::clone ( ) const & {
	return new AnyObject(*this);
}

template < class T >
AnyObjectBase * AnyObject < T >::clone() && {
	return new AnyObject(std::move(*this));
}

template < class T >
void AnyObject < T >::operator>>(ext::ostream& out) const {
	out << this->getData ( );
	for ( unsigned i = 0; i < m_id; ++ i )
		out << "'";
}

template < class T >
AnyObject < T > ::operator std::string () const {
	return ext::to_string ( this->getData ( ) ) + std::string ( "'", m_id );
}

template < class T >
const T & AnyObject < T >::getData ( ) const {
	return m_data;
}

} /* namespace object */

