#pragma once

#include <type_traits>

namespace core {

template < typename T >
struct normalize { };

template < class ReturnType >
using normalizationResult = typename std::decay_t < typename std::invoke_result_t < decltype ( core::normalize < ReturnType >::eval ), ReturnType && > >;

/**
 * \brief
 * Type trait to determine existence of eval static method. A boolean field namd value is set to true if provided class (possibly cv-qualified and via reference) has eval static method.
 *
 * \tparam the type to examine
 */
template < typename T >
struct has_eval {
private:
	template < class U >
	static std::true_type test ( U * )
	requires ( std::is_pointer_v < decltype ( & U::eval ) > );
	static std::false_type test ( ... );
public:
	/**
	 * \brief
	 * True if the type decayed type T has clone method.
	 */
	static const bool value = decltype ( has_eval::test ( std::declval < std::decay_t < T > * > ( ) ) )::value;
};

} /* namespace core */
