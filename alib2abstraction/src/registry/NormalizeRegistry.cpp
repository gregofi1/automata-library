#include <registry/NormalizeRegistry.hpp>

#include <ext/algorithm>

namespace abstraction {

ext::map < std::string, std::list < std::unique_ptr < NormalizeRegistry::Entry > > > & NormalizeRegistry::getEntries ( ) {
	static ext::map < std::string, std::list < std::unique_ptr < Entry > > > entries;
	return entries;
}

void NormalizeRegistry::unregisterNormalize ( const std::string & param, std::list < std::unique_ptr < Entry > >::const_iterator iter ) {
	auto & entry = getEntries ( ) [ param ];

	if ( ! ext::range_contains_iterator ( entry.begin ( ), entry.end ( ), iter ) )
		throw std::invalid_argument ( "Normalization entry not found in " + param + "." );

	entry.erase ( iter );
}

std::list < std::unique_ptr < NormalizeRegistry::Entry > >::const_iterator NormalizeRegistry::registerNormalize ( std::string param, std::unique_ptr < Entry > entry ) {
	auto & collection = getEntries ( ) [ std::move ( param ) ];
	return collection.insert ( collection.end ( ), std::move ( entry ) );
}

std::unique_ptr < abstraction::OperationAbstraction > NormalizeRegistry::getAbstraction ( const std::string & param ) {
	auto res = getEntries ( ).find ( param );
	if ( res == getEntries ( ).end ( ) || res->second.empty ( ) )
		throw std::invalid_argument ( "Entry " + param + " not available." );

	return res->second.front( )->getAbstraction ( );
}

bool NormalizeRegistry::hasNormalize ( const std::string & param ) {
	auto res = getEntries ( ).find ( param );
	return res != getEntries ( ).end ( ) && !res->second.empty ( );
}

} /* namespace abstraction */
