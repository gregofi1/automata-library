#include <registry/ValuePrinterRegistry.hpp>

namespace abstraction {

ext::map < std::string, std::unique_ptr < ValuePrinterRegistry::Entry > > & ValuePrinterRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > fileWriters;
	return fileWriters;
}

std::unique_ptr < abstraction::OperationAbstraction > ValuePrinterRegistry::getAbstraction ( const std::string & param ) {
	auto res = getEntries ( ).find ( param );
	if ( res == getEntries ( ).end ( ) )
		throw std::invalid_argument ( "Entry " + param + " not available." );

	return res->second->getAbstraction ( );
}

void ValuePrinterRegistry::unregisterValuePrinter ( const std::string & param ) {
	if ( getEntries ( ).erase ( param ) == 0u )
		throw std::invalid_argument ( "Entry " + param + " not registered." );
}

void ValuePrinterRegistry::registerValuePrinter ( std::string param, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( param ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

template < >
std::unique_ptr < abstraction::OperationAbstraction > ValuePrinterRegistry::EntryImpl < void >::getAbstraction ( ) const {
	return std::make_unique < abstraction::ValuePrinterAbstraction < void > > ( );
}

} /* namespace abstraction */
