#pragma once

#include <ext/memory>
#include <ext/string>
#include <ext/map>
#include <ext/list>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

#include <core/normalize.hpp>

namespace abstraction {

class NormalizeRegistry {
public:
	class Entry : public BaseRegistryEntry {
	};

private:
	template < class Param >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::list < std::unique_ptr < Entry > > > & getEntries ( );

public:
	static void unregisterNormalize ( const std::string & param, std::list < std::unique_ptr < Entry > >::const_iterator iter );

	template < class ParamType >
	static void unregisterNormalize ( std::list < std::unique_ptr < Entry > >::const_iterator iter ) {
		std::string param = ext::to_string < ParamType > ( );
		unregisterNormalize ( param, iter );
	}

	static std::list < std::unique_ptr < Entry > >::const_iterator registerNormalize ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static std::list < std::unique_ptr < Entry > >::const_iterator registerNormalize ( std::string param ) {
		return registerNormalize ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static std::list < std::unique_ptr < Entry > >::const_iterator registerNormalize ( ) {
		std::string param = ext::to_string < ParamType > ( );
		return registerNormalize < ParamType > ( std::move ( param ) );
	}

	static bool hasNormalize ( const std::string & param );

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & param );
};

} /* namespace abstraction */

#include <abstraction/NormalizeAbstraction.hpp>

namespace abstraction {

template < class Param >
std::unique_ptr < abstraction::OperationAbstraction > NormalizeRegistry::EntryImpl < Param >::getAbstraction ( ) const {
	return std::make_unique < NormalizeAbstraction < core::normalizationResult < Param >, Param > > ( );
}

} /* namespace abstraction */

