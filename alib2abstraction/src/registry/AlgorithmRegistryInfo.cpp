#include "AlgorithmRegistryInfo.hpp"
#include "Registry.h"

namespace abstraction {

AlgorithmBaseInfo::AlgorithmBaseInfo ( AlgorithmCategories::AlgorithmCategory category, ext::vector < ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > > params ) : m_category ( category ), m_params ( std::move ( params ) ) {
}

AlgorithmFullInfo::AlgorithmFullInfo ( AlgorithmBaseInfo baseInfo, ext::vector < std::string > paramNames, ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > result ) : AlgorithmBaseInfo ( std::move ( baseInfo ) ), m_paramNames ( std::move ( paramNames ) ), m_result ( std::move ( result ) ) {
}

ext::pair < std::string, abstraction::TypeQualifiers::TypeQualifierSet > AlgorithmFullInfo::getNormalizedResult ( ) const {
	if ( ! abstraction::Registry::hasNormalize ( m_result.first ) )
		return m_result;

	std::unique_ptr < abstraction::OperationAbstraction > normalized = abstraction::Registry::getNormalizeAbstraction ( m_result.first );

	return ext::make_pair ( normalized->getReturnType ( ), normalized->getReturnTypeQualifiers ( ) );
}

} /* namespace abstraction */
