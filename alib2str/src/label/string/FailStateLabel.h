#pragma once

#include <label/FailStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::FailStateLabel > {
	static label::FailStateLabel parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const label::FailStateLabel & label );
};

} /* namespace core */

