#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace regexp {

class RegExpFromStringLexer : public ext::Lexer < RegExpFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		EPS,
		EMPTY,
		TEOF,
		ERROR
	};

	static Token next(ext::istream& input);
};

} /* namespace regexp */

