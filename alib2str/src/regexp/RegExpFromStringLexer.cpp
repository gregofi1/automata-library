#include "RegExpFromStringLexer.h"

namespace regexp {

RegExpFromStringLexer::Token RegExpFromStringLexer::next(ext::istream& input) {
	RegExpFromStringLexer::Token token;
	token.type = TokenType::ERROR;
	token.value = "";
	token.raw = "";
	char character;

L0:
	character = static_cast < char > ( input.get ( ) );
	if ( input.eof ( ) ) {
		token.type = TokenType::TEOF;
		return token;
	} else if ( ext::isspace ( character ) ) {
		token.raw += character;
		goto L0;
	} else if(character == '(') {
		token.type = TokenType::LPAR;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == ')') {
		token.type = TokenType::RPAR;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '+') {
		token.type = TokenType::PLUS;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '*') {
		token.type = TokenType::STAR;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '#') {
		token.value += character;
		token.raw += character;
		goto L1;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}

L1:
	character = static_cast < char > ( input.get ( ) );
	if(input.eof()) {
		token.type = TokenType::TEOF;
		return token;
	} else if(character == 'E') {
		token.type = TokenType::EPS;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '0') {
		token.type = TokenType::EMPTY;
		token.value += character;
		token.raw += character;
		return token;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}
}

} /* namespace regexp */
