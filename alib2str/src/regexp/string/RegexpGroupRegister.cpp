#include <regexp/RegExp.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < regexp::RegExp > ( );

} /* namespace */
