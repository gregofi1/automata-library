#include "RankedNonlinearPattern.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::RankedNonlinearPattern < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::RankedNonlinearPattern < > > ( );

} /* namespace */
