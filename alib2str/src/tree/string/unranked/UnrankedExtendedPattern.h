#pragma once

#include <tree/unranked/UnrankedExtendedPattern.h>
#include <core/stringApi.hpp>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template<class SymbolType >
struct stringApi < tree::UnrankedExtendedPattern < SymbolType > > {
	static tree::UnrankedExtendedPattern < SymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const tree::UnrankedExtendedPattern < SymbolType > & tree );
};

template<class SymbolType >
tree::UnrankedExtendedPattern < SymbolType > stringApi < tree::UnrankedExtendedPattern < SymbolType > >::parse ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	if ( token.type != tree::TreeFromStringLexer::TokenType::UNRANKED_EXTENDED_PATTERN )
		throw exception::CommonException ( "Unrecognised UNRANKED_EXTENDED_PATTERN token." );

	ext::set < SymbolType > nonlinearVariables;
	bool isPattern = false;
	bool isExtendedPattern = false;

	ext::tree < SymbolType > content = tree::TreeFromStringParserCommon::parseUnrankedContent < SymbolType > ( input, isPattern, isExtendedPattern, nonlinearVariables );

	if ( !nonlinearVariables.empty ( ) )
		throw exception::CommonException ( "Unexpected variables recognised" );

	return tree::UnrankedExtendedPattern < SymbolType > ( alphabet::WildcardSymbol::instance < SymbolType > ( ), alphabet::GapSymbol::instance < SymbolType > ( ), alphabet::NodeWildcardSymbol::instance < SymbolType > ( ), content );
}

template<class SymbolType >
bool stringApi < tree::UnrankedExtendedPattern < SymbolType > >::first ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	bool res = token.type == tree::TreeFromStringLexer::TokenType::UNRANKED_EXTENDED_PATTERN;
	tree::TreeFromStringLexer::putback ( input, token );
	return res;
}

template<class SymbolType >
void stringApi < tree::UnrankedExtendedPattern < SymbolType > >::compose ( ext::ostream &, const tree::UnrankedExtendedPattern < SymbolType > & ) {
	throw exception::CommonException ( "Unimplemented." );
}

} /* namespace core */

