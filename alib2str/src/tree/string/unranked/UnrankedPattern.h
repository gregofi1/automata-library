#pragma once

#include <tree/unranked/UnrankedPattern.h>
#include <core/stringApi.hpp>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template<class SymbolType >
struct stringApi < tree::UnrankedPattern < SymbolType > > {
	static tree::UnrankedPattern < SymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const tree::UnrankedPattern < SymbolType > & tree );
};

template<class SymbolType >
tree::UnrankedPattern < SymbolType > stringApi < tree::UnrankedPattern < SymbolType > >::parse ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	if ( token.type != tree::TreeFromStringLexer::TokenType::UNRANKED_PATTERN )
		throw exception::CommonException ( "Unrecognised UNRANKED_PATTERN token." );

	ext::set < SymbolType > nonlinearVariables;

	bool isPattern = false;
	bool isExtendedPattern = false;

	ext::tree < SymbolType > content = tree::TreeFromStringParserCommon::parseUnrankedContent < SymbolType > ( input, isPattern, isExtendedPattern, nonlinearVariables );

	if ( ! nonlinearVariables.empty ( ) )
		throw exception::CommonException ( "Unexpected variables recognised" );
	if ( isExtendedPattern )
		throw exception::CommonException ( "Unexpected node wildcards recognised" );

	return tree::UnrankedPattern < SymbolType > ( alphabet::WildcardSymbol::instance < SymbolType > ( ), alphabet::GapSymbol::instance < SymbolType > ( ), content );
}

template<class SymbolType >
bool stringApi < tree::UnrankedPattern < SymbolType > >::first ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	bool res = token.type == tree::TreeFromStringLexer::TokenType::UNRANKED_PATTERN;
	tree::TreeFromStringLexer::putback ( input, token );
	return res;
}

template<class SymbolType >
void stringApi < tree::UnrankedPattern < SymbolType > >::compose ( ext::ostream & output, const tree::UnrankedPattern < SymbolType > & tree ) {
	output << "UNRANKED_PATTERN ";
	tree::TreeToStringComposerCommon::compose ( output, tree.getSubtreeWildcard ( ), tree.getSubtreeGap ( ), tree.getContent ( ) );
}

} /* namespace core */

