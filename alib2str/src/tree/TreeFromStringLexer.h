#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace tree {

class TreeFromStringLexer : public ext::Lexer < TreeFromStringLexer > {
public:
	enum class TokenType {
		RANKED_TREE,
		RANKED_PATTERN,
		RANKED_EXTENDED_PATTERN,
		RANKED_NONLINEAR_PATTERN,
		UNRANKED_TREE,
		UNRANKED_PATTERN,
		UNRANKED_EXTENDED_PATTERN,
		UNRANKED_NONLINEAR_PATTERN,
		BAR, RANK, SUBTREE_WILDCARD, NODE_WILDCARD, SUBTREE_GAP, NONLINEAR_VARIABLE, TEOF, ERROR
	};

	static Token next ( ext::istream & input );
};

} /* namespace tree */

