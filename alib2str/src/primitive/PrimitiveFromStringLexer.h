#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace primitive {

class PrimitiveFromStringLexer : public ext::Lexer < PrimitiveFromStringLexer > {
public:
	enum class TokenType {
		STRING,
		INTEGER,
		TEOF,
		ERROR
	};

	static Token next(ext::istream& input);
};

} /* namespace primitive */

