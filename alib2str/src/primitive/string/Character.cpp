#include "Character.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

char stringApi < char >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing char from string not implemented");
}

bool stringApi < char >::first ( ext::istream & ) {
	return false;
}

void stringApi < char >::compose ( ext::ostream & output, char primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < char > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, char > ( );

} /* namespace */
