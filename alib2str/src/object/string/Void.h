#pragma once

#include <object/Void.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < object::Void > {
	static object::Void parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const object::Void & primitive );
};

} /* namespace core */

