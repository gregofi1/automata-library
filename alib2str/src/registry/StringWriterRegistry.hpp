#pragma once

#include <ext/memory>
#include <ext/typeinfo>

#include <alib/string>
#include <alib/set>
#include <alib/map>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class StringWriterRegistry {
public:
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;

	};

private:
	template < class Param >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void unregisterStringWriter ( const std::string & param );

	template < class ParamType >
	static void unregisterStringWriter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		unregisterStringWriter ( param );
	}

	static void registerStringWriter ( std::string param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static void registerStringWriter ( std::string param ) {
		registerStringWriter ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static void registerStringWriter ( ) {
		std::string param = ext::to_string < ParamType > ( );
		registerStringWriter < ParamType > ( std::move ( param ) );
	}

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & param );
};

} /* namespace abstraction */

#include <abstraction/StringWriterAbstraction.hpp>

namespace abstraction {

template < class Param >
std::unique_ptr < abstraction::OperationAbstraction > StringWriterRegistry::EntryImpl < Param >::getAbstraction ( ) const {
	return std::make_unique < abstraction::StringWriterAbstraction < const Param & > > ( );
}


} /* namespace abstraction */

