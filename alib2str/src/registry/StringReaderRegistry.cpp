#include <registry/StringReaderRegistry.hpp>
#include <exception/CommonException.h>

namespace abstraction {

ext::map < std::string, ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < StringReaderRegistry::Entry > > > > & StringReaderRegistry::getEntries ( ) {
	static ext::map < std::string, ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > > > readers;
	return readers;
}

void StringReaderRegistry::unregisterStringReader ( const std::string & group, ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < StringReaderRegistry::Entry > > >::const_iterator iter ) {
	auto & entry = getEntries ( ) [ group ];
	if ( ! ext::range_contains_iterator ( entry.begin ( ), entry.end ( ), iter ) )
		throw std::invalid_argument ( "Entry not found in " + group + "." );

	entry.erase ( iter );
	if ( entry.empty ( ) )
		getEntries ( ).erase ( group );
}

ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < StringReaderRegistry::Entry > > >::const_iterator StringReaderRegistry::registerStringReader ( std::string group, std::function < bool ( ext::istream & ) > first, std::unique_ptr < Entry > entry ) {
	auto & collection = getEntries ( ) [ std::move ( group ) ];
	return collection.insert ( collection.end ( ), std::make_pair ( std::move ( first ), std::move ( entry ) ) );
}

std::unique_ptr < abstraction::OperationAbstraction > StringReaderRegistry::getAbstraction ( const std::string & group, const std::string & str ) {
	ext::istringstream ss ( str );
	while ( ext::isspace ( ss.peek ( ) ) )
		ss.get ( );

	auto lambda = [ & ] ( const std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > & entry ) {
		return entry.first ( ss );
	};

	auto entryIterator = getEntries ( ).end ( );
	for ( auto iter = getEntries ( ).begin ( ); iter != getEntries ( ).end ( ); ++ iter )
		if ( ext::is_same_type ( group, iter->first ) ) {
			if ( entryIterator == getEntries ( ).end ( ) )
				entryIterator = iter;
			else
				throw std::invalid_argument ( "Entry " + group + " is ambigous." );
		}

	if ( entryIterator == getEntries ( ).end ( ) ) {
		std::stringstream entryList;
		bool first = true;
		for ( const auto & entry : getEntries ( ) ) {
			entryList << ( ( ! first ) ? ", " : "" ) << entry.first;
			first = false;
		}

		throw exception::CommonException ( "Entry " + group + " not available. Available entries are: " + entryList.str ( ) );
	}

	const auto & entries = entryIterator->second;

	std::streamoff pos = ss.tellg();

	typename ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > >::const_iterator callback = find_if ( entries.begin ( ), entries.end ( ), lambda );
	if ( callback == entries.end ( ) )
		throw exception::CommonException ( "No callback handling input found." );

	if ( pos != ss.tellg ( ) )
		throw exception::CommonException ( "First function of registered callback moved the stream." );

	return callback->second->getAbstraction ( );
}

} /* namespace abstraction */
