#pragma once

#include <ext/memory>
#include <ext/algorithm>
#include <ext/typeinfo>

#include <alib/string>
#include <alib/map>
#include <alib/list>

#include <abstraction/OperationAbstraction.hpp>

#include <core/stringApi.hpp>

namespace abstraction {

class StringReaderRegistry {
public:
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;

		virtual ~Entry ( ) = default;
	};

private:
	template < class Return >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < std::string, ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > > > & getEntries ( );

public:
	static void unregisterStringReader ( const std::string & group, ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > >::const_iterator iter );

	template < class Group >
	static void unregisterStringReader ( ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > >::const_iterator iter ) {
		std::string group = ext::to_string < Group > ( );
		unregisterStringReader ( group, iter );
	}

	static ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > >::const_iterator registerStringReader ( std::string group, std::function < bool ( ext::istream & ) > first, std::unique_ptr < Entry > entry );

	template < class Group, class ReturnType >
	static ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < Entry > > >::const_iterator registerStringReader ( ) {
		return registerStringReader ( ext::to_string < Group > ( ), core::stringApi < ReturnType >::first, std::unique_ptr < Entry > ( new EntryImpl < ReturnType > ( ) ) );
	}

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & group, const std::string & str );
};

} /* namespace abstraction */

#include <abstraction/StringReaderAbstraction.hpp>

namespace abstraction {

template < class Return >
std::unique_ptr < abstraction::OperationAbstraction > StringReaderRegistry::EntryImpl < Return >::getAbstraction ( ) const {
	return std::make_unique < abstraction::StringReaderAbstraction < Return > > ( );
}

} /* namespace abstraction */

