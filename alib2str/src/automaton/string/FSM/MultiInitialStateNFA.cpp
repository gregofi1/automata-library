#include "MultiInitialStateNFA.h"
#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < automaton::MultiInitialStateNFA < > > ( );
auto stringReader = registration::StringReaderRegister < automaton::Automaton, automaton::MultiInitialStateNFA < > > ( );

} /* namespace */
