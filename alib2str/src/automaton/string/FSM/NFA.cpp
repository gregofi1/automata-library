#include "NFA.h"
#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < automaton::NFA < > > ( );
auto stringReader = registration::StringReaderRegister < automaton::Automaton, automaton::NFA < > > ( );

} /* namespace */
