#include <rte/RTE.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < rte::RTE > ( );

} /* namespace */
