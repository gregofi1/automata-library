#pragma once

#include <grammar/properties/IsFITDefinition.h>

#include <common/createUnique.hpp>

namespace grammar {

namespace simplify {

class MakeFITDefinition {
public:
	/*
	 * Checks whether grammar satmakefies rule about S->\eps from FIT definitions.
	 */
	template < class T >
	static T makeFITDefinition ( const T & grammar );
};

template < class T >
T MakeFITDefinition::makeFITDefinition( const T & grammar ) {
	if ( grammar::properties::IsFITDefinition::isFITDefinition ( grammar ) )
		return grammar;

	T res = grammar;
	auto newInitial = common::createUnique ( grammar.getInitialSymbol ( ), grammar.getNonterminalAlphabet ( ), grammar.getTerminalAlphabet ( ) );
	res.addNonterminalSymbol ( newInitial );

	res.addRules ( newInitial, res.getRules ( ).find ( res.getInitialSymbol ( ) )->second );

	res.setInitialSymbol ( newInitial );

	return res;
}

} /* namespace simplify */

} /* namespace grammar */

