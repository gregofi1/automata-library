#include "EpsilonFreeCFG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::EpsilonFreeCFG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::EpsilonFreeCFG < > > ( );

} /* namespace */
