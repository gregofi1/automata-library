#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/StringDataFactory.hpp>

namespace abstraction {

template < class ReturnType >
class StringReaderAbstraction : virtual public NaryOperationAbstraction < std::string && >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( factory::StringDataFactory::fromString ( abstraction::retrieveValue < std::string && > ( param ) ), true );
	}

};

} /* namespace abstraction */

