#include "ObjectsSet.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < ext::set < object::Object > > ( );
auto stringReaded = registration::StringReaderRegister < object::Object, ext::set < object::Object > > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, ext::set < object::Object > > ( );
auto stringReadedGroup = registration::StringReaderRegisterTypeInGroup < object::Object, ext::set < object::Object > > ( );

} /* namespace */
