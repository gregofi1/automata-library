#pragma once

#include <alphabet/BottomOfTheStackSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BottomOfTheStackSymbol > {
	static alphabet::BottomOfTheStackSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::BottomOfTheStackSymbol & symbol );
};

} /* namespace core */

