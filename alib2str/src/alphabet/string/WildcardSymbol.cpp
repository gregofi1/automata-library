#include "WildcardSymbol.h"
#include <alphabet/WildcardSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::WildcardSymbol stringApi < alphabet::WildcardSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing WildcardSymbol from string not implemented");
}

bool stringApi < alphabet::WildcardSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::WildcardSymbol >::compose ( ext::ostream & output, const alphabet::WildcardSymbol & ) {
	output << "#S";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::WildcardSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::WildcardSymbol > ( );

} /* namespace */
