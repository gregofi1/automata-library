#pragma once

#include <alphabet/VariablesBarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::VariablesBarSymbol > {
	static alphabet::VariablesBarSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::VariablesBarSymbol & symbol );
};

} /* namespace core */

