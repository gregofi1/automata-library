#include "NonlinearVariableSymbol.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::NonlinearVariableSymbol < > > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::NonlinearVariableSymbol < > > ( );

} /* namespace */
