#pragma once

#include <alphabet/StartSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::StartSymbol > {
	static alphabet::StartSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::StartSymbol & symbol );
};

} /* namespace core */

