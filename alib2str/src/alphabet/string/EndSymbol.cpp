#include "EndSymbol.h"
#include <alphabet/EndSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::EndSymbol stringApi < alphabet::EndSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing EndSymbol from string not implemented");
}

bool stringApi < alphabet::EndSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::EndSymbol >::compose ( ext::ostream & output, const alphabet::EndSymbol & ) {
	output << "#$";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::EndSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::EndSymbol > ( );

} /* namespace */
