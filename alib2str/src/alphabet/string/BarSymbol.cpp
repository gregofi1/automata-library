#include "BarSymbol.h"
#include <alphabet/BarSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::BarSymbol stringApi < alphabet::BarSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing BarSymbol from string not implemented");
}

bool stringApi < alphabet::BarSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::BarSymbol >::compose ( ext::ostream & output, const alphabet::BarSymbol & ) {
	output << "#|";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::BarSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::BarSymbol > ( );

} /* namespace */
