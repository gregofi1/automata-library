#include "GapSymbol.h"
#include <alphabet/GapSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::GapSymbol stringApi < alphabet::GapSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing GapSymbol from string not implemented");
}

bool stringApi < alphabet::GapSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::GapSymbol >::compose ( ext::ostream & output, const alphabet::GapSymbol & ) {
	output << "#G";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::GapSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::GapSymbol > ( );

} /* namespace */
