#include "StartSymbol.h"
#include <alphabet/StartSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::StartSymbol stringApi < alphabet::StartSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing StartSymbol from string not implemented");
}

bool stringApi < alphabet::StartSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::StartSymbol >::compose ( ext::ostream & output, const alphabet::StartSymbol & ) {
	output << "#^";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::StartSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::StartSymbol > ( );

} /* namespace */
