#pragma once

#include <alphabet/EndSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::EndSymbol > {
	static alphabet::EndSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::EndSymbol & symbol );
};

} /* namespace core */

