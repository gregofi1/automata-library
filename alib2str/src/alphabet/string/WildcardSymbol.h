#pragma once

#include <alphabet/WildcardSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::WildcardSymbol > {
	static alphabet::WildcardSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::WildcardSymbol & symbol );
};

} /* namespace core */

