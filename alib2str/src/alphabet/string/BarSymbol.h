#pragma once

#include <alphabet/BarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BarSymbol > {
	static alphabet::BarSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::BarSymbol & symbol );
};

} /* namespace core */

