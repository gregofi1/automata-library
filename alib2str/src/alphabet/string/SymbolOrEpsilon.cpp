#include "SymbolOrEpsilon.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < common::symbol_or_epsilon < > > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, common::symbol_or_epsilon < > > ( );

} /* namespace */
