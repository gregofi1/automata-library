#pragma once

#include <alphabet/BlankSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BlankSymbol > {
	static alphabet::BlankSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::BlankSymbol & symbol );
};

} /* namespace core */

