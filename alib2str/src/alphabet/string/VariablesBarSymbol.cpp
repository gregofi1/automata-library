#include "VariablesBarSymbol.h"
#include <alphabet/VariablesBarSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::VariablesBarSymbol stringApi < alphabet::VariablesBarSymbol >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing VariablesBarSymbol from string not implemented");
}

bool stringApi < alphabet::VariablesBarSymbol >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::VariablesBarSymbol >::compose ( ext::ostream & output, const alphabet::VariablesBarSymbol & ) {
	output << "#/";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::VariablesBarSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::VariablesBarSymbol > ( );

} /* namespace */
