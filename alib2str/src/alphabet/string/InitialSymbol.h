#pragma once

#include <alphabet/InitialSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::InitialSymbol > {
	static alphabet::InitialSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::InitialSymbol & symbol );
};

} /* namespace core */

