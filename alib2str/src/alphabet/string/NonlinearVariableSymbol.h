#pragma once

#include <alphabet/NonlinearVariableSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < class SymbolType >
struct stringApi < alphabet::NonlinearVariableSymbol < SymbolType > > {
	static alphabet::NonlinearVariableSymbol < SymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::NonlinearVariableSymbol < SymbolType > & symbol );
};

template < class SymbolType >
alphabet::NonlinearVariableSymbol < SymbolType > stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing NonlinearVariableSymbol from string not implemented");
}

template < class SymbolType >
bool stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::first ( ext::istream & ) {
	return false;
}

template < class SymbolType >
void stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::compose ( ext::ostream & output, const alphabet::NonlinearVariableSymbol < SymbolType > & symbol ) {
	output << "$";
	core::stringApi < SymbolType >::compose ( output, symbol.getSymbol ( ) );
}

} /* namespace core */

