#pragma once

#include <alphabet/GapSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::GapSymbol > {
	static alphabet::GapSymbol parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::GapSymbol & symbol );
};

} /* namespace core */
