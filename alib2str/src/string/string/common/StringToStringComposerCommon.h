#pragma once

#include <ostream>
#include <alib/vector>

#include <core/stringApi.hpp>

namespace string {

class StringToStringComposerCommon {
public:
	template < class SymbolType >
	static void composeContent ( ext::ostream & output, const std::vector < SymbolType > & content );
};

template < class SymbolType >
void StringToStringComposerCommon::composeContent ( ext::ostream & output, const std::vector < SymbolType > & content ) {
	bool first = true;
	for ( const SymbolType & symbol : content ) {
		if ( first )
			first = false;
		else
			output << " ";
		core::stringApi < SymbolType >::compose ( output, symbol );

	}
}

} /* namespace string */

