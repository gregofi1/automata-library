#include <catch2/catch.hpp>

#include <object/Object.h>
#include <factory/StringDataFactory.hpp>

#include <sstream>

TEST_CASE ( "Object Test", "[unit][object][label]" ) {
	SECTION ( "Composing" ) {
		std::string input = "(1, 2)'";
		object::Object label = factory::StringDataFactory::fromString (input);

		std::string output = factory::StringDataFactory::toString(label);
		CHECK( input == output );

		ext::ostringstream out;
		factory::StringDataFactory::toStream ( label, out );
		std::string output2 = out.str();
		CHECK( input == output2 );

		ext::ostringstream out2;
		factory::StringDataFactory::toStream ( label, out2 );
		std::string output3 = out2.str();
		CHECK( input == output3 );
	}
}
