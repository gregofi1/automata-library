#include "LevenshteinBitParalelism.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LevenshteinBitParalelismLinearString = registration::AbstractRegister < stringology::simulations::LevenshteinBitParalelism, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & , unsigned > ( stringology::simulations::LevenshteinBitParalelism::search );

} /* namespace */
