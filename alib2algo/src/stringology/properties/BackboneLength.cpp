#include "BackboneLength.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto backboneLengthDFA = registration::AbstractRegister < stringology::properties::BackboneLength, unsigned, const automaton::DFA < > & > ( stringology::properties::BackboneLength::length );

} /* namespace */
