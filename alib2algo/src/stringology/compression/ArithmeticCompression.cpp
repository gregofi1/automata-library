#include "ArithmeticCompression.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ArithmeticCompression = registration::AbstractRegister < stringology::compression::AdaptiveIntegerArithmeticCompression, ext::vector < bool >, const string::LinearString < > & > ( stringology::compression::AdaptiveIntegerArithmeticCompression::compress );

} /* namespace */

