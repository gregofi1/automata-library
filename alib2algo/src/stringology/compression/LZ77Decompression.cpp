#include "LZ77Decompression.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LZ77Decompression = registration::AbstractRegister < stringology::compression::LZ77Decompression, string::LinearString < >, const ext::vector < ext::tuple < unsigned, unsigned, DefaultSymbolType > > & > ( stringology::compression::LZ77Decompression::decompress );

} /* namespace */
