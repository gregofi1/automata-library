#include "ArithmeticDecompression.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ArithmeticDecompression = registration::AbstractRegister < stringology::compression::AdaptiveIntegerArithmeticDecompression, string::LinearString < >, const ext::vector < bool > &, const ext::set < object::Object > &  > ( stringology::compression::AdaptiveIntegerArithmeticDecompression::decompress );

} /* namespace */

