/*
 * Author: Radovan Cerveny
 */

#include "BackwardDAWGMatching.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BackwardDAWGMatchingLinearStringLinearString = registration::AbstractRegister < stringology::query::BackwardDAWGMatching, ext::set < unsigned >, const string::LinearString < > &, const indexes::stringology::SuffixAutomaton < > & > ( stringology::query::BackwardDAWGMatching::match );

} /* namespace */
