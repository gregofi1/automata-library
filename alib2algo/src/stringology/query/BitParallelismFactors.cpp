#include "BitParallelismFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BitParallelismFactorsLinearString = registration::AbstractRegister < stringology::query::BitParallelismFactors, ext::set < unsigned >, const indexes::stringology::BitParallelIndex < > &, const string::LinearString < > & > ( stringology::query::BitParallelismFactors::query );

} /* namespace */
