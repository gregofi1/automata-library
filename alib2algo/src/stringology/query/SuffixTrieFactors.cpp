#include "SuffixTrieFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixTrieFactorsLinearString = registration::AbstractRegister < stringology::query::SuffixTrieFactors, ext::set < unsigned >, const indexes::stringology::SuffixTrie < > &, const string::LinearString < > & > ( stringology::query::SuffixTrieFactors::query );

} /* namespace */
