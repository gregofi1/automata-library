#include "BNDMOccurrences.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto bndmOccurrencesLinearString = registration::AbstractRegister < stringology::query::BNDMOccurrences, ext::set < unsigned >, const indexes::stringology::BitSetIndex < > &, const string::LinearString < > & > ( stringology::query::BNDMOccurrences::query );

} /* namespace */
