#include "GalilSeiferas.h"
#include <registration/AlgoRegistration.hpp>

namespace {

    auto GalilSeiferas = registration::AbstractRegister < stringology::exact::GalilSeiferas, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::GalilSeiferas::match );

} /* namespace */
