#include "QuickSearch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto QuickSearchLinearString = registration::AbstractRegister < stringology::exact::QuickSearch, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::QuickSearch::match );

} /* namespace */
