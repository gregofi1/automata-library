#include "ReversedBoyerMooreHorspool.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReversedBoyerMooreHorpoolLinearStringLinearString = registration::AbstractRegister < stringology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::ReversedBoyerMooreHorspool::match );

} /* namespace */
