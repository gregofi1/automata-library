#include "ExactFactorMatch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactFactorMatchLinearString = registration::AbstractRegister < stringology::exact::ExactFactorMatch, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::ExactFactorMatch::match );

} /* namespace */
