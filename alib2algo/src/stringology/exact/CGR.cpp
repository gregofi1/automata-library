#include "CGR.h"
#include <registration/AlgoRegistration.hpp>

namespace {

    auto CGR = registration::AbstractRegister < stringology::exact::CGR, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::CGR::match );

} /* namespace */
