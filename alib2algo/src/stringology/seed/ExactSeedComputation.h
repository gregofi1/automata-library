#pragma once

#include <alib/set>
#include <alib/pair>
#include <string/LinearString.h>
#include <stringology/seed/ApproximateSeedComputation.h>

namespace stringology::seed {

/**
 * Class to compute the set of all exact seeds of a given string
 *
 * Source: Veronika Dolanska: Implementace automatovych algoritmu na hledani jader (2020)
 */
class ExactSeedComputation{
public:
	/**
	 * Computes all exact seeds of a string
	 * Source: Veronika Dolanska: Implementace automatovych algoritmu na hledani jader (2020)
	 *
	 * @param pattern - string for which seeds are computed
	 * @return set of all exact seeds of the input string and their minimal Hamming distance (= 0)
	 */
	template<class SymbolType>
	static ext::set < ext::pair < string::LinearString < SymbolType >, unsigned > > compute ( const string::LinearString < SymbolType > & pattern ){
		return stringology::seed::ApproximateSeedComputation::compute( pattern, 0, true );
	}
};

}
