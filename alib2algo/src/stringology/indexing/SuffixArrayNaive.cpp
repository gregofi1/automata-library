#include "SuffixArrayNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto suffixArrayNaiveLinearString = registration::AbstractRegister < stringology::indexing::SuffixArrayNaive, indexes::stringology::SuffixArray < >, const string::LinearString < > & > ( stringology::indexing::SuffixArrayNaive::construct );

} /* namespace */
