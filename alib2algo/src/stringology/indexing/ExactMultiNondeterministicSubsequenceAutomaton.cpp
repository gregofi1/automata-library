#include "ExactMultiNondeterministicSubsequenceAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactMultiNondeterministicSubsequenceAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactMultiNondeterministicSubsequenceAutomaton, automaton::EpsilonNFA < DefaultSymbolType, ext::pair < unsigned, unsigned > >, const ext::set < string::LinearString < > > & > ( stringology::indexing::ExactMultiNondeterministicSubsequenceAutomaton::construct );

} /* namespace */
