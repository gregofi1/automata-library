/*
 * Author: Radovan Cerveny
 */

#include "NondeterministicExactSuffixAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixAutomatonNondeterministicLinearString = registration::AbstractRegister < stringology::indexing::NondeterministicExactSuffixAutomaton, automaton::NFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::NondeterministicExactSuffixAutomaton::construct );

} /* namespace */
