#include "BitParallelIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto bitParallelIndexConstructionLinearString = registration::AbstractRegister < stringology::indexing::BitParallelIndexConstruction, indexes::stringology::BitParallelIndex < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::BitParallelIndexConstruction::construct );

} /* namespace */
