#include "HammingMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto HammingMatchingAutomatonLinearString = registration::AbstractRegister <stringology::matching::HammingMatchingAutomaton, automaton::NFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int> >, const string::LinearString < > &, unsigned > ( stringology::matching::HammingMatchingAutomaton::construct );

auto HammingMatchingAutomatonWildcardLinearString = registration::AbstractRegister <stringology::matching::HammingMatchingAutomaton, automaton::NFA < DefaultSymbolType, ext::pair<unsigned int, unsigned int> >, const string::WildcardLinearString < > &, unsigned > ( stringology::matching::HammingMatchingAutomaton::construct );

} /* namespace */
