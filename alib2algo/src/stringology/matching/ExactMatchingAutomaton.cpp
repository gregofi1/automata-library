#include "ExactMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactMatchingAutomatonLinearString = registration::AbstractRegister < stringology::matching::ExactMatchingAutomaton, automaton::NFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::matching::ExactMatchingAutomaton::construct );

} /* namespace */
