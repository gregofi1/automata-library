#pragma once

#include <indexes/stringology/BitParallelIndex.h>
#include <string/LinearString.h>
#include <exception/CommonException.h>

namespace stringology {

namespace matching {

/**
 * Constructs a bit parallel index for given string.
 *
 */
class WideBNDMMatcherConstruction {
public:
	/**
	 * Creates suffix trie
	 * @param string string to construct suffix trie for
	 * @return automaton
	 */
	template < class SymbolType >
	static indexes::stringology::BitParallelIndex < SymbolType > construct ( const string::LinearString < SymbolType > & w );

};

template < class SymbolType >
indexes::stringology::BitParallelIndex < SymbolType > WideBNDMMatcherConstruction::construct ( const string::LinearString < SymbolType > & w ) {
	ext::map < SymbolType, ext::vector < bool > > res;
	for ( const SymbolType & symbol : w.getAlphabet ( ) )
		res [ symbol ].resize ( w.getContent ( ).size ( ) );

	for ( unsigned i = 0; i < w.getContent ( ).size ( ); ++i )
		res [ w.getContent ( ) [ i ] ] [ w.getContent ( ).size ( ) - i - 1 ] = true;

	return indexes::stringology::BitParallelIndex < SymbolType > ( w.getAlphabet ( ), res );
}

} /* namespace matching */

} /* namespace stringology */

