#include "LeftRecursionRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LeftRecursionRemoverEpsilonFreeCFG = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::EpsilonFreeCFG < >, const grammar::EpsilonFreeCFG < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverCNF = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::EpsilonFreeCFG < >, const grammar::CNF < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverGNF = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::GNF < >, const grammar::GNF < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverRightRG = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverRightLG = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::RightLG < >, const grammar::RightLG < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverLeftRG = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::RightRG < >, const grammar::LeftRG < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

auto LeftRecursionRemoverLeftLG = registration::AbstractRegister < grammar::simplify::LeftRecursionRemover, grammar::RightLG < >, const grammar::LeftLG < > & > ( grammar::simplify::LeftRecursionRemover::remove, "grammar" ).setDocumentation (
"Removes left recursion from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without left recursion" );

} /* namespace */
