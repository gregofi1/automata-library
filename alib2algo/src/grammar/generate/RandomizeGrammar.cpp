#include "RandomizeGrammar.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RandomizeGrammarLeftRG = registration::AbstractRegister < grammar::generate::RandomizeGrammar, grammar::LeftRG < >, const grammar::LeftRG < > & > ( grammar::generate::RandomizeGrammar::randomize, "gram" ).setDocumentation (
"Shuffle the set of nonterminal symbols.\n\
\n\
@param gram the grammar to shuffle\n\
@return grammar with shuffled nonterminal symbols" );

auto RandomizeGrammarLeftLG = registration::AbstractRegister < grammar::generate::RandomizeGrammar, grammar::LeftLG < >, const grammar::LeftLG < > & > ( grammar::generate::RandomizeGrammar::randomize, "gram" ).setDocumentation (
"Shuffle the set of nonterminal symbols.\n\
\n\
@param gram the grammar to shuffle\n\
@return grammar with shuffled nonterminal symbols" );

auto RandomizeGrammarRightRG = registration::AbstractRegister < grammar::generate::RandomizeGrammar, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::generate::RandomizeGrammar::randomize, "gram" ).setDocumentation (
"Shuffle the set of nonterminal symbols.\n\
\n\
@param gram the grammar to shuffle\n\
@return grammar with shuffled nonterminal symbols" );

auto RandomizeGrammarRightLG = registration::AbstractRegister < grammar::generate::RandomizeGrammar, grammar::RightLG < >, const grammar::RightLG < > & > ( grammar::generate::RandomizeGrammar::randomize, "gram" ).setDocumentation (
"Shuffle the set of nonterminal symbols.\n\
\n\
@param gram the grammar to shuffle\n\
@return grammar with shuffled nonterminal symbols" );

auto RandomizeGrammarCFG = registration::AbstractRegister < grammar::generate::RandomizeGrammar, grammar::CFG < >, const grammar::CFG < > & > ( grammar::generate::RandomizeGrammar::randomize, "gram" ).setDocumentation (
"Shuffle the set of nonterminal symbols.\n\
\n\
@param gram the grammar to shuffle\n\
@return grammar with shuffled nonterminal symbols" );

} /* namespace */
