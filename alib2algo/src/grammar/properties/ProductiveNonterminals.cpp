#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "ProductiveNonterminals.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ProductiveNonterminalsCFG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::CFG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::EpsilonFreeCFG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsGNF = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::GNF < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsCNF = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::CNF < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsLG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::LG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsLeftLG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::LeftLG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsLeftRG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::LeftRG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsRightLG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::RightLG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

auto ProductiveNonterminalsRightRG = registration::AbstractRegister < grammar::properties::ProductiveNonterminals, ext::set < DefaultSymbolType >, const grammar::RightRG < > & > ( grammar::properties::ProductiveNonterminals::getProductiveNonterminals, "grammar" ).setDocumentation (
"Retrieves set of nonterminals N = { A : A => T* }\n\
\n\
@param grammar the tested grammar\n\
@returns set of nonterminals that can be rewriten to a sentence" );

} /* namespace */
