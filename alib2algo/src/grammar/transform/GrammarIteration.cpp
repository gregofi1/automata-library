#include "GrammarIteration.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GrammarIterationCFG = registration::AbstractRegister < grammar::transform::GrammarIteration,
	grammar::CFG < >,
	const grammar::CFG < > & > ( grammar::transform::GrammarIteration::iteration, "first" ).setDocumentation (
"Iterates a context-free grammars.\n\
\n\
@param grammar CFG grammar (G1)\n\
@return context-free grammar G where L(G) = L(G1)*" );

} /* namespace */
