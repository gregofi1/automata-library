#include "GrammarConcatenation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GrammarConcatenationCFG = registration::AbstractRegister < grammar::transform::GrammarConcatenation,
	grammar::CFG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >,
	const grammar::CFG < > &,
	const grammar::CFG < > & > ( grammar::transform::GrammarConcatenation::concatenation, "first", "second" ).setDocumentation (
"Concatenates two context-free grammars.\n\
\n\
@param first First grammar (G1)\n\
@param second Second grammar (G2)\n\
@return context-free grammar G where L(G) = L(G1).L(G2)" );

} /* namespace */
