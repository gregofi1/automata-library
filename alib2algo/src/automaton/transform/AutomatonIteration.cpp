#include "AutomatonIteration.h"
#include <common/createUnique.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomatonIterationDFA = registration::AbstractRegister < automaton::transform::AutomatonIteration, automaton::NFA < >, const automaton::DFA < > & > ( automaton::transform::AutomatonIteration::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton without epsilon transitions.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA accepting the iteration of language that @p automaton accepted" );

auto AutomatonIterationNFA = registration::AbstractRegister < automaton::transform::AutomatonIteration, automaton::NFA < >, const automaton::NFA < > & > ( automaton::transform::AutomatonIteration::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton without epsilon transitions.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA accepting the iteration of language that @p automaton accepted" );

} /* namespace */
