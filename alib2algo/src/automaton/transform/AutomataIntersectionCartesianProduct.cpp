#include "AutomataIntersectionCartesianProduct.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomataIntersectionCartesianProductNFA2 = registration::AbstractRegister < automaton::transform::AutomataIntersectionCartesianProduct, automaton::NFA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::NFA < > &, const automaton::NFA < > & > ( automaton::transform::AutomataIntersectionCartesianProduct::intersection, "first", "second" ).setDocumentation (
"Intersects two finite automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the intersection of two automata" );

auto AutomataIntersectionCartesianProductDFA2 = registration::AbstractRegister < automaton::transform::AutomataIntersectionCartesianProduct, automaton::DFA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFA < > &, const automaton::DFA < > & > ( automaton::transform::AutomataIntersectionCartesianProduct::intersection ).setDocumentation (
"Intersects two finite automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the intersection of two automata" );

auto AutomataIntersectionCartesianProductDFTA = registration::AbstractRegister < automaton::transform::AutomataIntersectionCartesianProduct, automaton::DFTA < DefaultSymbolType, ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFTA < > &, const automaton::DFTA < > & > ( automaton::transform::AutomataIntersectionCartesianProduct::intersection ).setDocumentation (
"Intersects two finite automata.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the intersection of two automata" );

} /* namespace */
