/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>

#include <alib/deque>
#include <alib/set>
#include <alib/map>

#include <automaton/FSM/ExtendedNFA.h>
#include <automaton/FSM/CompactNFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>
#include <automaton/TA/ArcFactoredDeterministicZAutomaton.h>
#include <automaton/TA/ArcFactoredNondeterministicZAutomaton.h>
#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

namespace automaton {

namespace properties {

/**
 * Find all useful states of an automaton.
 * A state is useful if there exists any sequence of transitions that leads to the final state.
 */
class UsefulStates {
public:
	/**
	 * Finds all useful states of a finite automaton.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite automaton
	 * @param fsm automaton
	 * @return set of useful states of @p fsm
	 */
	template < class T >
	requires isDFA < T > || isNFA < T > || isEpsilonNFA < T > || isMultiInitialStateNFA < T > || isMultiInitialStateEpsilonNFA < T > || isCompactNFA < T > || isExtendedNFA < T >
	static ext::set < typename T::StateType > usefulStates ( const T & fsm );

	/**
	 * Finds all useful states of a finite tree automaton.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite tree automaton
	 * @param fta automaton
	 * @return set of useful states of @p fta
	 */
	template < class T >
	requires isDFTA < T > || isNFTA < T >
	static ext::set < typename T::StateType > usefulStates ( const T & fta );

	/**
	 * Finds all useful states of a arc-factored z-automaton.
	 * Using closure implementation of the BFS algorithm.
	 *
	 * @tparam T type of finite tree automaton
	 * @param afza automaton
	 * @return set of useful states of @p afza
	 */
	template < class T >
	requires isAFDZA < T > || isAFNZA < T >
	static ext::set < typename T::StateType > usefulStates ( const T & afza );
};

template < class T >
requires isDFA < T > || isNFA < T > || isEpsilonNFA < T > || isMultiInitialStateNFA < T > || isMultiInitialStateEpsilonNFA < T > || isCompactNFA < T > || isExtendedNFA < T >
ext::set < typename T::StateType > UsefulStates::usefulStates ( const T & fsm ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );
	Qi.at( 0 ) = fsm.getFinalStates( );

	int i = 1;

	// 1bc
	while( true ) {
		Qi.push_back( Qi.at( i - 1 ) ); // copy Qi-1 into Qi
		for( const auto & p : Qi.at( i - 1 ) )
			for( const auto & t : fsm.getTransitionsToState( p ) )
				Qi.at( i ).insert( t.first.first );

		if( Qi.at( i ) == Qi.at( i - 1 ) )
			break;

		i = i + 1;
	}
	return Qi.at( i );
}

template < class T >
requires isDFTA < T > || isNFTA < T >
ext::set < typename T::StateType > UsefulStates::usefulStates ( const T & fta ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );
	Qi.at( 0 ) = fta.getFinalStates( );

	int i = 0;

	// 1bc
	do {
		i = i + 1;

		Qi.push_back( Qi.at( i - 1 ) ); // copy Qi-1 into Qi
		for( const auto & p : Qi.at( i - 1 ) )
			for( const auto & t : fta.getTransitionsToState ( p ) )
				Qi.at( i ).insert( t.first.second.begin ( ), t.first.second.end ( ) );

	} while ( Qi.at( i ) != Qi.at( i - 1 ) );

	return Qi.at( i );
}

template < class T >
requires isAFDZA < T > || isAFNZA < T >
ext::set < typename T::StateType > UsefulStates::usefulStates ( const T & afza ) {
	using StateType = typename T::StateType;

	// 1a
	ext::deque<ext::set<StateType>> Qi;
	Qi.push_back( ext::set<StateType>( ) );
	Qi.at( 0 ) = afza.getFinalStates( );

	int i = 0;

	// 1bc
	do {
		i = i + 1;

		Qi.push_back( Qi.at( i - 1 ) ); // copy Qi-1 into Qi
		for( const auto & p : Qi.at( i - 1 ) )
			for( const auto & t : afza.getTransitionsToState ( p ) ) {
				if ( t.first.template is < ext::pair < StateType, StateType > > ( ) ) {
					const auto& lhs = t.first.template get < ext::pair < StateType, StateType > > ( );
					Qi.at( i ).insert( lhs.first );
					Qi.at( i ).insert( lhs.second );
				}
			}

	} while ( Qi.at( i ) != Qi.at( i - 1 ) );

	return Qi.at( i );
}

} /* namespace properties */

} /* namespace automaton */

