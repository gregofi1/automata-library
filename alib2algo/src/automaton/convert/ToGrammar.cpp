#include "ToGrammar.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarNFA = registration::AbstractRegister < automaton::convert::ToGrammar, grammar::RightRG < >, const automaton::NFA < > & > ( automaton::convert::ToGrammar::convert, "automaton" ).setDocumentation (
"Performs the conversion (@sa ToGrammarRightRG)\n\
\n\
@param automaton the automaton to convert\n\
@return right regular grammar equivalent to the input @p automaton" );

auto ToGrammarDFA = registration::AbstractRegister < automaton::convert::ToGrammar, grammar::RightRG < >, const automaton::DFA < > & > ( automaton::convert::ToGrammar::convert, "automaton" ).setDocumentation (
"Performs the conversion (@sa ToGrammarRightRG)\n\
\n\
@param automaton the automaton to convert\n\
@return right regular grammar equivalent to the input @p automaton" );

} /* namespace */
