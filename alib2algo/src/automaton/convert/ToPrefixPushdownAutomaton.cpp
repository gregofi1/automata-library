#include "ToPrefixPushdownAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToAutomatonDPDA = registration::AbstractRegister < automaton::convert::ToPrefixPushdownAutomaton, automaton::DPDA < DefaultSymbolType, ext::variant < DefaultStateType, alphabet::BottomOfTheStackSymbol >, char >, const automaton::ArcFactoredDeterministicZAutomaton < > & > ( automaton::convert::ToPrefixPushdownAutomaton::convert, "afdza" ).setDocumentation (
"Performs the conversion of the deterministic Z Automaton to the deterministic PDA\n\
\n\
@param afdza Deterministic Z Automaton to convert\n\
@return (D)PDA equivalent to original finite tree automaton reading linearized prefix bar tree" );

auto ToAutomatonNPDA = registration::AbstractRegister < automaton::convert::ToPrefixPushdownAutomaton, automaton::NPDA < DefaultSymbolType, ext::variant < DefaultStateType, alphabet::BottomOfTheStackSymbol >, char >, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( automaton::convert::ToPrefixPushdownAutomaton::convert, "afnza" ).setDocumentation (
"Performs the conversion of the nondeterministic Z Automaton to the nondeterministic PDA\n\
\n\
@param afnza Nondeterministic Z Automaton to convert\n\
@return (N)PDA equivalent to original finite tree automaton reading linearized prefix bar tree" );

} /* namespace */
