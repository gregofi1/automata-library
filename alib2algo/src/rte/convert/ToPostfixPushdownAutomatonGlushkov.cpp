#include "ToPostfixPushdownAutomatonGlushkov.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToPostfixPushdownAutomatonGlushkovFormalRTE = registration::AbstractRegister < rte::convert::ToPostfixPushdownAutomatonGlushkov,
			automaton::NPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::EndSymbol >, ext::variant < ext::set < common::ranked_symbol < ext::pair < DefaultSymbolType, unsigned > > >, alphabet::BottomOfTheStackSymbol >, char >,
			const rte::FormalRTE < > & > ( rte::convert::ToPostfixPushdownAutomatonGlushkov::convert );

} /* namespace */
