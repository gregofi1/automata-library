#include "ToFTAThompson.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToFTAThompsonFormalRTE = registration::AbstractRegister < rte::convert::ToFTAThompson,
			automaton::EpsilonNFTA < DefaultSymbolType, unsigned >,
			const rte::FormalRTE < > & > ( rte::convert::ToFTAThompson::convert );

} /* namespace */
