#include "ToFTAGlushkov.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToFTAGlushkovFormalRTE = registration::AbstractRegister < rte::convert::ToFTAGlushkov,
			automaton::NFTA < DefaultSymbolType,
				ext::set < common::ranked_symbol < ext::pair < DefaultSymbolType, unsigned > > > >,
			const rte::FormalRTE < > & > ( rte::convert::ToFTAGlushkov::convert );

} /* namespace */
