#include "StringReverse.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LinearStringReverse = registration::AbstractRegister < string::transform::StringReverse, string::LinearString < >, const string::LinearString < > & > ( string::transform::StringReverse::reverse, "arg" ).setDocumentation (
"Implements the reverse of a string.\n\
\n\
@param arg the string to reverse\n\
@return string arg ^ R" );

} /* namespace */
