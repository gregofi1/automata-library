#include "MaximalSuffix.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto MaxSuffixLinearString = registration::AbstractRegister < string::properties::MaximalSuffix, ext::pair < size_t, size_t >, const string::LinearString < > & > ( string::properties::MaximalSuffix::construct );

} /* namespace */
