#include "PeriodicPrefix.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto PeriodicPrefix = registration::AbstractRegister < string::properties::PeriodicPrefix, ext::pair < ssize_t, ssize_t >, const string::LinearString < > & > ( string::properties::PeriodicPrefix::construct );

} /* namespace */
