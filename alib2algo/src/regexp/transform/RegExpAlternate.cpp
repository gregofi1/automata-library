#include "RegExpAlternate.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpAlternateFormalRegExp = registration::AbstractRegister < regexp::transform::RegExpAlternate, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const regexp::FormalRegExp < > & > ( regexp::transform::RegExpAlternate::alternate, "first", "second" ).setDocumentation (
"Implements alternation of two regular expressions.\n\
\n\
@param first the first regexp to alternate\n\
@param second the second regexp to alternate\n\
@return regexp describing first + second" );

auto RegExpAlternateUnboundedRegExp = registration::AbstractRegister < regexp::transform::RegExpAlternate, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const regexp::UnboundedRegExp < > & > ( regexp::transform::RegExpAlternate::alternate, "first", "second" ).setDocumentation (
"Implements alternation of two regular expressions.\n\
\n\
@param first the first regexp to alternate\n\
@param second the second regexp to alternate\n\
@return regexp describing first + second" );

} /* namespace */
