#include "ToGrammarRightRGGlushkov.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarRightRGGlushkovUnboundedRegExp = registration::AbstractRegister < regexp::convert::ToGrammarRightRGGlushkov, grammar::RightRG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::UnboundedRegExp < > & > ( regexp::convert::ToGrammarRightRGGlushkov::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Glushkov's method of neighbours.\n\
\n\
@param regexp the regexp to convert\n\
@return right regular grammar generating the language described by the original regular expression" );

auto ToGrammarRightRGGlushkovFormalRegExp = registration::AbstractRegister < regexp::convert::ToGrammarRightRGGlushkov, grammar::RightRG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::FormalRegExp < > & > ( regexp::convert::ToGrammarRightRGGlushkov::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Glushkov's method of neighbours.\n\
\n\
@param regexp the regexp to convert\n\
@return right regular grammar generating the language described by the original regular expression" );

} /* namespace */
