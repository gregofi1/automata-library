#include "ToAutomatonGlushkov.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToAutomatonGlushkovUnboundedRegExp = registration::AbstractRegister < regexp::convert::ToAutomatonGlushkov, automaton::NFA < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::UnboundedRegExp < > & > ( regexp::convert::ToAutomatonGlushkov::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Glushkov's method of neighbours.\n\
\n\
@param regexp the regexp to convert\n\
@return finite automaton accepting the language described by the original regular expression" );

auto ToAutomatonGlushkovFormalRegExp = registration::AbstractRegister < regexp::convert::ToAutomatonGlushkov, automaton::NFA < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >, const regexp::FormalRegExp < > & > ( regexp::convert::ToAutomatonGlushkov::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Glushkov's method of neighbours.\n\
\n\
@param regexp the regexp to convert\n\
@return finite automaton accepting the language described by the original regular expression" );

} /* namespace */
