#pragma once

#include <tree/ranked/PrefixRankedBarPattern.h>
#include <tree/ranked/PrefixRankedBarNonlinearPattern.h>

namespace tree {

namespace properties {

class LastVariableOffsetBack {
public:
	template < class SymbolType >
	static size_t offset ( const tree::PrefixRankedBarPattern < SymbolType > & pattern );
	template < class SymbolType >
	static size_t offset ( const tree::PrefixRankedBarNonlinearPattern < SymbolType > & pattern );

};

template < class SymbolType >
size_t LastVariableOffsetBack::offset ( const tree::PrefixRankedBarPattern < SymbolType > & pattern ) {
	return offset ( tree::PrefixRankedBarNonlinearPattern < SymbolType > ( pattern ) );
}

template < class SymbolType >
size_t LastVariableOffsetBack::offset ( const tree::PrefixRankedBarNonlinearPattern < SymbolType > & pattern ) {
	 // find the distance between the end of the pattern and the index
	 // of the last symbol representing the variable
	size_t offset = pattern.getContent ( ).size ( );

	for ( size_t i = 0; i < pattern.getContent ( ).size ( ); i++ )
		if ( pattern.getContent ( )[i] == pattern.getSubtreeWildcard ( ) || pattern.getNonlinearVariables ( ).count ( pattern.getContent ( )[i] ) )
			offset = pattern.getContent ( ).size ( ) - i - 1;

	return offset;
}

} /* namespace properties */

} /* namespace tree */

