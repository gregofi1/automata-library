#include "NormalizeTreeLabels.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NormalizeTreeLabelsRankedTree = registration::AbstractRegister < tree::simplify::NormalizeTreeLabels, tree::RankedTree < unsigned >, const tree::RankedTree < > & > ( tree::simplify::NormalizeTreeLabels::normalize );

} /* namespace */
