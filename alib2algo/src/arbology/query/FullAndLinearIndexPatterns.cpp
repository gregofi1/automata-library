#include "FullAndLinearIndexPatterns.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto fullAndLinearIndexPatternsPrefixRankedPattern = registration::AbstractRegister < arbology::query::FullAndLinearIndexPatterns, ext::set < unsigned >, const indexes::arbology::FullAndLinearIndex < > &, const tree::PrefixRankedPattern < > & > ( arbology::query::FullAndLinearIndexPatterns::query );
auto fullAndLinearIndexPatternsPrefixRankedBarPattern = registration::AbstractRegister < arbology::query::FullAndLinearIndexPatterns, ext::set < unsigned >, const indexes::arbology::FullAndLinearIndex < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::query::FullAndLinearIndexPatterns::query );

} /* namespace */
