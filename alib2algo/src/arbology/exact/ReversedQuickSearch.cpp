#include "ReversedQuickSearch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarTree < > & > ( arbology::exact::ReversedQuickSearch::match );
auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::exact::ReversedQuickSearch::match );
auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarNonlinearPattern < > & > ( arbology::exact::ReversedQuickSearch::match );
auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedTree = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedTree < > & > ( arbology::exact::ReversedQuickSearch::match );
auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedPattern = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedPattern < > & > ( arbology::exact::ReversedQuickSearch::match );
auto ReversedQuickSearchPrefixRankedBarTreePrefixRankedNonlinearPattern = registration::AbstractRegister < arbology::exact::ReversedQuickSearch, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedNonlinearPattern < > & > ( arbology::exact::ReversedQuickSearch::match );

} /* namespace */
