#include "ReversedBoyerMooreHorspool.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarTree < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );
auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );
auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarNonlinearPattern < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );
auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedTree = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedTree < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );
auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedPattern = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedPattern < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );
auto ReversedBoyerMooreHorspoolPrefixRankedBarTreePrefixRankedNonlinearPattern = registration::AbstractRegister < arbology::exact::ReversedBoyerMooreHorspool, ext::set < unsigned >, const tree::PrefixRankedTree < > &, const tree::PrefixRankedNonlinearPattern < > & > ( arbology::exact::ReversedBoyerMooreHorspool::match );

} /* namespace */
