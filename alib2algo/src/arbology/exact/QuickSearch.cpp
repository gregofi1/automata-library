#include "QuickSearch.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto QuickSearchPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister < arbology::exact::QuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarTree < > & > ( arbology::exact::QuickSearch::match );
auto QuickSearchPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister < arbology::exact::QuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::exact::QuickSearch::match );
auto QuickSearchPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister < arbology::exact::QuickSearch, ext::set < unsigned >, const tree::PrefixRankedBarTree < > &, const tree::PrefixRankedBarNonlinearPattern < > & > ( arbology::exact::QuickSearch::match );

} /* namespace */
