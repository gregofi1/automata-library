#include <catch2/catch.hpp>

#include <automaton/FSM/EpsilonNFA.h>

#include "automaton/properties/EpsilonClosure.h"
#include "automaton/properties/AllEpsilonClosure.h"

TEST_CASE ( "Epsilon Closure", "[unit][algo][automaton]" ) {
	SECTION ( "Test closure" ) {
		std::string q0 = std::string ("q0");
		std::string q1 = std::string ("q1");
		std::string q2 = std::string ("q2");
		std::string q3 = std::string ("q3");
		std::string q4 = std::string ("q4");
		std::string q5 = std::string ("q5");
		std::string q6 = std::string ("q6");
		std::string q7 = std::string ("q7");
		std::string q8 = std::string ("q8");
		std::string q9 = std::string ("q9");

		automaton::EpsilonNFA < int, std::string > automaton(q0);
		automaton.setStates({q0, q1, q2, q3, q4, q5, q6, q7, q8, q9});

		automaton.addTransition(q0, q1);
		automaton.addTransition(q1, q2);
		automaton.addTransition(q2, q3);
		automaton.addTransition(q3, q4);
		automaton.addTransition(q4, q1);
		automaton.addTransition(q5, q6);
		automaton.addTransition(q6, q1);
		automaton.addTransition(q8, q8);

		ext::map<std::string, ext::set<std::string>> allClosures = automaton::properties::AllEpsilonClosure::allEpsilonClosure(automaton);

		for(const std::string& state : automaton.getStates()) {
			CHECK(automaton::properties::EpsilonClosure::epsilonClosure(automaton, state) == allClosures[state]);
		}
	}
}
