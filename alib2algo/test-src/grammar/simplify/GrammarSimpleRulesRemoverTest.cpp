#include <catch2/catch.hpp>

#include "grammar/simplify/SimpleRulesRemover.h"

#include "grammar/ContextFree/CFG.h"
#include "grammar/ContextFree/EpsilonFreeCFG.h"

TEST_CASE ( "Simple rules remover", "[unit][algo][grammar][simplify]" ) {
	SECTION ( "Test CFG" ) {
		{
			std::string S = std::string("S");
			std::string A = std::string("A");
			std::string B = std::string("B");
			std::string C = std::string("C");
			std::string D = std::string("D");

			char a = 'a';
			char b = 'b';

			grammar::CFG < char, std::string > grammar1(S);
			grammar1.setNonterminalAlphabet({S, A, B, C, D});
			grammar1.setTerminalAlphabet({a, b});

			grammar::CFG < char, std::string > grammar2 = grammar::simplify::SimpleRulesRemover::remove(grammar1);

			grammar::CFG < char, std::string > grammar3(S);
			grammar3.setNonterminalAlphabet({S, A, B, C, D});
			grammar3.setTerminalAlphabet({a, b});

			CHECK(grammar2 == grammar3);
		}
		{
			std::string S = std::string("S");
			std::string A = std::string("A");
			std::string B = std::string("B");
			std::string C = std::string("C");
			std::string D = std::string("D");

			char a = 'a';
			char b = 'b';
			char c = 'c';
			char d = 'd';

			grammar::CFG < char, std::string > grammar1(S);
			grammar1.setNonterminalAlphabet({S, A, B, C, D});
			grammar1.setTerminalAlphabet({a, b, c, d});
			grammar1.addRule(S, {A});
			grammar1.addRule(A, {a, A, d, D});
			grammar1.addRule(A, {B});
			grammar1.addRule(B, {b, B, c, C});
			grammar1.addRule(B, {});
			grammar1.addRule(C, {c, C});
			grammar1.addRule(C, {});
			grammar1.addRule(D, {d, D});
			grammar1.addRule(D, {});

			grammar::CFG < char, std::string > grammar2 = grammar::simplify::SimpleRulesRemover::remove(grammar1);

			grammar::CFG < char, std::string > grammar3(S);
			grammar3.setNonterminalAlphabet({S, A, B, C, D});
			grammar3.setTerminalAlphabet({a, b, c, d});
			grammar3.addRule(S, {a, A, d, D});
			grammar3.addRule(S, {b, B, c, C});
			grammar3.addRule(S, {});
			grammar3.addRule(A, {a, A, d, D});
			grammar3.addRule(A, {b, B, c, C});
			grammar3.addRule(A, {});
			grammar3.addRule(B, {b, B, c, C});
			grammar3.addRule(B, {});
			grammar3.addRule(C, {c, C});
			grammar3.addRule(C, {});
			grammar3.addRule(D, {d, D});
			grammar3.addRule(D, {});

			CHECK(grammar2 == grammar3);
		}
		{
			std::string S = std::string("S");
			std::string A = std::string("A");
			std::string B = std::string("B");
			std::string C = std::string("C");
			std::string D = std::string("D");

			char a = 'a';
			char b = 'b';
			char c = 'c';
			char d = 'd';

			grammar::EpsilonFreeCFG < char, std::string > grammar1(S);
			grammar1.setNonterminalAlphabet({S, A, B, C, D});
			grammar1.setTerminalAlphabet({a, b, c, d});
			grammar1.addRule(S, {A});
			grammar1.addRule(A, {a, A, d, D});
			grammar1.addRule(A, {B});
			grammar1.addRule(B, {b, B, c, C});
			grammar1.addRule(B, {c});
			grammar1.addRule(C, {c, C});
			grammar1.addRule(C, {b});
			grammar1.addRule(D, {d, D});
			grammar1.addRule(D, {a});

			grammar::EpsilonFreeCFG < char, std::string > grammar2 = grammar::simplify::SimpleRulesRemover::remove(grammar1);

			grammar::EpsilonFreeCFG < char, std::string > grammar3(S);
			grammar3.setNonterminalAlphabet({S, A, B, C, D});
			grammar3.setTerminalAlphabet({a, b, c, d});
			grammar3.addRule(S, {a, A, d, D});
			grammar3.addRule(S, {b, B, c, C});
			grammar3.addRule(S, {c});
			grammar3.addRule(A, {a, A, d, D});
			grammar3.addRule(A, {b, B, c, C});
			grammar3.addRule(A, {c});
			grammar3.addRule(B, {b, B, c, C});
			grammar3.addRule(B, {c});
			grammar3.addRule(C, {c, C});
			grammar3.addRule(C, {b});
			grammar3.addRule(D, {d, D});
			grammar3.addRule(D, {a});

			CHECK(grammar2 == grammar3);
		}
	}
}
