#pragma once
#include <QDialog>

#include <abstraction/Value.hpp>

#include <ui_InputDialog.h>

class InputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputDialog(std::shared_ptr<abstraction::Value>);

    std::shared_ptr<abstraction::Value> getAutomaton();

private slots:
    void on_generateRandomAutomatonButton_clicked();
    void on_generateRandomGrammarButton_clicked();
    void on_okCancelButtonBox_accepted();
    void on_okCancelButtonBox_rejected();
    void on_plainTextEdit_xml_textChanged();
    void on_plainTextEdit_text_textChanged();
    void on_openFileButton_clicked();

private:
    void setAutomaton(std::shared_ptr<abstraction::Value> p_automaton, bool updateText = true );

    void setTabShown(QWidget* tab, bool value);


    std::unique_ptr<Ui::InputDialog> ui;
    std::shared_ptr<abstraction::Value> automaton;
};


