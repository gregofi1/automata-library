#pragma once
#include <set>

#include <Graphics/Connection/ConnectionBox.hpp>

class OutputConnectionBox : public ConnectionBox {
    friend class Connection;

public:
    explicit OutputConnectionBox(GraphicsBox* parent);

    void addConnection(Connection* connection);
    const std::set<Connection*>& getConnections() const { return this->connections; }

private:
    void on_Disconnect() override;

protected:
    std::set<Connection*> connections;
};


