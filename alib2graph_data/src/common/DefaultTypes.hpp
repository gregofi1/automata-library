// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include "default_type/DefaultCoordinateType.hpp"
#include "default_type/DefaultEdgeType.hpp"
#include "default_type/DefaultNodeType.hpp"
#include "default_type/DefaultSquareGridEdgeType.hpp"
#include "default_type/DefaultSquareGridNodeType.hpp"
#include "default_type/DefaultSquareGridWeightedEdgeType.hpp"
#include "default_type/DefaultWeightedEdgeType.hpp"
#include "default_type/DefaultWeightType.hpp"
#include "default_type/DefaultCapacityType.hpp"
#include "default_type/DefaultCapacityEdgeType.hpp"

