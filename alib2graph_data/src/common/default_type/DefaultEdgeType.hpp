#pragma once

#include <alib/pair>
#include <object/Object.h>
#include "DefaultNodeType.hpp"

typedef ext::pair<DefaultNodeType, DefaultNodeType> DefaultEdgeType;

