// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include "DefaultCoordinateType.hpp"

typedef ext::pair<DefaultCoordinateType, DefaultCoordinateType> DefaultSquareGridNodeType;

