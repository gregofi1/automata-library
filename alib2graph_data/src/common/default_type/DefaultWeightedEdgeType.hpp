// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultWeightType.hpp>
#include <edge/weighted/WeightedEdge.hpp>

typedef edge::WeightedEdge<DefaultNodeType, DefaultWeightType> DefaultWeightedEdgeType;

