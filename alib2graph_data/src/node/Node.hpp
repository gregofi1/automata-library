// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <ostream>
#include <object/Object.h>

#include "NodeBase.hpp"

namespace node {

class Node : public NodeBase {
// ---------------------------------------------------------------------------------------------------------------------

 private:
  int m_id;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit Node();
  explicit Node(int id);

// ---------------------------------------------------------------------------------------------------------------------

  auto operator <=> ( const Node &rhs) const { return m_id <=> rhs.m_id; }
  bool operator ==( const Node &rhs) const { return m_id == rhs.m_id; }

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// NodeBase interface

 public:
  void operator>>(ext::ostream &ostream) const override;

// =====================================================================================================================
 public:

  std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------

  int getId() const;

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

} // namespace node

// =====================================================================================================================

namespace std {

template<>
struct hash<node::Node> {
  std::size_t operator()(const node::Node &k) const {
    return hash<int>()(k.getId());
  }
};
}

// =====================================================================================================================

