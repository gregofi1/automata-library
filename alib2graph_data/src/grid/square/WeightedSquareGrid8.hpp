// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/tuple>
#include <edge/weighted/WeightedEdge.hpp>

#include "SquareGrid.hpp"

namespace grid {

template<typename TCoordinate, typename TEdge>
class WeightedSquareGrid8 final : public SquareGrid<TCoordinate, TEdge> {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  using coordinate_type = TCoordinate;
  using edge_type = TEdge;
  using node_type = ext::pair<TCoordinate, TCoordinate>;
  using direction_type = SquareGridDirections;

// ---------------------------------------------------------------------------------------------------------------------
 protected:
  typename edge_type::weight_type m_unit;

// =====================================================================================================================
// Constructor, Destructor, Operators
 public:
  explicit WeightedSquareGrid8(TCoordinate height, TCoordinate width, typename edge_type::weight_type unit = 1);

// ---------------------------------------------------------------------------------------------------------------------

  typename edge_type::weight_type getUnit() const;

  void setUnit(typename edge_type::weight_type unit);

// ---------------------------------------------------------------------------------------------------------------------

// =====================================================================================================================
// GridBase interface
 public:
	auto operator <=> (const WeightedSquareGrid8 &other) const {
		return std::tie(this->m_obstacles, this->m_height, this->m_width) <=> std::tie(other.getObstacleList(), other.getHeight(), other.getWidth());
	}

	bool operator == (const WeightedSquareGrid8 &other) const {
		return std::tie(this->m_obstacles, this->m_height, this->m_width) == std::tie(other.getObstacleList(), other.getHeight(), other.getWidth());
	}


// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// GridInterface interface

 public:

  bool isValidDirection(direction_type direction) const override;

// ---------------------------------------------------------------------------------------------------------------------

 protected:
  TEdge createEdge(const node_type &a, const node_type &b) const override;

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// GraphInterface interface

 public:
  std::string name() const override;

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

template<typename TCoordinate, typename TEdge>
WeightedSquareGrid8<TCoordinate, TEdge>::WeightedSquareGrid8(TCoordinate height,
                                                             TCoordinate width,
                                                             typename TEdge::weight_type unit)
    : SquareGrid<TCoordinate, TEdge>(height, width), m_unit(unit) {
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
typename TEdge::weight_type WeightedSquareGrid8<TCoordinate, TEdge>::getUnit() const {
  return m_unit;
}

template<typename TCoordinate, typename TEdge>
void WeightedSquareGrid8<TCoordinate, TEdge>::setUnit(typename TEdge::weight_type unit) {
  if (unit > 0) m_unit = unit;
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
TEdge WeightedSquareGrid8<TCoordinate, TEdge>::createEdge(const WeightedSquareGrid8::node_type &a,
                                                          const WeightedSquareGrid8::node_type &b) const {
  if (GridFunction::sqaureGridDirectionIsDiagonal(GridFunction::squareGridDirection(a, b))) {
    return TEdge(a, b, M_SQRT2 * m_unit);
  }

  return TEdge(a, b, m_unit);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
bool WeightedSquareGrid8<TCoordinate, TEdge>::isValidDirection(WeightedSquareGrid8::direction_type direction) const {
  return SQUARE_GRID_DIRECTIONS.find(direction) != SQUARE_GRID_DIRECTIONS.end();
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
std::string WeightedSquareGrid8<TCoordinate, TEdge>::name() const {
  return "WeightedSquareGrid8";
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace grid

// =====================================================================================================================

namespace core {

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */

template<typename TCoordinate, typename TEdge>
struct normalize < grid::WeightedSquareGrid8 < TCoordinate, TEdge > > {
  static grid::WeightedSquareGrid8<> eval(grid::WeightedSquareGrid8<TCoordinate, TEdge> &&value) {
    grid::WeightedSquareGrid8<> grid(value.getHeight(), value.getWidth(), value.getUnit());

    // Copy obstacles
    for (auto &&i: ext::make_mover(std::move(value).getObstacleList())) {
      DefaultSquareGridNodeType
          obstacle = graph::GraphNormalize::normalizeObstacle(std::move(i));

      grid.addObstacle(std::move(obstacle));
    }

    return grid;
  }
};

} // namespace grid

// =====================================================================================================================

