// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

namespace grid {

/**
 * Represents Grid.
 */
class GridBase {
public:
	virtual ~GridBase ( ) noexcept = default;

// ---------------------------------------------------------------------------------------------------------------------

 public:
// ---------------------------------------------------------------------------------------------------------------------
	friend ext::ostream & operator << ( ext::ostream & os, const GridBase & instance ) {
		instance >> os;
		return os;
	}

	virtual void operator >> ( ext::ostream & os ) const = 0;
};

} // namespace grid

