// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <common/DefaultTypes.hpp>

namespace grid {

class GridBase;

template<typename TCoordinate, typename TEdge>
class SqaureGrid;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridEdgeType>
class SquareGrid4;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridEdgeType>
class SquareGrid8;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridWeightedEdgeType>
class WeightedSquareGrid4;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridWeightedEdgeType>
class WeightedSquareGrid8;

} // namespace grid

