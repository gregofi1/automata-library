// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

namespace edge {

/**
 * Represents edge in graph.
 */
class EdgeBase {
public:
	virtual ~EdgeBase ( ) noexcept = default;

// ---------------------------------------------------------------------------------------------------------------------

 public:
// ---------------------------------------------------------------------------------------------------------------------
	friend ext::ostream & operator << ( ext::ostream & os, const EdgeBase & instance ) {
		instance >> os;
		return os;
	}

	virtual void operator >> ( ext::ostream & os ) const = 0;
};

} // namespace edge

