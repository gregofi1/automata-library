// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include "Edge.hpp"
#include "weighted/WeightedEdge.hpp"
#include "capacity/CapacityEdge.hpp"

