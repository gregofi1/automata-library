// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.


#include "WeightedEdge.hpp"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<edge::WeightedEdge<> >();

}
