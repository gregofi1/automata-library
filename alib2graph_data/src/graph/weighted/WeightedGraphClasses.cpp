// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "WeightedGraphClasses.hpp"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter1 = registration::ValuePrinterRegister<graph::WeightedUndirectedGraph<> >();
auto valuePrinter2 = registration::ValuePrinterRegister<graph::WeightedUndirectedMultiGraph<> >();
auto valuePrinter3 = registration::ValuePrinterRegister<graph::WeightedDirectedGraph<> >();
auto valuePrinter4 = registration::ValuePrinterRegister<graph::WeightedMixedGraph<> >();
auto valuePrinter5 = registration::ValuePrinterRegister<graph::WeightedMixedMultiGraph<> >();

}
