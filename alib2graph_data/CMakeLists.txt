project(alt-libgraph_data VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
alt_library(alib2graph_data
	DEPENDS alib2xml alib2common alib2abstraction alib2measure alib2std
	TEST_DEPENDS LibXml2::LibXml2
)
