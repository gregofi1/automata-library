#include "EpsilonRemoverOutgoing.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto EpsilonRemoverOutgoingDFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverOutgoing, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::efficient::EpsilonRemoverOutgoing::remove );
auto EpsilonRemoverOutgoingMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::efficient::EpsilonRemoverOutgoing::remove );
auto EpsilonRemoverOutgoingNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverOutgoing, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::efficient::EpsilonRemoverOutgoing::remove );
auto EpsilonRemoverOutgoingEpsilonNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::efficient::EpsilonRemoverOutgoing::remove );

} /* namespace */
