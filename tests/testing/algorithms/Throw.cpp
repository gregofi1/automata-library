/*
 * Throw.h
 */

#include "Throw.h"
#include <registration/AlgoRegistration.hpp>

namespace debug {

int debug::Throw::throwme ( ) {
	throw 10; // NOLINT(readability-magic-numbers) // just some random number
}

} /* namespace debug */

namespace {

auto ThrowInt = registration::AbstractRegister < debug::Throw, int > ( debug::Throw::throwme );

} /* namespace */
