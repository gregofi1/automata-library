/*
 * UndefinedBehaviour.h
 */

#include "UndefinedBehaviour.h"
#include <registration/AlgoRegistration.hpp>

namespace debug {

int UndefinedBehaviour::undefined_behaviour ( ) {
	int a = 5; // NOLINT(readability-magic-numbers) // just some random number
	int * b = reinterpret_cast < int * > ( reinterpret_cast < size_t > ( & a ) + 1 ); //NOLINT(performance-no-int-to-ptr) // well we just test undefined behavior detection

	return * b;
}

} /* namespace debug */

namespace {

auto UndefinedBehaviourInt = registration::AbstractRegister < debug::UndefinedBehaviour, int > ( debug::UndefinedBehaviour::undefined_behaviour );

} /* namespace */
