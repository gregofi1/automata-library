#include <catch2/catch.hpp>
#include <alib/vector>
#include <sstream>

#include "testing/TimeoutAqlTest.hpp"
#include "testing/TestFiles.hpp"

const size_t SIZE = 100;
const size_t HEIGHT = 15;
const size_t ALPHABET_SIZE = 10;
const size_t RANDOM_ITERATIONS = 15;


TEST_CASE ( "TreeNotation tests | Files", "[integration]" ) {
	auto casts = GENERATE ( as < std::string > ( ), "(PostfixRankedTree)" );

	SECTION ( "Test files" ) {
		for ( const std::string & file : TestFiles::Get ( "/tree/repeats.*.xml" ) ) {
			ext::vector < std::string > qs = {
				"execute < " + file + " > $res1",
				"execute (RankedTree)" + casts + "$res1 > $res2",
				"quit compare::TreeCompare $res1 $res2",
			};

			TimeoutAqlTest ( 1s, qs );
		}
	}

	SECTION ( "Random tests" ) {
		std::ostringstream oss;
		oss << "execute tree::generate::RandomRankedTreeFactory ";
		oss << HEIGHT << " " << SIZE << " " << ( rand ( ) % ALPHABET_SIZE + 1 );
		oss << " (bool)true 5 > $res1";

		for ( size_t i = 0; i < RANDOM_ITERATIONS; i++ ) {
			ext::vector < std::string > qs = {
				oss.str ( ),
				"execute (RankedTree)" + casts + "$res1 > $res2",
				"quit compare::TreeCompare $res1 $res2",
			};

			TimeoutAqlTest ( 2s, qs );
		}
	}
}
