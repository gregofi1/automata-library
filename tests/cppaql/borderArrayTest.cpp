#include <catch2/catch.hpp>
#include "testing/TimeoutAqlTest.hpp"

#include <tree/properties/BorderArrayNaive.h>
#include <tree/properties/BorderArray.h>

const size_t PATTERN_SIZE = 50;
const size_t PATTERN_HEIGHT = 7;

const size_t ALPHABET_SIZE = 3;
const size_t RANDOM_ITERATIONS = 100;

const size_t NODE_WILDCARD_PROBABILITY = 15;

std::string gen ( const std::string & type, const std::string & var ) {
	std::ostringstream oss;

	if ( type == "PrefixRankedExtendedPattern" ) {
		oss << "execute ( " << type << " ) tree::generate::RandomRankedExtendedPatternFactory ";
	} else {
		oss << "execute ( " << type << " ) tree::generate::RandomRankedPatternFactory ";
	}
	oss << "(int) " << PATTERN_HEIGHT << " ";
	oss << "(int) " << PATTERN_SIZE   << " ";
	oss << "(int) " << ALPHABET_SIZE  << " ";
	oss << "(bool) false ";
	oss << "(int) 5"; // rank
	if ( type == "PrefixRankedExtendedPattern" ) {
		oss << " (double)" << NODE_WILDCARD_PROBABILITY;
	}
	oss << " > $" << var;

	return oss.str ( );
}

void run ( const std::string & prefixRankedType ) {
	ext::vector < std::string > qs = {
		gen ( prefixRankedType, "pattern" ),
		"execute tree::properties::BorderArrayNaive $pattern > $res1",
		"execute tree::properties::BorderArray      $pattern > $res2",
		// "execute $pattern | string::Compose - ",
		// "execute $res1",
		// "execute $res2",
		"quit compare::VectorCompare $res1 $res2",
	};
	for ( size_t i = 0; i < RANDOM_ITERATIONS; i++ )
		TimeoutAqlTest ( 1s, qs );
}

TEST_CASE ( "Tree BorderArray", "[integration]" ) {
	SECTION ( "PrefixRankedPattern" ) {
		run ( "PrefixRankedPattern" );
	}
	SECTION ( "PrefixRankedBarPattern" ) {
		run ( "PrefixRankedBarPattern" );
	}
	SECTION ( "PrefixRankedNonlinearPattern" ) {
		run ( "PrefixRankedNonlinearPattern" );
	}
	SECTION ( "PrefixRankedBarNonlinearPattern" ) {
		run ( "PrefixRankedBarNonlinearPattern" );
	}
	SECTION ( "PrefixRankedExtendedPattern" ) {
		run ( "PrefixRankedExtendedPattern" );
	}
}
