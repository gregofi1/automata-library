#pragma once
#include "DataType.h"

namespace example {

class CreateDataType {
public:
	static DataType create ( );
};

class CreateDataType2 {
public:
	template < typename T >
	static DataType create ( T a ) {
		return DataType ( a );
	}

	static DataType create ( int a );
};

} /* namespace example */

