#include "DataType.h"
#include <primitive/xml/Integer.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

example::DataType xmlApi < example::DataType >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	int value = xmlApi < int >::parse ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return example::DataType ( value );
}

bool xmlApi < example::DataType >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < example::DataType >::xmlTagName ( ) {
	return "DataType";
}

void xmlApi < example::DataType >::compose ( ext::deque < sax::Token > & output, const example::DataType & data ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT);
	xmlApi < int >::compose ( output, data.getA ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

// registration of XML parser and writer.
auto xmlWrite = registration::XmlWriterRegister < example::DataType > ( );
auto xmlRead = registration::XmlReaderRegister < example::DataType > ( );

} /* namespace */
