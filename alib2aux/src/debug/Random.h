#pragma once

#include <ext/random>

namespace debug {

template < class T >
class Random {
public:
	static T random ( );
};

template < class T >
T Random < T >::random ( ) {
	return ext::random_devices::semirandom ( );
}

} /* namespace debug */

