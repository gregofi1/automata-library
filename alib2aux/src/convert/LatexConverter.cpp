#include "LatexConverter.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DFA = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::DFA < > & > ( convert::LatexConverter::convertFSM, "automaton" ).setDocumentation ( "Converts a DFA table representation to Latex." );
auto NFA = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::NFA < > & > ( convert::LatexConverter::convertFSM ).setDocumentation ( "Converts a NFA table representation to Latex." );
auto ENFA = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::EpsilonNFA < > & > ( convert::LatexConverter::convertFSM ).setDocumentation ( "Converts a EpsilonNFA table representation to Latex." );
auto MISNFA = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::MultiInitialStateNFA < > & > ( convert::LatexConverter::convertFSM ).setDocumentation ( "Converts a MultiInitialStateNFA table representation to Latex." );
auto MISENFA = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::MultiInitialStateEpsilonNFA < > & > ( convert::LatexConverter::convertFSM ).setDocumentation ( "Converts a MultiInitialStateEpsilonNFA table representation to Latex." );

auto DFAw = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::DFA < > &, bool > ( convert::LatexConverter::convertFSM, "automaton", "wide" ).setDocumentation ( "Converts a DFA table representation to Latex. If 'wide' is true, create an empty column and make it as wide as the page. This is useful for instance for test generation of minimization algorithm." );
auto NFAw = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::NFA < > &, bool > ( convert::LatexConverter::convertFSM, "automaton", "wide" ).setDocumentation ( "Converts a DFA table representation to Latex. If 'wide' is true, create an empty column and make it as wide as the page. This is useful for instance for test generation of minimization algorithm." );
auto ENFAw = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::EpsilonNFA < > &, bool > ( convert::LatexConverter::convertFSM, "automaton", "wide" ).setDocumentation ( "Converts a DFA table representation to Latex. If 'wide' is true, create an empty column and make it as wide as the page. This is useful for instance for test generation of minimization algorithm." );
auto MISNFAw = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::MultiInitialStateNFA < > &, bool > ( convert::LatexConverter::convertFSM, "automaton", "wide" ).setDocumentation ( "Converts a DFA table representation to Latex. If 'wide' is true, create an empty column and make it as wide as the page. This is useful for instance for test generation of minimization algorithm." );
auto MISENFAw = registration::AbstractRegister < convert::LatexConverter, std::string, const automaton::MultiInitialStateEpsilonNFA < > &, bool > ( convert::LatexConverter::convertFSM, "automaton", "wide" ).setDocumentation ( "Converts a DFA table representation to Latex. If 'wide' is true, create an empty column and make it as wide as the page. This is useful for instance for test generation of minimization algorithm." );

auto CFG = registration::AbstractRegister < convert::LatexConverter, std::string, const grammar::CFG < > & > ( convert::LatexConverter::convertGrammar, "grammar" ).setDocumentation ( "Converts CFG grammar to Latex representation." );
auto RRG = registration::AbstractRegister < convert::LatexConverter, std::string, const grammar::RightRG < > & > ( convert::LatexConverter::convertGrammar, "grammar" ).setDocumentation ( "Converts (right) regular grammar to Latex representation." );


} /* namespace */
