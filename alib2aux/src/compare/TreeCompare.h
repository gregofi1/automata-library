#pragma once

#include <tree/ranked/RankedTree.h>

namespace compare {

class TreeCompare {
public:
	template < class SymbolType >
	static bool compare ( const tree::RankedTree < SymbolType > & a, const tree::RankedTree < SymbolType > & b ) {
		return a == b;
	}
};

} /* namespace compare */

