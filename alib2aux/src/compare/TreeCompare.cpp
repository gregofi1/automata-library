#include "TreeCompare.h"
#include <registration/AlgoRegistration.hpp>
#include <common/DefaultSymbolType.h>

namespace {

auto TreeCompareRankedTree = registration::AbstractRegister < compare::TreeCompare, bool, const tree::RankedTree < DefaultSymbolType> &, const tree::RankedTree < DefaultSymbolType > & > ( compare::TreeCompare::compare );

} /* namespace */
