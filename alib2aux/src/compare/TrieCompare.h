#pragma once

#include <alib/trie>
#include <common/DefaultSymbolType.h>

namespace compare {

class TrieCompare {
public:
	template < class R, class T >
	static bool compare ( const ext::trie < R, T > & a, const ext::trie < R, T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

