#include "VectorCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto VectorCompareULI = registration::AbstractRegister < compare::VectorCompare, bool, const ext::vector < unsigned long > &, const ext::vector < unsigned long > & > ( compare::VectorCompare::compare );

} /* namespace */
