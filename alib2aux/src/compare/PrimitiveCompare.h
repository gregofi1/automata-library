#pragma once

#include <alib/set>

namespace compare {

class PrimitiveCompare {
public:
	template < class T >
	static bool compare ( const T & a, const T & b ) {
		return a == b;
	}
};

} /* namespace compare */

