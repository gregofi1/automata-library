#include "GrammarCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GrammarCompareLeftLG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::LeftLG < > &, const grammar::LeftLG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareLeftRG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::LeftRG < > &, const grammar::LeftRG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareRightLG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::RightLG < > &, const grammar::RightLG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareRightRG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::RightRG < > &, const grammar::RightRG < > & > ( compare::GrammarCompare::compare );

auto GrammarCompareLG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::LG < > &, const grammar::LG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareCFG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::CFG < > &, const grammar::CFG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareEpsilonFreeCFG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::EpsilonFreeCFG < > &, const grammar::EpsilonFreeCFG < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareCNF = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::CNF < > &, const grammar::CNF < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareGNF = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::GNF < > &, const grammar::GNF < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareCSG = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::CSG < > &, const grammar::CSG < > & > ( compare::GrammarCompare::compare );

auto GrammarCompareNonContractingGrammar = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::NonContractingGrammar < > &, const grammar::NonContractingGrammar < > & > ( compare::GrammarCompare::compare );
auto GrammarCompareContextPreservingUnrestrictedGrammar = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::ContextPreservingUnrestrictedGrammar < > &, const grammar::ContextPreservingUnrestrictedGrammar < > & > ( compare::GrammarCompare::compare );

auto GrammarCompareUnrestrictedGrammar = registration::AbstractRegister < compare::GrammarCompare, bool, const grammar::UnrestrictedGrammar < > &, const grammar::UnrestrictedGrammar < > & > ( compare::GrammarCompare::compare );

} /* namespace */
