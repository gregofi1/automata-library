#include "AutomatonCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomatonCompareDFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::DFA < > &, const automaton::DFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareNFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::NFA < > &, const automaton::NFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareMultiInitialStateNFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::MultiInitialStateNFA < > &, const automaton::MultiInitialStateNFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareEpsilonNFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::EpsilonNFA < > &, const automaton::EpsilonNFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareExtendedNFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::ExtendedNFA < > &, const automaton::ExtendedNFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareCompactDFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::CompactDFA < > &, const automaton::CompactDFA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareCompactNFA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::CompactNFA < > &, const automaton::CompactNFA < > & > ( compare::AutomatonCompare::compare );

auto AutomatonCompareDFTA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::DFTA < > &, const automaton::DFTA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareNFTA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::NFTA < > &, const automaton::NFTA < > & > ( compare::AutomatonCompare::compare );

auto AutomatonCompareDPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::DPDA < > &, const automaton::DPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareNPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::NPDA < > &, const automaton::NPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareInputDrivenDPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::InputDrivenDPDA < > &, const automaton::InputDrivenDPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareInputDrivenNPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::InputDrivenNPDA < > &, const automaton::InputDrivenNPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareVisiblyPushdownDPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::VisiblyPushdownDPDA < > &, const automaton::VisiblyPushdownDPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareVisiblyPushdownNPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::VisiblyPushdownNPDA < > &, const automaton::VisiblyPushdownNPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareRealTimeHeightDeterministicDPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::RealTimeHeightDeterministicDPDA < > &, const automaton::RealTimeHeightDeterministicDPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareRealTimeHeightDeterministicNPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::RealTimeHeightDeterministicNPDA < > &, const automaton::RealTimeHeightDeterministicNPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareSinglePopDPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::SinglePopDPDA < > &, const automaton::SinglePopDPDA < > & > ( compare::AutomatonCompare::compare );
auto AutomatonCompareSinglePopNPDA = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::SinglePopNPDA < > &, const automaton::SinglePopNPDA < > & > ( compare::AutomatonCompare::compare );

auto AutomatonCompareOneTapeDTM = registration::AbstractRegister < compare::AutomatonCompare, bool, const automaton::OneTapeDTM < > &, const automaton::OneTapeDTM < > & > ( compare::AutomatonCompare::compare );

} /* namespace */
