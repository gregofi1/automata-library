#pragma once

#include <alib/vector>

namespace compare {

class VectorCompare {
public:
	template < class T >
	static bool compare ( const ext::vector < T > & a, const ext::vector < T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

