#pragma once

#include <compare/DiffAux.h>
#include <compare/GrammarCompare.h>

#include <ostream>

#include <ext/utility>
#include <ext/typeinfo>
#include <ext/iostream>
#include <ext/algorithm>

#include <alib/set>
#include <alib/map>
#include <alib/list>
#include <alib/vector>

#include "grammar/Regular/LeftLG.h"
#include "grammar/Regular/LeftRG.h"
#include "grammar/Regular/RightLG.h"
#include "grammar/Regular/RightRG.h"
#include "grammar/ContextFree/LG.h"
#include "grammar/ContextFree/CFG.h"
#include "grammar/ContextFree/EpsilonFreeCFG.h"
#include "grammar/ContextFree/CNF.h"
#include "grammar/ContextFree/GNF.h"
#include "grammar/ContextSensitive/CSG.h"
#include "grammar/ContextSensitive/NonContractingGrammar.h"
#include "grammar/Unrestricted/ContextPreservingUnrestrictedGrammar.h"
#include "grammar/Unrestricted/UnrestrictedGrammar.h"

namespace compare {

class GrammarDiff {
private:
	template < class SymbolType >
	static void printDiff(const grammar::LeftLG < SymbolType > & a, const grammar::LeftLG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::LeftRG < SymbolType > & a, const grammar::LeftRG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::RightLG < SymbolType > & a, const grammar::RightLG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::RightRG < SymbolType > & a, const grammar::RightRG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::LG < SymbolType > & a, const grammar::LG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::CFG < SymbolType > & a, const grammar::CFG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::EpsilonFreeCFG < SymbolType > & a, const grammar::EpsilonFreeCFG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::CNF < SymbolType > & a, const grammar::CNF < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::GNF < SymbolType > & a, const grammar::GNF < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::CSG < SymbolType > & a, const grammar::CSG < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::NonContractingGrammar < SymbolType > & a, const grammar::NonContractingGrammar < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & a, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & b, ext::ostream & out );

	template < class SymbolType >
	static void printDiff(const grammar::UnrestrictedGrammar < SymbolType > & a, const grammar::UnrestrictedGrammar < SymbolType > & b, ext::ostream & out );
public:
	template<class T>
	static void diff(const T & a, const T & b, ext::ostream & out );

	template < class T >
	static std::string diff ( const T & a, const T & b );
};

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::LeftLG < SymbolType > & a, const grammar::LeftLG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::LeftRG < SymbolType > & a, const grammar::LeftRG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::RightLG < SymbolType > & a, const grammar::RightLG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::RightRG < SymbolType > & a, const grammar::RightRG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::LG < SymbolType > & a, const grammar::LG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::CFG < SymbolType > & a, const grammar::CFG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::EpsilonFreeCFG < SymbolType > & a, const grammar::EpsilonFreeCFG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::CNF < SymbolType > & a, const grammar::CNF < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::GNF < SymbolType > & a, const grammar::GNF < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::CSG < SymbolType > & a, const grammar::CSG < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::NonContractingGrammar < SymbolType > & a, const grammar::NonContractingGrammar < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & a, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class SymbolType >
void GrammarDiff::printDiff(const grammar::UnrestrictedGrammar < SymbolType > & a, const grammar::UnrestrictedGrammar < SymbolType > & b, ext::ostream & out ) {
	out << "GrammarsComparer" << std::endl;

	if(a.getNonterminalAlphabet() != b.getNonterminalAlphabet()) {
		out << "Nonterminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getNonterminalAlphabet(), b.getNonterminalAlphabet());
	}

	if(a.getRules() != b.getRules()){
		out << "Rules" << std::endl;

		DiffAux::mapDiff ( out, a.getRules(), b.getRules());
	}

	if(a.getInitialSymbol() != b.getInitialSymbol()){
		out << "Initial symbol" << std::endl;

		out << "< " << a.getInitialSymbol() << std::endl;
		out << "---" << std::endl;
		out << "> " << b.getInitialSymbol() << std::endl;
	}

	if(a.getTerminalAlphabet() != b.getTerminalAlphabet()) {
		out << "Terminal alphabet" << std::endl;

		DiffAux::setDiff ( out, a.getTerminalAlphabet(), b.getTerminalAlphabet());
	}
}

template < class T >
void GrammarDiff::diff(const T & a, const T & b, ext::ostream & out ) {
	if(!GrammarCompare::compare(a, b)) {
	  GrammarDiff::printDiff ( a, b, out );
	}
}

template < class T >
std::string GrammarDiff::diff ( const T & a, const T & b ) {
	ext::ostringstream ss;
	diff ( a, b, ss );
	return ss.str ( );
}

} /* namespace compare */

