#include "StringDiff.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto StringDiffLinear = registration::AbstractRegister < compare::StringDiff, std::string, const string::LinearString < > &, const string::LinearString < > & > ( compare::StringDiff::diff );
auto StringDiffCyclic = registration::AbstractRegister < compare::StringDiff, std::string, const string::CyclicString < > &, const string::CyclicString < > & > ( compare::StringDiff::diff );

} /* namespace */
