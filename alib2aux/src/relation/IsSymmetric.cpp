#include "IsSymmetric.h"
#include <registration/AlgoRegistration.hpp>

#include <object/Object.h>

namespace {

auto IsSymmetric = registration::AbstractRegister < relation::IsSymmetric, bool, const ext::set < ext::pair < object::Object, object::Object > > & > ( relation::IsSymmetric::isSymmetric, "relation" ).setDocumentation (
"Checks whether a relation is symmetric.\n\
\n\
@param relation the tested relation\n\
\n\
@return true if the relation is symmetric, false otherwise");

} /* namespace */
