/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/set>
#include <alib/pair>

namespace relation {

/**
 * Computes the complement relation in given universe.
 */
class RelationComplement {
public:
	/**
	 * Computes the complement relation in given universe
	 *
	 * @tparam T Type of the items in relation.
	 *
	 * @param relation the original relation
	 * @param universe the universe of items participating in the relation
	 *
	 * @return relation where items in relation are those which that were not in relation and otherwise
	 */
	template < class T >
	static ext::set < ext::pair < T, T > > complement ( const ext::set < ext::pair < T, T > > & relation, const ext::set < T > & universe );
};

template < class T >
ext::set < ext::pair < T, T > > RelationComplement::complement ( const ext::set < ext::pair < T, T > > & relation, const ext::set < T > & universe ) {
	ext::set < ext::pair < T, T > > res;

	for ( const T & state : universe )
		for ( const T & other : universe )
			if ( ! relation.contains ( ext::make_pair ( state, other ) ) )
				res.insert ( ext::make_pair ( state, other ) );

	return res;
}

} /* namespace relation */

