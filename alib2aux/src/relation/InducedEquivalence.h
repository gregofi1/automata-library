/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/set>
#include <alib/map>
#include <alib/pair>

namespace relation {

/**
 * Partition inducedEquivalence states in automata
 */
class InducedEquivalence {
public:
	/**
	 * Creates a set of equivalent objects from a symetric relation.
	 *
	 * @tparam T Type of objects.
	 * @param  automaton which states are to be partitioned.
	 *
	 * @return induced equivalence set.
	 */
	template < class T >
	static ext::set < ext::set < T > > inducedEquivalence ( const ext::set < ext::pair < T, T > > & relation );
};

template < class T >
ext::set < ext::set < T > > InducedEquivalence::inducedEquivalence ( const ext::set < ext::pair < T, T > > & relation ) {
	ext::map < T, ext::set < T > > transform;

	for ( const ext::pair < T, T > & item : relation ) {
		transform [ item.first ].insert ( item.second );
	}

	ext::set < ext::set < T > > res;

	for ( const std::pair < const T, ext::set < T > > & item : transform ) {
		res.insert ( item.second );
	}

	return res;
}

} /* namespace relation */

