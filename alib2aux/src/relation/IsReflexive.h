/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/set>
#include <alib/pair>

namespace relation {

/**
 * Computes the complement relation in given universe.
 */
class IsReflexive {
public:
	/**
	 * Checks whether a relation is reflexive.
	 *
	 * @tparam T Type of the items in relation.
	 *
	 * @param relation the tested relation
	 * @param universe the universe of items participating in the relation
	 *
	 * @return true if the relation is reflexive, false otherwise
	 */
	template < class T >
	static bool isReflexive ( const ext::set < ext::pair < T, T > > & relation, const ext::set < T > & universe );
};

template < class T >
bool IsReflexive::isReflexive ( const ext::set < ext::pair < T, T > > & relation, const ext::set < T > & universe ) {
	for ( const T & state : universe )
		if ( ! relation.contains ( ext::make_pair ( state, state ) ) )
			return false;

	return true;
}

} /* namespace relation */

