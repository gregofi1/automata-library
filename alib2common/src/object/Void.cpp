#include <object/Void.h>
#include <object/Object.h>
#include <registration/ValuePrinterRegistration.hpp>

namespace object {

Void::Void() = default;

std::ostream & operator << ( std::ostream & out, const Void & ) {
	return out << "(Void)";
}

Void Void::VOID = Void();

} /* namespace object */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < object::Void > ( );

} /* namespace */
