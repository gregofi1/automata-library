#include <catch2/catch.hpp>

#include <common/createUnique.hpp>


TEST_CASE ( "Create Unique", "[unit][common]" ) {
	ext::set < int > alphabet1 { 0, 1, 2 };
	ext::set < int > alphabet2 { 3, 4, 5 };

	CHECK ( common::createUnique ( 0, alphabet1, alphabet2 ) == 6 );
	CHECK ( common::createUnique ( 0, alphabet1 ) == 3 );
	CHECK ( common::createUnique ( 0, alphabet2 ) == 0 );
	CHECK ( common::createUnique ( 0 ) == 0 );
}
