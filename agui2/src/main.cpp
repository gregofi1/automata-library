#include <QApplication>

#include <MainWindow.hpp>

#include <version.hpp>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return QApplication::exec();
}
