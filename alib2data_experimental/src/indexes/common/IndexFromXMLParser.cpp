#include <sax/FromXMLParserHelper.h>
#include "IndexFromXMLParser.h"
#include <sax/ParserException.h>
#include <core/xmlApi.hpp>

namespace indexes {

ext::set < DefaultSymbolType > IndexFromXMLParser::parseAlphabet ( ext::deque < sax::Token >::iterator & input ) {
	ext::set < DefaultSymbolType > symbols;
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "alphabet" );

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) )
		symbols.insert ( core::xmlApi < DefaultSymbolType >::parse ( input ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "alphabet" );
	return symbols;
}

SuffixTrieNodeTerminatingSymbol * IndexFromXMLParser::parseSuffixTrieNodeTerminatingSymbol ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "node" );
	ext::map < DefaultSymbolType, SuffixTrieNodeTerminatingSymbol * > children;

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) ) {
		sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, "child" );
		DefaultSymbolType symbol = core::xmlApi < DefaultSymbolType >::parse ( input );
		children.insert ( std::make_pair ( std::move ( symbol ), parseSuffixTrieNodeTerminatingSymbol ( input ) ) );
		sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "child" );
	}

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, "node" );

	return new SuffixTrieNodeTerminatingSymbol ( children );
}

} /* namespace indexes */
