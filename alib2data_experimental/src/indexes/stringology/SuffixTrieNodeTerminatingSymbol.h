#pragma once

#include <common/DefaultSymbolType.h>
#include <alib/map>
#include <alib/set>
#include <core/xmlApi.hpp>

namespace indexes {

class SuffixTrieTerminatingSymbol;

/**
 * Represents a node in the ranked tree. Contains name of the symbol.
 */
class SuffixTrieNodeTerminatingSymbol {
protected:
	ext::map < DefaultSymbolType, SuffixTrieNodeTerminatingSymbol * > m_children;

	SuffixTrieNodeTerminatingSymbol * parent;

	/**
	 * Parent tree contanining this instance of RankedTree
	 */
	const SuffixTrieTerminatingSymbol * parentTree;

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::attachTree()
	 */
	bool attachTree ( const SuffixTrieTerminatingSymbol * tree );

public:
	explicit SuffixTrieNodeTerminatingSymbol ( ext::map < DefaultSymbolType, SuffixTrieNodeTerminatingSymbol * > children );

	SuffixTrieNodeTerminatingSymbol ( const SuffixTrieNodeTerminatingSymbol & other );
	SuffixTrieNodeTerminatingSymbol ( SuffixTrieNodeTerminatingSymbol && other ) noexcept;
	SuffixTrieNodeTerminatingSymbol & operator =( const SuffixTrieNodeTerminatingSymbol & other );
	SuffixTrieNodeTerminatingSymbol & operator =( SuffixTrieNodeTerminatingSymbol && other ) noexcept;
	~SuffixTrieNodeTerminatingSymbol ( ) noexcept;

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::clone ( ) const &
	 */
	SuffixTrieNodeTerminatingSymbol * clone ( ) const &;

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::clone ( ) const &
	 */
	SuffixTrieNodeTerminatingSymbol * clone ( ) &&;

	/**
	 * @copydoc RankedNode::computeMinimalAlphabet()
	 */
	ext::set < DefaultSymbolType > computeMinimalAlphabet ( ) const;

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < DefaultSymbolType > & alphabet ) const;

	/**
	 * @return children
	 */
	const ext::map < const DefaultSymbolType, const SuffixTrieNodeTerminatingSymbol * > & getChildren ( ) const;

	/**
	 * @return children
	 */
	const ext::map < DefaultSymbolType, SuffixTrieNodeTerminatingSymbol * > & getChildren ( );

	SuffixTrieNodeTerminatingSymbol & getChild ( const DefaultSymbolType & symbol );

	const SuffixTrieNodeTerminatingSymbol & getChild ( const DefaultSymbolType & symbol ) const;

	bool hasChild ( const DefaultSymbolType & symbol ) const;

	SuffixTrieNodeTerminatingSymbol & addChild ( DefaultSymbolType symbol, SuffixTrieNodeTerminatingSymbol node );

	SuffixTrieNodeTerminatingSymbol * getParent ( );

	const SuffixTrieNodeTerminatingSymbol * getParent ( ) const;

	void swap ( SuffixTrieNodeTerminatingSymbol & other );

	auto operator <=> ( const SuffixTrieNodeTerminatingSymbol & other ) const {
		return std::tie ( m_children ) <=> std::tie ( other.m_children );
	}

	bool operator == ( const SuffixTrieNodeTerminatingSymbol & other ) const {
		return std::tie ( m_children ) == std::tie ( other.m_children );
	}

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::testSymbol() const
	 */
	bool testSymbol ( const DefaultSymbolType & symbol ) const;

	/**
	 * @copydoc SuffixTrieNodeTerminatingSymbol::operator>>() const
	 */
	void operator >>( ext::ostream & out ) const;

	friend ext::ostream & operator <<( ext::ostream &, const SuffixTrieNodeTerminatingSymbol & node );

	friend class SuffixTrieTerminatingSymbol;
};

} /* namespace indexes */

